﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SoftserveTimeTracker
{
    /// <summary>
    /// Interaction logic for Configuration.xaml
    /// </summary>
    public partial class Configuration : Window
    {
        public Configuration()
        {
            InitializeComponent();
        }

        private void btnContinue_Click(object sender, RoutedEventArgs e)
        {
            bool bHasInternet = Convert.ToBoolean(Properties.Settings.Default["bHasInternet"]);

            if (bHasInternet)
            {
                string strSelectedConfiguration = lstConfigurationTime.SelectedValue.ToString();
                strSelectedConfiguration = strSelectedConfiguration.Substring(strSelectedConfiguration.IndexOf(" ") + 1);

                clsTimeTrackUsers clsUser = new clsTimeTrackUsers(Convert.ToInt32(Properties.Settings.Default["iTimeTrackUserID"]));

                if (clsUser.iConfigurationID == 0)
                {
                    clsConfiguration clsConfig = new clsConfiguration();

                    switch (strSelectedConfiguration)
                    {
                        case "10 Minutes":

                            Properties.Settings.Default["iConfigurationTime"] = 10;
                            Properties.Settings.Default["strConfigurationUnit"] = "Minutes";
                            Properties.Settings.Default.Save();

                            clsConfig.iAddedBy = clsUser.iTimeTrackUserID;
                            clsConfig.dtAdded = DateTime.Now;
                            clsConfig.b10Minutes = true;
                            clsConfig.b30Minutes = false;
                            clsConfig.b1Hour = false;
                            clsConfig.b2Hours = false;
                            clsConfig.b5Hours = false;
                            clsConfig.Update();
                            clsUser.iConfigurationID = clsConfig.iConfigurationID;
                            clsUser.dtEdited = DateTime.Now;
                            clsUser.iEditedBy = clsUser.iTimeTrackUserID;
                            clsUser.Update();

                            break;

                        case "30 Minutes":

                            Properties.Settings.Default["iConfigurationTime"] = 30;
                            Properties.Settings.Default["strConfigurationUnit"] = "Minutes";
                            Properties.Settings.Default.Save();
                            clsConfig.iAddedBy = clsUser.iTimeTrackUserID;
                            clsConfig.dtAdded = DateTime.Now;
                            clsConfig.b10Minutes = false;
                            clsConfig.b30Minutes = true;
                            clsConfig.b1Hour = false;
                            clsConfig.b2Hours = false;
                            clsConfig.b5Hours = false;
                            clsConfig.Update();
                            clsUser.iConfigurationID = clsConfig.iConfigurationID;
                            clsUser.dtEdited = DateTime.Now;
                            clsUser.iEditedBy = clsUser.iTimeTrackUserID;
                            clsUser.Update();

                            break;

                        case "1 Hour":

                            Properties.Settings.Default["iConfigurationTime"] = 1;
                            Properties.Settings.Default["strConfigurationUnit"] = "Hours";
                            Properties.Settings.Default.Save();

                            clsConfig.iAddedBy = clsUser.iTimeTrackUserID;
                            clsConfig.dtAdded = DateTime.Now;
                            clsConfig.b10Minutes = false;
                            clsConfig.b30Minutes = false;
                            clsConfig.b1Hour = true;
                            clsConfig.b2Hours = false;
                            clsConfig.b5Hours = false;
                            clsConfig.Update();
                            clsUser.iConfigurationID = clsConfig.iConfigurationID;
                            clsUser.dtEdited = DateTime.Now;
                            clsUser.iEditedBy = clsUser.iTimeTrackUserID;
                            clsUser.Update();

                            break;

                        case "2 Hours":

                            Properties.Settings.Default["iConfigurationTime"] = 2;
                            Properties.Settings.Default["strConfigurationUnit"] = "Hours";
                            Properties.Settings.Default.Save();

                            clsConfig.iAddedBy = clsUser.iTimeTrackUserID;
                            clsConfig.dtAdded = DateTime.Now;
                            clsConfig.b10Minutes = false;
                            clsConfig.b30Minutes = false;
                            clsConfig.b1Hour = false;
                            clsConfig.b2Hours = true;
                            clsConfig.b5Hours = false;
                            clsConfig.Update();
                            clsUser.iConfigurationID = clsConfig.iConfigurationID;
                            clsUser.dtEdited = DateTime.Now;
                            clsUser.iEditedBy = clsUser.iTimeTrackUserID;
                            clsUser.Update();

                            break;

                        case "5 Hours":

                            Properties.Settings.Default["iConfigurationTime"] = 5;
                            Properties.Settings.Default["strConfigurationUnit"] = "Hours";
                            Properties.Settings.Default.Save();

                            clsConfig.iAddedBy = clsUser.iTimeTrackUserID;
                            clsConfig.dtAdded = DateTime.Now;
                            clsConfig.b10Minutes = false;
                            clsConfig.b30Minutes = false;
                            clsConfig.b1Hour = false;
                            clsConfig.b2Hours = false;
                            clsConfig.b5Hours = true;
                            clsConfig.Update();
                            clsUser.iConfigurationID = clsConfig.iConfigurationID;
                            clsUser.dtEdited = DateTime.Now;
                            clsUser.iEditedBy = clsUser.iTimeTrackUserID;
                            clsUser.Update();

                            break;

                    }

                    CaptureTask ct = new CaptureTask();
                    ct.Show();
                    this.Close();
                }
                else
                {
                    clsConfiguration clsConfig = new clsConfiguration(clsUser.iConfigurationID);

                    switch (strSelectedConfiguration)
                    {
                        case "10 Minutes":

                            Properties.Settings.Default["iConfigurationTime"] = 10;
                            Properties.Settings.Default["strConfigurationUnit"] = "Minutes";
                            Properties.Settings.Default.Save();
                            clsConfig.b10Minutes = true;
                            clsConfig.b30Minutes = false;
                            clsConfig.b1Hour = false;
                            clsConfig.b2Hours = false;
                            clsConfig.b5Hours = false;
                            clsConfig.iEditedBy = clsUser.iTimeTrackUserID;
                            clsConfig.dtEdited = DateTime.Now;
                            clsConfig.Update();
                            clsUser.dtEdited = DateTime.Now;
                            clsUser.iEditedBy = clsUser.iTimeTrackUserID;
                            clsUser.Update();

                            break;

                        case "30 Minutes":

                            Properties.Settings.Default["iConfigurationTime"] = 30;
                            Properties.Settings.Default["strConfigurationUnit"] = "Minutes";
                            Properties.Settings.Default.Save();
                            clsConfig.b10Minutes = false;
                            clsConfig.b30Minutes = true;
                            clsConfig.b1Hour = false;
                            clsConfig.b2Hours = false;
                            clsConfig.b5Hours = false;
                            clsConfig.iEditedBy = clsUser.iTimeTrackUserID;
                            clsConfig.dtEdited = DateTime.Now;
                            clsConfig.Update();
                            clsUser.dtEdited = DateTime.Now;
                            clsUser.iEditedBy = clsUser.iTimeTrackUserID;
                            clsUser.Update();

                            break;

                        case "1 Hour":

                            Properties.Settings.Default["iConfigurationTime"] = 1;
                            Properties.Settings.Default["strConfigurationUnit"] = "Hours";
                            Properties.Settings.Default.Save();
                            clsConfig.b10Minutes = false;
                            clsConfig.b30Minutes = false;
                            clsConfig.b1Hour = true;
                            clsConfig.b2Hours = false;
                            clsConfig.b5Hours = false;
                            clsConfig.iEditedBy = clsUser.iTimeTrackUserID;
                            clsConfig.dtEdited = DateTime.Now;
                            clsConfig.Update();
                            clsUser.dtEdited = DateTime.Now;
                            clsUser.iEditedBy = clsUser.iTimeTrackUserID;
                            clsUser.Update();

                            break;

                        case "2 Hours":

                            Properties.Settings.Default["iConfigurationTime"] = 2;
                            Properties.Settings.Default["strConfigurationUnit"] = "Hours";
                            Properties.Settings.Default.Save();
                            clsConfig.b10Minutes = false;
                            clsConfig.b30Minutes = false;
                            clsConfig.b1Hour = false;
                            clsConfig.b2Hours = true;
                            clsConfig.b5Hours = false;
                            clsConfig.iEditedBy = clsUser.iTimeTrackUserID;
                            clsConfig.dtEdited = DateTime.Now;
                            clsConfig.Update();
                            clsUser.dtEdited = DateTime.Now;
                            clsUser.iEditedBy = clsUser.iTimeTrackUserID;
                            clsUser.Update();

                            break;

                        case "5 Hours":

                            Properties.Settings.Default["iConfigurationTime"] = 5;
                            Properties.Settings.Default["strConfigurationUnit"] = "Hours";
                            Properties.Settings.Default.Save();
                            clsConfig.b10Minutes = false;
                            clsConfig.b30Minutes = false;
                            clsConfig.b1Hour = false;
                            clsConfig.b2Hours = false;
                            clsConfig.b5Hours = true;
                            clsConfig.iEditedBy = clsUser.iTimeTrackUserID;
                            clsConfig.dtEdited = DateTime.Now;
                            clsConfig.Update();
                            clsUser.dtEdited = DateTime.Now;
                            clsUser.iEditedBy = clsUser.iTimeTrackUserID;
                            clsUser.Update();

                            break;

                    }

                    this.Close();
                }

            }
            else
            {
                string strSelectedConfiguration = lstConfigurationTime.SelectedValue.ToString();
                strSelectedConfiguration = strSelectedConfiguration.Substring(strSelectedConfiguration.IndexOf(" ") + 1);

                clsTimeTrackUsers clsUser = new clsTimeTrackUsers(Properties.Settings.Default["Username"].ToString(), clsCommonFunctions.GetMd5Sum(Properties.Settings.Default["strPassword"].ToString()), true, true);

                if (clsUser.iConfigurationID == 0)
                {
                    clsConfiguration clsConfig = new clsConfiguration();

                    switch (strSelectedConfiguration)
                    {
                        case "10 Minutes":

                            Properties.Settings.Default["iConfigurationTime"] = 10;
                            Properties.Settings.Default["strConfigurationUnit"] = "Minutes";
                            Properties.Settings.Default.Save();

                            clsConfig.iAddedBy = clsUser.iTimeTrackUserID;
                            clsConfig.dtAdded = DateTime.Now;
                            clsConfig.b10Minutes = true;
                            clsConfig.b30Minutes = false;
                            clsConfig.b1Hour = false;
                            clsConfig.b2Hours = false;
                            clsConfig.b5Hours = false;
                            clsConfig.UpdateLocal();
                            clsUser.iConfigurationID = clsConfig.iConfigurationID;
                            clsUser.dtEdited = DateTime.Now;
                            clsUser.iEditedBy = clsUser.iTimeTrackUserID;
                            clsUser.UpdateLocal();

                            break;

                        case "30 Minutes":

                            Properties.Settings.Default["iConfigurationTime"] = 30;
                            Properties.Settings.Default["strConfigurationUnit"] = "Minutes";
                            Properties.Settings.Default.Save();
                            clsConfig.iAddedBy = clsUser.iTimeTrackUserID;
                            clsConfig.dtAdded = DateTime.Now;
                            clsConfig.b10Minutes = false;
                            clsConfig.b30Minutes = true;
                            clsConfig.b1Hour = false;
                            clsConfig.b2Hours = false;
                            clsConfig.b5Hours = false;
                            clsConfig.UpdateLocal();
                            clsUser.iConfigurationID = clsConfig.iConfigurationID;
                            clsUser.dtEdited = DateTime.Now;
                            clsUser.iEditedBy = clsUser.iTimeTrackUserID;
                            clsUser.UpdateLocal();

                            break;

                        case "1 Hour":

                            Properties.Settings.Default["iConfigurationTime"] = 1;
                            Properties.Settings.Default["strConfigurationUnit"] = "Hours";
                            Properties.Settings.Default.Save();

                            clsConfig.iAddedBy = clsUser.iTimeTrackUserID;
                            clsConfig.dtAdded = DateTime.Now;
                            clsConfig.b10Minutes = false;
                            clsConfig.b30Minutes = false;
                            clsConfig.b1Hour = true;
                            clsConfig.b2Hours = false;
                            clsConfig.b5Hours = false;
                            clsConfig.UpdateLocal();
                            clsUser.iConfigurationID = clsConfig.iConfigurationID;
                            clsUser.dtEdited = DateTime.Now;
                            clsUser.iEditedBy = clsUser.iTimeTrackUserID;
                            clsUser.UpdateLocal();

                            break;

                        case "2 Hours":

                            Properties.Settings.Default["iConfigurationTime"] = 2;
                            Properties.Settings.Default["strConfigurationUnit"] = "Hours";
                            Properties.Settings.Default.Save();

                            clsConfig.iAddedBy = clsUser.iTimeTrackUserID;
                            clsConfig.dtAdded = DateTime.Now;
                            clsConfig.b10Minutes = false;
                            clsConfig.b30Minutes = false;
                            clsConfig.b1Hour = false;
                            clsConfig.b2Hours = true;
                            clsConfig.b5Hours = false;
                            clsConfig.UpdateLocal();
                            clsUser.iConfigurationID = clsConfig.iConfigurationID;
                            clsUser.dtEdited = DateTime.Now;
                            clsUser.iEditedBy = clsUser.iTimeTrackUserID;
                            clsUser.UpdateLocal();

                            break;

                        case "5 Hours":

                            Properties.Settings.Default["iConfigurationTime"] = 5;
                            Properties.Settings.Default["strConfigurationUnit"] = "Hours";
                            Properties.Settings.Default.Save();

                            clsConfig.iAddedBy = clsUser.iTimeTrackUserID;
                            clsConfig.dtAdded = DateTime.Now;
                            clsConfig.b10Minutes = false;
                            clsConfig.b30Minutes = false;
                            clsConfig.b1Hour = false;
                            clsConfig.b2Hours = false;
                            clsConfig.b5Hours = true;
                            clsConfig.UpdateLocal();
                            clsUser.iConfigurationID = clsConfig.iConfigurationID;
                            clsUser.dtEdited = DateTime.Now;
                            clsUser.iEditedBy = clsUser.iTimeTrackUserID;
                            clsUser.UpdateLocal();

                            break;

                    }

                    CaptureTask ct = new CaptureTask();
                    ct.Show();
                    this.Close();
                }
                else
                {
                    clsConfiguration clsConfig = new clsConfiguration(clsUser.iConfigurationID, true);

                    switch (strSelectedConfiguration)
                    {
                        case "10 Minutes":

                            Properties.Settings.Default["iConfigurationTime"] = 10;
                            Properties.Settings.Default["strConfigurationUnit"] = "Minutes";
                            Properties.Settings.Default.Save();
                            clsConfig.b10Minutes = true;
                            clsConfig.b30Minutes = false;
                            clsConfig.b1Hour = false;
                            clsConfig.b2Hours = false;
                            clsConfig.b5Hours = false;
                            clsConfig.iEditedBy = clsUser.iTimeTrackUserID;
                            clsConfig.dtEdited = DateTime.Now;
                            clsConfig.UpdateLocal();
                            clsUser.dtEdited = DateTime.Now;
                            clsUser.iEditedBy = clsUser.iTimeTrackUserID;
                            clsUser.UpdateLocal();

                            break;

                        case "30 Minutes":

                            Properties.Settings.Default["iConfigurationTime"] = 30;
                            Properties.Settings.Default["strConfigurationUnit"] = "Minutes";
                            Properties.Settings.Default.Save();
                            clsConfig.b10Minutes = false;
                            clsConfig.b30Minutes = true;
                            clsConfig.b1Hour = false;
                            clsConfig.b2Hours = false;
                            clsConfig.b5Hours = false;
                            clsConfig.iEditedBy = clsUser.iTimeTrackUserID;
                            clsConfig.dtEdited = DateTime.Now;
                            clsConfig.UpdateLocal();
                            clsUser.dtEdited = DateTime.Now;
                            clsUser.iEditedBy = clsUser.iTimeTrackUserID;
                            clsUser.UpdateLocal();

                            break;

                        case "1 Hour":

                            Properties.Settings.Default["iConfigurationTime"] = 1;
                            Properties.Settings.Default["strConfigurationUnit"] = "Hours";
                            Properties.Settings.Default.Save();
                            clsConfig.b10Minutes = false;
                            clsConfig.b30Minutes = false;
                            clsConfig.b1Hour = true;
                            clsConfig.b2Hours = false;
                            clsConfig.b5Hours = false;
                            clsConfig.iEditedBy = clsUser.iTimeTrackUserID;
                            clsConfig.dtEdited = DateTime.Now;
                            clsConfig.UpdateLocal();
                            clsUser.dtEdited = DateTime.Now;
                            clsUser.iEditedBy = clsUser.iTimeTrackUserID;
                            clsUser.UpdateLocal();

                            break;

                        case "2 Hours":

                            Properties.Settings.Default["iConfigurationTime"] = 2;
                            Properties.Settings.Default["strConfigurationUnit"] = "Hours";
                            Properties.Settings.Default.Save();
                            clsConfig.b10Minutes = false;
                            clsConfig.b30Minutes = false;
                            clsConfig.b1Hour = false;
                            clsConfig.b2Hours = true;
                            clsConfig.b5Hours = false;
                            clsConfig.iEditedBy = clsUser.iTimeTrackUserID;
                            clsConfig.dtEdited = DateTime.Now;
                            clsConfig.UpdateLocal();
                            clsUser.dtEdited = DateTime.Now;
                            clsUser.iEditedBy = clsUser.iTimeTrackUserID;
                            clsUser.UpdateLocal();

                            break;

                        case "5 Hours":

                            Properties.Settings.Default["iConfigurationTime"] = 5;
                            Properties.Settings.Default["strConfigurationUnit"] = "Hours";
                            Properties.Settings.Default.Save();
                            clsConfig.b10Minutes = false;
                            clsConfig.b30Minutes = false;
                            clsConfig.b1Hour = false;
                            clsConfig.b2Hours = false;
                            clsConfig.b5Hours = true;
                            clsConfig.iEditedBy = clsUser.iTimeTrackUserID;
                            clsConfig.dtEdited = DateTime.Now;
                            clsConfig.UpdateLocal();
                            clsUser.dtEdited = DateTime.Now;
                            clsUser.iEditedBy = clsUser.iTimeTrackUserID;
                            clsUser.UpdateLocal();

                            break;

                    }

                    this.Close();
                }
            }

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            PopulateDropDown();
        }

        private void PopulateDropDown()
        {
            bool bHasInternet = Convert.ToBoolean(Properties.Settings.Default["bHasInternet"]);

            if (bHasInternet)
            {
                clsTimeTrackUsers clsUser = new clsTimeTrackUsers(Convert.ToInt32(Properties.Settings.Default["iTimeTrackUserID"]));
                if (clsUser.iConfigurationID != null && clsUser.iConfigurationID != 0)
                {
                    clsConfiguration clsConfig = new clsConfiguration(clsUser.iConfigurationID);
                    if (clsConfig.b10Minutes)
                    {
                        lstConfigurationTime.SelectedIndex = 0;
                    }
                    else if (clsConfig.b30Minutes)
                    {
                        lstConfigurationTime.SelectedIndex = 1;
                    }
                    else if (clsConfig.b1Hour)
                    {
                        lstConfigurationTime.SelectedIndex = 2;
                    }
                    else if (clsConfig.b2Hours)
                    {
                        lstConfigurationTime.SelectedIndex = 3;
                    }
                    else if (clsConfig.b5Hours)
                    {
                        lstConfigurationTime.SelectedIndex = 4;
                    }
                }
                else
                {

                }
            }
            else
            {
                clsTimeTrackUsers clsUser = new clsTimeTrackUsers(Properties.Settings.Default["Username"].ToString(), clsCommonFunctions.GetMd5Sum(Properties.Settings.Default["strPassword"].ToString()), true, true);
                if (clsUser.iConfigurationID != 0)
                {
                    clsConfiguration clsConfig = new clsConfiguration(clsUser.iConfigurationID, true);
                    if (clsConfig.b10Minutes)
                    {
                        lstConfigurationTime.SelectedIndex = 0;
                    }
                    else if (clsConfig.b30Minutes)
                    {
                        lstConfigurationTime.SelectedIndex = 1;
                    }
                    else if (clsConfig.b1Hour)
                    {
                        lstConfigurationTime.SelectedIndex = 2;
                    }
                    else if (clsConfig.b2Hours)
                    {
                        lstConfigurationTime.SelectedIndex = 3;
                    }
                    else if (clsConfig.b5Hours)
                    {
                        lstConfigurationTime.SelectedIndex = 4;
                    }
                }
                else
                {

                }
            }
        }
    }
}
