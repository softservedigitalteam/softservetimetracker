﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.IO;
using System.Text;

/// <summary>
/// Summary description for clsSiteCommon
/// </summary>
public class clsSiteCommon
{
    #region CONSTRUCTOR

    private clsSiteCommon()
    {
    }

    /// <summary>
    /// Checks to see if the iEmployeeID cookie is set and active. If it is not active, method redirects to the login page, with the encrypted form of the branch id.
    /// </summary>
    //public static void DoLoggedInCheck()
    //{
    //    //### Check logged in
    //    if (HttpContext.Current.Request.Cookies["strEmail"] == null || string.IsNullOrEmpty(HttpContext.Current.Request.Cookies["strEmail"].Value))
    //    {
    //        HttpContext.Current.Response.Redirect("frmLogin.aspx");
    //    }
    //    else
    //    {
    //        HttpContext.Current.Session["clsAccountHolders"] = new clsAccountHolders(HttpContext.Current.Request.Cookies["strEmail"].Value, HttpContext.Current.Request.Cookies["strPassword"].Value);

    //        clsAccountHolders clsAccountHolders = (clsAccountHolders)HttpContext.Current.Session["clsAccountHolders"];

    //        //### Populate that users Access Rights
    //        clsAccessList clsAccessList = new clsAccessList(clsAccountHolders);
    //        clsAccessList.FilterExpression = "(iEmployeeID =" + clsAccountHolders.iTimeTrackUserID + ")";

    //        clsAccountHolders.DicAccessRights = new Dictionary<int, clsAccountHolders.AccessRights>();

    //        foreach (DataRow dataRowAccessItem in clsAccessList.GetRecords().ToTable().Rows)
    //        {
    //            //Creates the struct and sets its values
    //            clsAccountHolders.AccessRights structAccessRights = new clsAccountHolders.AccessRights(Convert.ToBoolean(dataRowAccessItem["bCreate"]), Convert.ToBoolean(dataRowAccessItem["bManage"]), Convert.ToBoolean(dataRowAccessItem["bIsReadable"]));
    //            clsAccountHolders.DicAccessRights.Add(Convert.ToInt32(dataRowAccessItem["iModuleID"]), structAccessRights);
    //        }
    //    }
    //}

    #endregion

    //#region CONTACT FORM

    //public static void ShowContactForm(Page Page, String strFormTitle, String strContactForm, int iClientID)
    //{
    //    Page.Session["ContactForm"] = strContactForm;

    //    try
    //    {
    //        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowContactForm" + DateTime.Now.ToString("hh_mm_ss"),
    //            "$(document).ready(function() {" +
    //            "    $.ajaxSetup ({cache: false});" +
    //            "    $.colorbox({ returnFocus:true, href: 'frmContactsAddModify.aspx?iClientID=" + iClientID + "', opacity: 0.7, overlayClose:false, fixed:true, width:'610px', height:'650px', title:'<span class=\"\">" + strFormTitle + "</span>'  });" +
    //            "});", true);
    //    }
    //    catch { }
    //}

    //public static void ShowDomainForm(Page Page, String strFormTitle, String strDomainForm, int iHostID)
    //{
    //    Page.Session["DomainForm"] = strDomainForm;

    //    try
    //    {
    //        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowDomainForm" + DateTime.Now.ToString("hh_mm_ss"),
    //            "$(document).ready(function() {" +
    //            "    $.ajaxSetup ({cache: false});" +
    //            "    $.colorbox({ returnFocus:true, href: 'frmDomainAddModify.aspx?iHostID=" + iHostID + "', opacity: 0.7, overlayClose:false, fixed:true, width:'610px', height:'650px', title:'<span class=\"\">" + strFormTitle + "</span>'  });" +
    //            "});", true);
    //    }
    //    catch { }
    //}

    //public static void ShowFullMessageForm(Page Page, String strFormTitle, String strContactForm, int iMessageID)
    //{
    //    Page.Session["ContactForm"] = strContactForm;

    //    try
    //    {
    //        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowFullMessageForm" + DateTime.Now.ToString("hh_mm_ss"),
    //            "$(document).ready(function() {" +
    //            "    $.ajaxSetup ({cache: false});" +
    //            "    $.colorbox({ returnFocus:true, href: 'frmFullMessage.aspx?iMessageID=" + iMessageID + "', opacity: 0.7, overlayClose:false, fixed:true, width:'610px', height:'300px', title:'<span class=\"\">" + strFormTitle + "</span>'  });" +
    //            "});", true);
    //    }
    //    catch { }
    //}

    ////public static void ShowContactForm(Page Page, String strFormTitle, String strContactForm, int iClientID, int iContactID)
    ////{
    ////    Page.Session["ContactForm"] = strContactForm;

    ////    try
    ////    {
    ////        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowContactForm",
    ////            "$(document).ready(function() {" +
    ////            "    $.ajaxSetup ({cache: false});" +
    ////            "    $.colorbox({ href: 'frmContactsAddModify.aspx?iClientID=" + iClientID + "&iContactID=" + iContactID + "', opacity: 0.7, overlayClose:false, fixed:true, width:'610px', height:'650px', title:'<span class=\"\">" + strFormTitle + "</span>'  });" +
    ////            "});", true);
    ////    }
    ////    catch { }
    ////}

    //public static void ShowContactForm(Page Page)
    //{
    //    ShowContactForm(Page, " Contact Form", "", 0);
    //}

    //public static void ShowDomainForm(Page Page)
    //{
    //    ShowContactForm(Page, " Domain Form", "Yes", 0);
    //}

    //#endregion

    #region ERROR MESSAGE

   

    #endregion

    #region CHECKOUT

    #endregion
}