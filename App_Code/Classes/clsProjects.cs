﻿
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Data.SqlClient;
public class clsProjects
{
    #region MEMBER VARIABLES

    private int m_iProjectID;
    private DateTime m_dtAdded;
    private int m_iAddedBy;
    private DateTime m_dtEdited;
    private int m_iEditedBy;
    private string m_strAsanaProjectID;
    private String m_strTitle;
    private String m_strDescription;
    private bool m_bIsDeleted;

    #endregion

    #region PROPERTIES

    public int iProjectID
    {
        get
        {
            return m_iProjectID;
        }
    }

    public DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public string strAsanaProjectID
    {
        get
        {
            return m_strAsanaProjectID;
        }
        set
        {
            m_strAsanaProjectID = value;
        }
    }

    public String strTitle
    {
        get
        {
            return m_strTitle;
        }
        set
        {
            m_strTitle = value;
        }
    }

    public String strDescription
    {
        get
        {
            return m_strDescription;
        }
        set
        {
            m_strDescription = value;
        }
    }

    public bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }


    #endregion

    #region CONSTRUCTORS

    public clsProjects()
    {
        m_iProjectID = 0;
    }

    public clsProjects(int iProjectID)
    {
        m_iProjectID = iProjectID;
        GetData();
    }

    public clsProjects(int iProjectID, bool bIsLocal)
    {
        m_iProjectID = iProjectID;
        GetDataLocal();
    }

    #endregion

    #region PUBLIC METHODS

    public virtual void Update()
    {
        try
        {
            if (iProjectID == 0)
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strAsanaProjectID", m_strAsanaProjectID),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strDescription", m_strDescription),          
                  };

                //### Add
                m_iProjectID = (int)clsDataAccess.ExecuteScalar("spProjectsInsert", sqlParametersInsert);
            }
            else
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iProjectID", m_iProjectID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strAsanaProjectID", m_strAsanaProjectID),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strDescription", m_strDescription),
                    };
                //### Update
                clsDataAccess.Execute("spProjectsUpdate", sqlParametersUpdate);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void Delete(int iProjectID)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iProjectID", iProjectID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spProjectsDelete", sqlParameter);
    }

    public static DataTable GetProjectsList()
    {
        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        return clsDataAccess.GetDataTable("spProjectsList", EmptySqlParameter);
    }
    public static DataTable GetProjectsList(string strFilterExpression, string strSortExpression)
    {
        DataView dvClientsList = new DataView();

        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        dvClientsList = clsDataAccess.GetDataView("spProjectsList", EmptySqlParameter);
        dvClientsList.RowFilter = strFilterExpression;
        dvClientsList.Sort = strSortExpression;

        return dvClientsList.ToTable();
    }
    #endregion

    #region PUBLIC LOCAL METHODS

    public virtual void UpdateLocal()
    {
        try
        {
            if (iProjectID == 0)
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strAsanaProjectID", m_strAsanaProjectID),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strDescription", m_strDescription),          
                  };

                //### Add
                m_iProjectID = (int)clsDataAccessLocal.ExecuteScalar("spProjectsInsert", sqlParametersInsert);
            }
            else
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iProjectID", m_iProjectID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strAsanaProjectID", m_strAsanaProjectID),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strDescription", m_strDescription),
                    };
                //### Update
                clsDataAccessLocal.Execute("spProjectsUpdate", sqlParametersUpdate);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void DeleteLocal(int iProjectID)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iProjectID", iProjectID)
        };
        //### Executes delete sp
        clsDataAccessLocal.Execute("spProjectsDelete", sqlParameter);
    }

    public static DataTable GetProjectsListLocal()
    {
        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        DataTable dt = clsDataAccessLocal.GetDataTable("spProjectsList", EmptySqlParameter);
        //foreach(DataRow dr in dt.Rows)
        //{
        //    string str = dr["strTitle"].ToString();
        //}
        return clsDataAccessLocal.GetDataTable("spProjectsList", EmptySqlParameter);
    }


    public static DataTable GetProjectsListLocal(string strFilterExpression, string strSortExpression)
    {
        DataView dvClientsList = new DataView();

        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        dvClientsList = clsDataAccessLocal.GetDataView("spProjectsList", EmptySqlParameter);
        dvClientsList.RowFilter = strFilterExpression;
        dvClientsList.Sort = strSortExpression;

        return dvClientsList.ToTable();
    }
    #endregion

    #region PROTECTED METHODS

    protected virtual void GetData()
    {
        try
        {
            //### Populate
            SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iProjectID", m_iProjectID)
                        };
            DataRow drRecord = clsDataAccess.GetRecord("spProjectsGetRecord", sqlParameter);

            m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
            m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

            if (drRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

            if (drRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);
            m_strAsanaProjectID = drRecord["strAsanaProjectID"].ToString();
            m_strTitle = drRecord["strTitle"].ToString();
            m_strDescription = drRecord["strDescription"].ToString();
            m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    #region PROTECTED LOCAL METHODS

    protected virtual void GetDataLocal()
    {
        try
        {
            //### Populate
            SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iProjectID", m_iProjectID)
                        };
            DataRow drRecord = clsDataAccessLocal.GetRecord("spProjectsGetRecord", sqlParameter);

            m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
            m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

            if (drRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

            if (drRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);
            m_strAsanaProjectID = drRecord["strAsanaProjectID"].ToString();
            m_strTitle = drRecord["strTitle"].ToString();
            m_strDescription = drRecord["strDescription"].ToString();
            m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
}
