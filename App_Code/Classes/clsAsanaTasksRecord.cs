
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;

using System.Data.SqlClient;

/// <summary>
/// Summary description for clsAsanaTasks
/// </summary>
public class clsAsanaTasks
{
    #region MEMBER VARIABLES

     private     int m_iAsanaTaskID;
     private     DateTime m_dtAdded;
     private     int m_iAddedBy;
     private     DateTime m_dtEdited;
     private     int m_iEditedBy;
     private     string m_strTitle;
     private     string m_strDescription;
    private string m_strAsanaTaskLiveID;
     private     string m_strAssigneeID;
     private     bool m_bIsCompleted;
     private     DateTime m_dtCompleted;
     private     string m_strWorkspaceID;
     private     string m_strAsanaProjectID;
    private int m_iProjectID;
     private     bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

    public int iAsanaTaskID
    {
        get
        {
            return m_iAsanaTaskID;
        }
    }

    public    DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public    int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public    DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public    int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public    string strTitle
    {
        get
        {
            return m_strTitle;
        }
        set
        {
            m_strTitle = value;
        }
    }

    public    string strDescription
    {
        get
        {
            return m_strDescription;
        }
        set
        {
            m_strDescription = value;
        }
    }

    public string strAsanaTaskLiveID
    {
        get
        {
            return m_strAsanaTaskLiveID;
        }
        set
        {
            m_strAsanaTaskLiveID = value;
        }
    }

    public    string strAssigneeID
    {
        get
        {
            return m_strAssigneeID;
        }
        set
        {
            m_strAssigneeID = value;
        }
    }

    public    bool bIsCompleted
    {
        get
        {
            return m_bIsCompleted;
        }
        set
        {
            m_bIsCompleted = value;
        }
    }

    public    DateTime dtCompleted
    {
        get
        {
            return m_dtCompleted;
        }
        set
        {
            m_dtCompleted = value;
        }
    }

    public    string strWorkspaceID
    {
        get
        {
            return m_strWorkspaceID;
        }
        set
        {
            m_strWorkspaceID = value;
        }
    }

    public  string strAsanaProjectID
    {
        get
        {
            return m_strAsanaProjectID;
        }
        set
        {
            m_strAsanaProjectID = value;
        }
    }

        public int iProjectID
    {
        get
        {
            return m_iProjectID;
        }
        set
        {
            m_iProjectID = value;
        }
    }

    public    bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsAsanaTasks()
    {
        m_iAsanaTaskID = 0;
    }

    public clsAsanaTasks(int iAsanaTaskID)
    {
        m_iAsanaTaskID = iAsanaTaskID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iAsanaTaskID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strDescription", m_strDescription),
                        new SqlParameter("@strAsanaTaskLiveID", m_strAsanaTaskLiveID),
                        new SqlParameter("@strAssigneeID", m_strAssigneeID),
                        new SqlParameter("@bIsCompleted", m_bIsCompleted),
                        new SqlParameter("@dtCompleted", m_dtCompleted),
                        new SqlParameter("@strWorkspaceID", m_strWorkspaceID),
                        new SqlParameter("@strAsanaProjectID", m_strAsanaProjectID),
                        new SqlParameter("@iProjectID", m_iProjectID)
                  };

                  //### Add
                  m_iAsanaTaskID = (int)clsDataAccess.ExecuteScalar("spAsanaTasksInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iAsanaTaskID", m_iAsanaTaskID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strDescription", m_strDescription),
                         new SqlParameter("@strAsanaTaskLiveID", m_strAsanaTaskLiveID),
                        new SqlParameter("@strAssigneeID", m_strAssigneeID),
                         new SqlParameter("@bIsCompleted", m_bIsCompleted),
                         new SqlParameter("@dtCompleted", m_dtCompleted),
                         new SqlParameter("@strWorkspaceID", m_strWorkspaceID),
                        new SqlParameter("@strAsanaProjectID", m_strAsanaProjectID),
                         new SqlParameter("@iProjectID", m_iProjectID)
          };
          //### Update
          clsDataAccess.Execute("spAsanaTasksUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iAsanaTaskID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iAsanaTaskID", iAsanaTaskID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spAsanaTasksDelete", sqlParameter);
        }

        public static DataTable GetAsanaTasksList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spAsanaTasksList", EmptySqlParameter);
        }
        public static DataTable GetAsanaTasksList(string strFilterExpression, string strSortExpression)
        {
            DataView dvAsanaTasksList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvAsanaTasksList = clsDataAccess.GetDataView("spAsanaTasksList", EmptySqlParameter);
            dvAsanaTasksList.RowFilter = strFilterExpression;
            dvAsanaTasksList.Sort = strSortExpression;

            return dvAsanaTasksList.ToTable();
        }
    #endregion

    #region PUBLIC LOCAL METHODS
    public virtual void UpdateLocal()
    {
        try
        {
            if (iAsanaTaskID == 0)
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersInsert = new SqlParameter[]
                {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strDescription", m_strDescription),
                        new SqlParameter("@strAsanaTaskLiveID", m_strAsanaTaskLiveID),
                        new SqlParameter("@strAssigneeID", m_strAssigneeID),
                        new SqlParameter("@bIsCompleted", m_bIsCompleted),
                        new SqlParameter("@dtCompleted", m_dtCompleted),
                        new SqlParameter("@strWorkspaceID", m_strWorkspaceID),
                        new SqlParameter("@strAsanaProjectID", m_strAsanaProjectID),
                        new SqlParameter("@iProjectID", m_iProjectID)
              };

                //### Add
                m_iAsanaTaskID = (int)clsDataAccessLocal.ExecuteScalar("spAsanaTasksInsert", sqlParametersInsert);
            }
            else
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersUpdate = new SqlParameter[]
                {
                         new SqlParameter("@iAsanaTaskID", m_iAsanaTaskID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strDescription", m_strDescription),
                         new SqlParameter("@strAsanaTaskLiveID", m_strAsanaTaskLiveID),
                        new SqlParameter("@strAssigneeID", m_strAssigneeID),
                         new SqlParameter("@bIsCompleted", m_bIsCompleted),
                         new SqlParameter("@dtCompleted", m_dtCompleted),
                         new SqlParameter("@strWorkspaceID", m_strWorkspaceID),
                        new SqlParameter("@strAsanaProjectID", m_strAsanaProjectID),
                         new SqlParameter("@iProjectID", m_iProjectID)
      };
                //### Update
                clsDataAccessLocal.Execute("spAsanaTasksUpdate", sqlParametersUpdate);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public static void DeleteLocal(int iAsanaTaskID)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iAsanaTaskID", iAsanaTaskID)
        };
        //### Executes delete sp
        clsDataAccessLocal.Execute("spAsanaTasksDelete", sqlParameter);
    }

    public static DataTable GetAsanaTasksListLocal()
    {
        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        return clsDataAccessLocal.GetDataTable("spAsanaTasksList", EmptySqlParameter);
    }
    public static DataTable GetAsanaTasksListLocal(string strFilterExpression, string strSortExpression)
    {
        DataView dvAsanaTasksList = new DataView();

        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        dvAsanaTasksList = clsDataAccessLocal.GetDataView("spAsanaTasksList", EmptySqlParameter);
        dvAsanaTasksList.RowFilter = strFilterExpression;
        dvAsanaTasksList.Sort = strSortExpression;

        return dvAsanaTasksList.ToTable();
    }
    #endregion


    #region PROTECTED METHODS

    protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iAsanaTaskID", m_iAsanaTaskID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spAsanaTasksGetRecord", sqlParameter);

               m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
               m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

           if (drRecord["dtEdited"] != DBNull.Value)
               m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

           if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                   m_strTitle = drRecord["strTitle"].ToString();
                   m_strDescription = drRecord["strDescription"].ToString();
            m_strAsanaTaskLiveID = drRecord["strAsanaTaskLiveID"].ToString();
            m_strAssigneeID = drRecord["strAssigneeID"].ToString();
                   m_bIsCompleted = Convert.ToBoolean(drRecord["bIsCompleted"]);
               m_dtCompleted = Convert.ToDateTime(drRecord["dtCompleted"]);
               m_strWorkspaceID = drRecord["strWorkspaceID"].ToString();
            m_strAsanaProjectID = drRecord["strAsanaProjectID"].ToString();
            m_iProjectID = Convert.ToInt32(drRecord["iProjectID"]);
            m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    protected virtual void GetDataLocal()
    {
        try
        {
            //### Populate
            SqlParameter[] sqlParameter = new SqlParameter[]
                {
                            new SqlParameter("@iAsanaTaskID", m_iAsanaTaskID)
                };
            DataRow drRecord = clsDataAccessLocal.GetRecord("spAsanaTasksGetRecord", sqlParameter);

            m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
            m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

            if (drRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

            if (drRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

            m_strTitle = drRecord["strTitle"].ToString();
            m_strDescription = drRecord["strDescription"].ToString();
            m_strAsanaTaskLiveID = drRecord["strAsanaTaskLiveID"].ToString();
            m_strAssigneeID = drRecord["strAssigneeID"].ToString();
            m_bIsCompleted = Convert.ToBoolean(drRecord["bIsCompleted"]);
            m_dtCompleted = Convert.ToDateTime(drRecord["dtCompleted"]);
            m_strWorkspaceID = drRecord["strWorkspaceID"].ToString();
            m_strAsanaProjectID = drRecord["strAsanaProjectID"].ToString();
            m_iProjectID = Convert.ToInt32(drRecord["iProjectID"]);
            m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
}