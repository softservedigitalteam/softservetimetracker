
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsTasks
/// </summary>
public class clsTasks
{
    #region MEMBER VARIABLES

    private int m_iTaskID;
    private DateTime m_dtAdded;
    private int m_iAddedBy;
    private DateTime m_dtEdited;
    private int m_iEditedBy;
    private int m_iProjectID;
    private String m_strTaskName;
    private String m_strTaskNotes;
    private String m_strDuration;
    private DateTime m_dtTimeSessionElapsed;
    private bool m_bIsTaskcompleted;
    private int m_iAsanaTaskID;
    private bool m_bIsDeleted;

    #endregion

    #region PROPERTIES

    public int iTaskID
    {
        get
        {
            return m_iTaskID;
        }
    }

    public DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public int iProjectID
    {
        get
        {
            return m_iProjectID;
        }
        set
        {
            m_iProjectID = value;
        }
    }

    public String strTaskName
    {
        get
        {
            return m_strTaskName;
        }
        set
        {
            m_strTaskName = value;
        }
    }

    public String strDuration
    {
        get
        {
            return m_strDuration;
        }
        set
        {
            m_strDuration = value;
        }
    }

    public String strTaskNotes
    {
        get
        {
            return m_strTaskNotes;
        }
        set
        {
            m_strTaskNotes = value;
        }
    }

    public DateTime dtTimeSessionElapsed
    {
        get
        {
            return m_dtTimeSessionElapsed;
        }
        set
        {
            m_dtTimeSessionElapsed = value;
        }
    }

    public bool bIsTaskcompleted
    {
        get
        {
            return m_bIsTaskcompleted;
        }
        set
        {
            m_bIsTaskcompleted = value;
        }
    }

    public int iAsanaTaskID
    {
        get
        {
            return m_iAsanaTaskID;
        }
        set
        {
            m_iAsanaTaskID = value;
        }
    }

    public bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }


    #endregion

    #region CONSTRUCTORS

    public clsTasks()
    {
        m_iTaskID = 0;
    }

    public clsTasks(int iTaskID)
    {
        m_iTaskID = iTaskID;
        GetData();
    }

    public clsTasks(int iTaskID, bool bIsLocal)
    {
        m_iTaskID = iTaskID;
        GetDataLocal();
    }

    #endregion

    #region PUBLIC METHODS

    public virtual void Update()
    {
        try
        {
            if (iTaskID == 0)
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@iProjectID", m_iProjectID),
                        new SqlParameter("@strTaskName", m_strTaskName),
                        new SqlParameter("@strTaskNotes", m_strTaskNotes),
                        new SqlParameter("@strDuration", m_strDuration),
                        new SqlParameter("@dtTimeSessionElapsed", m_dtTimeSessionElapsed),
                        new SqlParameter("@bIsTaskcompleted", m_bIsTaskcompleted),
                        new SqlParameter("@iAsanaTaskID", m_iAsanaTaskID)
                  };

                //### Add
                m_iTaskID = (int)clsDataAccess.ExecuteScalar("spTasksInsert", sqlParametersInsert);
            }
            else
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iTaskID", m_iTaskID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@iProjectID", m_iProjectID),
                         new SqlParameter("@strTaskName", m_strTaskName),
                         new SqlParameter("@strTaskNotes", m_strTaskNotes),
                         new SqlParameter("@strDuration", m_strDuration),
                         new SqlParameter("@dtTimeSessionElapsed", m_dtTimeSessionElapsed),
                         new SqlParameter("@bIsTaskcompleted", m_bIsTaskcompleted),
                         new SqlParameter("@iAsanaTaskID", m_iAsanaTaskID)
                    };
                //### Update
                clsDataAccess.Execute("spTasksUpdate", sqlParametersUpdate);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void Delete(int iTaskID)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iTaskID", iTaskID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spTasksDelete", sqlParameter);
    }

    public static DataTable GetTasksList()
    {
        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        return clsDataAccess.GetDataTable("spTasksList", EmptySqlParameter);
    }

    public static DataTable GetTasksList(string strFilterExpression, string strSortExpression)
    {
        DataView dvTasksList = new DataView();

        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        dvTasksList = clsDataAccess.GetDataView("spTasksList", EmptySqlParameter);
        dvTasksList.RowFilter = strFilterExpression;
        dvTasksList.Sort = strSortExpression;

        return dvTasksList.ToTable();
    }
    #endregion

    #region PUBLIC LOCAL METHODS

    public virtual void UpdateLocal()
    {
        try
        {
            if (iTaskID == 0)
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@iProjectID", m_iProjectID),
                        new SqlParameter("@strTaskName", m_strTaskName),
                        new SqlParameter("@strTaskNotes", m_strTaskNotes),
                        new SqlParameter("@strDuration", m_strDuration),
                        new SqlParameter("@dtTimeSessionElapsed", m_dtTimeSessionElapsed),
                        new SqlParameter("@bIsTaskcompleted", m_bIsTaskcompleted),
                        new SqlParameter("@iAsanaTaskID", m_iAsanaTaskID)
                  };

                //### Add
                m_iTaskID = (int)clsDataAccessLocal.ExecuteScalar("spTasksInsert", sqlParametersInsert);
            }
            else
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iTaskID", m_iTaskID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@iProjectID", m_iProjectID),
                         new SqlParameter("@strTaskName", m_strTaskName),
                         new SqlParameter("@strTaskNotes", m_strTaskNotes),
                         new SqlParameter("@strDuration", m_strDuration),
                         new SqlParameter("@dtTimeSessionElapsed", m_dtTimeSessionElapsed),
                         new SqlParameter("@bIsTaskcompleted", m_bIsTaskcompleted),
                         new SqlParameter("@iAsanaTaskID", m_iAsanaTaskID)
                    };
                //### Update
                clsDataAccessLocal.Execute("spTasksUpdate", sqlParametersUpdate);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void DeleteLocal(int iTaskID)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iTaskID", iTaskID)
        };
        //### Executes delete sp
        clsDataAccessLocal.Execute("spTasksDelete", sqlParameter);
    }

    public static DataTable GetTasksListLocal()
    {
        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        return clsDataAccessLocal.GetDataTable("spTasksList", EmptySqlParameter);
    }

    public static DataTable GetTasksListLocal(string strFilterExpression, string strSortExpression)
    {
        DataView dvTasksList = new DataView();

        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        dvTasksList = clsDataAccessLocal.GetDataView("spTasksList", EmptySqlParameter);
        dvTasksList.RowFilter = strFilterExpression;
        dvTasksList.Sort = strSortExpression;

        return dvTasksList.ToTable();
    }
    #endregion

    #region PROTECTED METHODS

    protected virtual void GetData()
    {
        try
        {
            //### Populate
            SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iTaskID", m_iTaskID)
                        };
            DataRow drRecord = clsDataAccess.GetRecord("spTasksGetRecord", sqlParameter);

            m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
            m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

            if (drRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

            if (drRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);


            m_iProjectID = Convert.ToInt32(drRecord["iProjectID"]);
            m_strTaskName = drRecord["strTaskName"].ToString();
            m_strTaskNotes = drRecord["strTaskNotes"].ToString();
            m_strDuration = drRecord["strDuration"].ToString();
            m_dtTimeSessionElapsed = Convert.ToDateTime(drRecord["dtTimeSessionElapsed"]);
            m_bIsTaskcompleted = Convert.ToBoolean(drRecord["bIsTaskcompleted"]);
                m_iAsanaTaskID = Convert.ToInt32(drRecord["iAsanaTaskID"]);
            m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    #region PROTECTED LOCAL METHODS

    protected virtual void GetDataLocal()
    {
        try
        {
            //### Populate
            SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iTaskID", m_iTaskID)
                        };
            DataRow drRecord = clsDataAccessLocal.GetRecord("spTasksGetRecord", sqlParameter);

            m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
            m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

            if (drRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

            if (drRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);


            m_iProjectID = Convert.ToInt32(drRecord["iProjectID"]);
            m_strTaskName = drRecord["strTaskName"].ToString();
            m_strTaskNotes = drRecord["strTaskNotes"].ToString();
            m_strDuration = drRecord["strDuration"].ToString();
            m_dtTimeSessionElapsed = Convert.ToDateTime(drRecord["dtTimeSessionElapsed"]);
            m_bIsTaskcompleted = Convert.ToBoolean(drRecord["bIsTaskcompleted"]);
            m_iAsanaTaskID = Convert.ToInt32(drRecord["iAsanaTaskID"]);
            m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
}