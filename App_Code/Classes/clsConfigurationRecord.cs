
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsConfiguration
/// </summary>
public class clsConfiguration
{
    #region MEMBER VARIABLES

    private int m_iConfigurationID;
    private DateTime m_dtAdded;
    private int m_iAddedBy;
    private DateTime m_dtEdited;
    private int m_iEditedBy;
    private bool m_b10Minutes;
    private bool m_b30Minutes;
    private bool m_b1Hour;
    private bool m_b2Hours;
    private bool m_b5Hours;

    #endregion

    #region PROPERTIES

    public int iConfigurationID
    {
        get
        {
            return m_iConfigurationID;
        }
    }

    public DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public bool b10Minutes
    {
        get
        {
            return m_b10Minutes;
        }
        set
        {
            m_b10Minutes = value;
        }
    }

    public bool b30Minutes
    {
        get
        {
            return m_b30Minutes;
        }
        set
        {
            m_b30Minutes = value;
        }
    }

    public bool b1Hour
    {
        get
        {
            return m_b1Hour;
        }
        set
        {
            m_b1Hour = value;
        }
    }

    public bool b2Hours
    {
        get
        {
            return m_b2Hours;
        }
        set
        {
            m_b2Hours = value;
        }
    }

    public bool b5Hours
    {
        get
        {
            return m_b5Hours;
        }
        set
        {
            m_b5Hours = value;
        }
    }


    #endregion

    #region CONSTRUCTORS

    public clsConfiguration()
    {
        m_iConfigurationID = 0;
    }

    public clsConfiguration(int iConfigurationID)
    {
        m_iConfigurationID = iConfigurationID;
        GetData();
    }

    public clsConfiguration(int iConfigurationID, bool bIsLocal)
    {
        m_iConfigurationID = iConfigurationID;
        GetDataLocal();
    }

    #endregion

    #region PUBLIC METHODS

    public virtual void Update()
    {
        try
        {
            if (iConfigurationID == 0)
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@b10Minutes", m_b10Minutes),
                        new SqlParameter("@b30Minutes", m_b30Minutes),
                        new SqlParameter("@b1Hour", m_b1Hour),
                        new SqlParameter("@b2Hours", m_b2Hours),
                        new SqlParameter("@b5Hours", m_b5Hours)                
                  };

                //### Add
                m_iConfigurationID = (int)clsDataAccess.ExecuteScalar("spConfigurationInsert", sqlParametersInsert);
            }
            else
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iConfigurationID", m_iConfigurationID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@b10Minutes", m_b10Minutes),
                        new SqlParameter("@b30Minutes", m_b30Minutes),
                        new SqlParameter("@b1Hour", m_b1Hour),
                        new SqlParameter("@b2Hours", m_b2Hours),
                        new SqlParameter("@b5Hours", m_b5Hours)  
                    };
                //### Update
                clsDataAccess.Execute("spConfigurationUpdate", sqlParametersUpdate);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void Delete(int iConfigurationID)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iConfigurationID", iConfigurationID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spConfigurationDelete", sqlParameter);
    }

    public static DataTable GetTasksList()
    {
        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        return clsDataAccess.GetDataTable("spConfigurationList", EmptySqlParameter);
    }

    public static DataTable GetTasksList(string strFilterExpression, string strSortExpression)
    {
        DataView dvTasksList = new DataView();

        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        dvTasksList = clsDataAccess.GetDataView("spConfigurationList", EmptySqlParameter);
        dvTasksList.RowFilter = strFilterExpression;
        dvTasksList.Sort = strSortExpression;

        return dvTasksList.ToTable();
    }
    #endregion

    #region PUBLIC LOCAL METHODS

    public virtual void UpdateLocal()
    {
        try
        {
            if (iConfigurationID == 0)
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@b10Minutes", m_b10Minutes),
                        new SqlParameter("@b30Minutes", m_b30Minutes),
                        new SqlParameter("@b1Hour", m_b1Hour),
                        new SqlParameter("@b2Hours", m_b2Hours),
                        new SqlParameter("@b5Hours", m_b5Hours)                
                  };

                //### Add
                m_iConfigurationID = (int)clsDataAccessLocal.ExecuteScalar("spConfigurationInsert", sqlParametersInsert);
            }
            else
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iConfigurationID", m_iConfigurationID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@b10Minutes", m_b10Minutes),
                        new SqlParameter("@b30Minutes", m_b30Minutes),
                        new SqlParameter("@b1Hour", m_b1Hour),
                        new SqlParameter("@b2Hours", m_b2Hours),
                        new SqlParameter("@b5Hours", m_b5Hours)  
                    };
                //### Update
                clsDataAccessLocal.Execute("spConfigurationUpdate", sqlParametersUpdate);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void DeleteLocal(int iConfigurationID)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iConfigurationID", iConfigurationID)
        };
        //### Executes delete sp
        clsDataAccessLocal.Execute("spConfigurationDelete", sqlParameter);
    }

    public static DataTable GetTasksListLocal()
    {
        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        return clsDataAccessLocal.GetDataTable("spConfigurationList", EmptySqlParameter);
    }

    public static DataTable GetTasksListLocal(string strFilterExpression, string strSortExpression)
    {
        DataView dvTasksList = new DataView();

        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        dvTasksList = clsDataAccessLocal.GetDataView("spConfigurationList", EmptySqlParameter);
        dvTasksList.RowFilter = strFilterExpression;
        dvTasksList.Sort = strSortExpression;

        return dvTasksList.ToTable();
    }
    #endregion

    #region PROTECTED METHODS

    protected virtual void GetData()
    {
        try
        {
            //### Populate
            SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iConfigurationID", m_iConfigurationID)
                        };
            DataRow drRecord = clsDataAccess.GetRecord("spConfigurationGetRecord", sqlParameter);

            m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
            m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

            if (drRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

            if (drRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

            m_b10Minutes = Convert.ToBoolean(drRecord["b10Minutes"]);
            m_b30Minutes = Convert.ToBoolean(drRecord["b30Minutes"]);
            m_b1Hour = Convert.ToBoolean(drRecord["b1Hour"]);
            m_b2Hours = Convert.ToBoolean(drRecord["b2Hours"]);
            m_b5Hours = Convert.ToBoolean(drRecord["b5Hours"]);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    #region PROTECTED LOCAL METHODS

    protected virtual void GetDataLocal()
    {
        try
        {
            //### Populate
            SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iConfigurationID", m_iConfigurationID)
                        };
            DataRow drRecord = clsDataAccessLocal.GetRecord("spConfigurationGetRecord", sqlParameter);

            m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
            m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

            if (drRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

            if (drRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

            m_b10Minutes = Convert.ToBoolean(drRecord["b10Minutes"]);
            m_b30Minutes = Convert.ToBoolean(drRecord["b30Minutes"]);
            m_b1Hour = Convert.ToBoolean(drRecord["b1Hour"]);
            m_b2Hours = Convert.ToBoolean(drRecord["b2Hours"]);
            m_b5Hours = Convert.ToBoolean(drRecord["b5Hours"]);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
}