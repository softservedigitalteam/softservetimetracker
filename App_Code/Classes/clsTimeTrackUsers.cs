﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsUsersRecord
/// </summary>
public class clsTimeTrackUsers
{
    #region MEMBER VARIABLES

    private int m_iTimeTrackUserID;

    private DateTime m_dtAdded;
    private int m_iAddedBy;
    private DateTime m_dtEdited;
    private int m_iEditedBy;
    private int m_iConfigurationID;
    private String m_strFirstName;
    private String m_strSurname;
    private String m_strPassword;
    private String m_strEmailAddress;
    private String m_strClientID;
    private String m_strClientSecret;
    private String m_strRedirectURL;
    private String m_strRefreshToken;
    private String m_strAssigneeID;
    private bool m_bIsDeleted;
    
    #endregion

    #region PROPERTIES

    public int iTimeTrackUserID
    {
        get
        {
            return m_iTimeTrackUserID;
        }
    }
    public DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }
    public int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }
    public DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }
    public int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public int iConfigurationID
    {
        get
        {
            return m_iConfigurationID;
        }
        set
        {
            m_iConfigurationID = value;
        }
    }

    public String strFirstName
    {
        get
        {
            return m_strFirstName;
        }
        set
        {
            m_strFirstName = value;
        }
    }
    public String strSurname
    {
        get
        {
            return m_strSurname;
        }
        set
        {
            m_strSurname = value;
        }
    }
    public String strPassword
    {
        get
        {
            return m_strPassword;
        }
        set
        {
            m_strPassword = value;
        }
    }
    public String strEmailAddress
    {
        get
        {
            return m_strEmailAddress;
        }
        set
        {
            m_strEmailAddress = value;
        }
    }

    public String strClientID
    {
        get
        {
            return m_strClientID;
        }
        set
        {
            m_strClientID = value;
        }
    }

    public String strClientSecret
    {
        get
        {
            return m_strClientSecret;
        }
        set
        {
            m_strClientSecret = value;
        }
    }
    

    public String strRedirectURL
    {
        get
        {
            return m_strRedirectURL;
        }
        set
        {
            m_strRedirectURL = value;
        }
    }

    public String strRefreshToken
    {
        get
        {
            return m_strRefreshToken;
        }
        set
        {
            m_strRefreshToken = value;
        }
    }
    
        public String strAssigneeID
    {
        get
        {
            return m_strAssigneeID;
        }
        set
        {
            m_strAssigneeID = value;
        }
    }
    public bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }

    #endregion

    #region CONSTRUCTORS

    public clsTimeTrackUsers()
    {
        m_iTimeTrackUserID = 0;
    }

    public clsTimeTrackUsers(int iTimeTrackUserID)
    {
        m_iTimeTrackUserID = iTimeTrackUserID;
        GetData();
    }

    public clsTimeTrackUsers(string strUsername, string strPassword)
    {
        //### Assign values to the parameter list for each corresponding column in the DB 
        SqlParameter[] sqlParametersInit = new SqlParameter[] 
                { 
                    new SqlParameter("@strEmail", strUsername), 
                    new SqlParameter("@strPassword", strPassword) 
                };

        DataRow drLoginRecord = clsDataAccess.GetRecord("spInitialiseTimeTrackUser", sqlParametersInit);

        if (drLoginRecord != null)
        {
            m_iTimeTrackUserID = Convert.ToInt32(drLoginRecord["iTimeTrackUserID"]);
            m_dtAdded = Convert.ToDateTime(drLoginRecord["dtAdded"]);
            m_iAddedBy = Convert.ToInt32(drLoginRecord["iAddedBy"]);
            m_iConfigurationID = Convert.ToInt32(drLoginRecord["iConfigurationID"]);

            if (drLoginRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(drLoginRecord["dtEdited"]);

            if (drLoginRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(drLoginRecord["iEditedBy"]);
            m_iConfigurationID = Convert.ToInt32(drLoginRecord["iConfigurationID"]);
            m_strFirstName = drLoginRecord["strFirstName"].ToString();
            m_strSurname = drLoginRecord["strSurname"].ToString();
            m_strPassword = drLoginRecord["strPassword"].ToString();
            m_strEmailAddress = drLoginRecord["strEmailAddress"].ToString();
            m_strClientID = drLoginRecord["strClientID"].ToString();
            m_strClientSecret = drLoginRecord["strClientSecret"].ToString();
             m_strRedirectURL = drLoginRecord["strRedirectURL"].ToString(); 
             m_strRefreshToken = drLoginRecord["strRefreshToken"].ToString();
            m_strAssigneeID = drLoginRecord["strAssigneeID"].ToString();
            m_bIsDeleted = Convert.ToBoolean(drLoginRecord["bIsDeleted"]);
        }
        else
            throw new Exception("Invalid User Login Credentials");
    }

    public clsTimeTrackUsers(string strUsername, string strPassword, bool bIsInitial)
    {
        //### Assign values to the parameter list for each corresponding column in the DB 
        SqlParameter[] sqlParametersInit = new SqlParameter[] 
                { 
                    new SqlParameter("@strEmail", strUsername), 
                    new SqlParameter("@strPassword", strPassword) 
                };

        DataRow drLoginRecord = clsDataAccess.GetRecord("spInitialiseTimeTrackUser", sqlParametersInit);

        if (drLoginRecord != null)
        {
            m_iTimeTrackUserID = Convert.ToInt32(drLoginRecord["iTimeTrackUserID"]);
            m_dtAdded = Convert.ToDateTime(drLoginRecord["dtAdded"]);
            m_iAddedBy = Convert.ToInt32(drLoginRecord["iAddedBy"]);

            if (drLoginRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(drLoginRecord["dtEdited"]);

            if (drLoginRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(drLoginRecord["iEditedBy"]);

            m_iConfigurationID = Convert.ToInt32(drLoginRecord["iConfigurationID"]);
            m_strFirstName = drLoginRecord["strFirstName"].ToString();
            m_strSurname = drLoginRecord["strSurname"].ToString();
            m_strPassword = drLoginRecord["strPassword"].ToString();
            m_strEmailAddress = drLoginRecord["strEmailAddress"].ToString();
            m_strClientID = drLoginRecord["strClientID"].ToString();
            m_strClientSecret = drLoginRecord["strClientSecret"].ToString();
             m_strRedirectURL = drLoginRecord["strRedirectURL"].ToString();
            m_strRefreshToken = drLoginRecord["strRefreshToken"].ToString();
            m_strAssigneeID = drLoginRecord["strAssigneeID"].ToString();
            m_bIsDeleted = Convert.ToBoolean(drLoginRecord["bIsDeleted"]);
        }
        else
            throw new Exception("Invalid User Login Credentials");
    }

    public clsTimeTrackUsers(string strUsername, bool bIsLocal)
    {
        //### Assign values to the parameter list for each corresponding column in the DB 
        SqlParameter[] sqlParametersInit = new SqlParameter[] 
                { 
                    new SqlParameter("@strEmail", strUsername), 
                };

        DataRow drLoginRecord = clsDataAccessLocal.GetRecord("spInitialiseTimeTrackUserOffline", sqlParametersInit);

        if (drLoginRecord != null)
        {
            m_iTimeTrackUserID = Convert.ToInt32(drLoginRecord["iTimeTrackUserID"]);
            m_dtAdded = Convert.ToDateTime(drLoginRecord["dtAdded"]);
            m_iAddedBy = Convert.ToInt32(drLoginRecord["iAddedBy"]);

            if (drLoginRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(drLoginRecord["dtEdited"]);

            if (drLoginRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(drLoginRecord["iEditedBy"]);

            m_iConfigurationID = Convert.ToInt32(drLoginRecord["iConfigurationID"]);
            m_strFirstName = drLoginRecord["strFirstName"].ToString();
            m_strSurname = drLoginRecord["strSurname"].ToString();
            m_strPassword = drLoginRecord["strPassword"].ToString();
            m_strEmailAddress = drLoginRecord["strEmailAddress"].ToString();
            m_strClientID = drLoginRecord["strClientID"].ToString();
            m_strClientSecret = drLoginRecord["strClientSecret"].ToString();
            m_strRedirectURL = drLoginRecord["strRedirectURL"].ToString();
            m_strRefreshToken = drLoginRecord["strRefreshToken"].ToString();
            m_strAssigneeID = drLoginRecord["strAssigneeID"].ToString();
            m_bIsDeleted = Convert.ToBoolean(drLoginRecord["bIsDeleted"]);
        }
        else
            throw new Exception("Invalid User Login Credentials");
    }

    public clsTimeTrackUsers(string strUsername, string strPassword, bool bIsLocal, bool bIsInitial)
    {
        //### Assign values to the parameter list for each corresponding column in the DB 
        SqlParameter[] sqlParametersInit = new SqlParameter[] 
                { 
                    new SqlParameter("@strEmail", strUsername), 
                    new SqlParameter("@strPassword", strPassword) 
                };

        DataRow drLoginRecord = clsDataAccessLocal.GetRecord("spInitialiseTimeTrackUser", sqlParametersInit);

        if (drLoginRecord != null)
        {
            m_iTimeTrackUserID = Convert.ToInt32(drLoginRecord["iTimeTrackUserID"]);
            m_dtAdded = Convert.ToDateTime(drLoginRecord["dtAdded"]);
            m_iAddedBy = Convert.ToInt32(drLoginRecord["iAddedBy"]);

            if (drLoginRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(drLoginRecord["dtEdited"]);

            if (drLoginRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(drLoginRecord["iEditedBy"]);

            m_iConfigurationID = Convert.ToInt32(drLoginRecord["iConfigurationID"]);
            m_strFirstName = drLoginRecord["strFirstName"].ToString();
            m_strSurname = drLoginRecord["strSurname"].ToString();
            m_strPassword = drLoginRecord["strPassword"].ToString();
            m_strEmailAddress = drLoginRecord["strEmailAddress"].ToString();
            m_strClientID = drLoginRecord["strClientID"].ToString();
            m_strClientSecret = drLoginRecord["strClientSecret"].ToString();
            m_strRedirectURL = drLoginRecord["strRedirectURL"].ToString();
            m_strRefreshToken = drLoginRecord["strRefreshToken"].ToString();
            m_strAssigneeID = drLoginRecord["strAssigneeID"].ToString();
            m_bIsDeleted = Convert.ToBoolean(drLoginRecord["bIsDeleted"]);
        }
        else
            throw new Exception("Invalid User Login Credentials");
    }

    #endregion

    #region PUBLIC METHODS

    public virtual void Update()
    {
        try
        {
            if (iTimeTrackUserID == 0)
            {
                //### Assign values to the parameter list for each corresponding column in the DB 
                SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                { 
                    new SqlParameter("@dtAdded", m_dtAdded), 
                    new SqlParameter("@iAddedBy", m_iAddedBy), 
                    new SqlParameter("@iConfigurationID", m_iConfigurationID), 
                    new SqlParameter("@strFirstName", m_strFirstName), 
                    new SqlParameter("@strSurname", m_strSurname), 
                    new SqlParameter("@strEmailAddress", m_strEmailAddress), 
                    new SqlParameter("@strPassword", m_strPassword),
                    new SqlParameter("@strClientID", m_strClientID),
                         new SqlParameter("@strClientSecret", m_strClientSecret),
                         new SqlParameter("@strRedirectURL", m_strRedirectURL),
                         new SqlParameter("@strRefreshToken", m_strRefreshToken),
                         new SqlParameter("@strAssigneeID", m_strAssigneeID)
                };

                //### Add
                m_iTimeTrackUserID = (int)clsDataAccess.ExecuteScalar("spTimeTrackUsersInsert", sqlParametersInsert);
            }
            else
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                { 
                    new SqlParameter("@iTimeTrackUserID", m_iTimeTrackUserID),
                    new SqlParameter("@dtEdited", m_dtEdited), 
                    new SqlParameter("@iEditedBy", m_iEditedBy), 
                    new SqlParameter("@iConfigurationID", m_iConfigurationID), 
                    new SqlParameter("@strFirstName", m_strFirstName), 
                    new SqlParameter("@strSurname", m_strSurname), 
                    new SqlParameter("@strEmailAddress", m_strEmailAddress), 
                    new SqlParameter("@strPassword", m_strPassword),
                    new SqlParameter("@strClientID", m_strClientID),
                         new SqlParameter("@strClientSecret", m_strClientSecret),
                         new SqlParameter("@strRedirectURL", m_strRedirectURL),
                         new SqlParameter("@strRefreshToken", m_strRefreshToken),
                         new SqlParameter("@strAssigneeID", m_strAssigneeID)
                };

                //### Update
                clsDataAccess.Execute("spTimeTrackUsersUpdate", sqlParametersUpdate);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void Delete(int iTimeTrackUserID)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[] 
                { 
                    new SqlParameter("@iTimeTrackUserID", iTimeTrackUserID)
                };

        //### Executes delete sp
        clsDataAccess.Execute("spTimeTrackUsersDelete", sqlParameter);
    }

    public static DataTable GetUsersList()
    {
        SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
        return clsDataAccess.GetDataTable("spTimeTrackUsersList", EmptySqlParameter);
    }

    public static DataTable GetUsersList(string strFilterExpression, string strSortExpression)
    {
        DataView dvUsersList = new DataView();

        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };

        dvUsersList = clsDataAccess.GetDataView("spTimeTrackUsersList", EmptySqlParameter);

        dvUsersList.RowFilter = strFilterExpression;
        dvUsersList.Sort = strSortExpression;

        return dvUsersList.ToTable();
    }

    #endregion

    #region PUBLIC LOCAL METHODS

    public virtual void UpdateLocal()
    {
        try
        {
            if (iTimeTrackUserID == 0)
            {
                //### Assign values to the parameter list for each corresponding column in the DB 
                SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                { 
                    new SqlParameter("@dtAdded", m_dtAdded), 
                    new SqlParameter("@iAddedBy", m_iAddedBy), 
                    new SqlParameter("@iConfigurationID", m_iConfigurationID), 
                    new SqlParameter("@strFirstName", m_strFirstName), 
                    new SqlParameter("@strSurname", m_strSurname), 
                    new SqlParameter("@strEmailAddress", m_strEmailAddress), 
                    new SqlParameter("@strPassword", m_strPassword),
                    new SqlParameter("@strClientID", m_strClientID),
                         new SqlParameter("@strClientSecret", m_strClientSecret),
                         new SqlParameter("@strRedirectURL", m_strRedirectURL),
                         new SqlParameter("@strRefreshToken", m_strRefreshToken),
                         new SqlParameter("@strAssigneeID", m_strAssigneeID)
                };

                //### Add
                m_iTimeTrackUserID = (int)clsDataAccessLocal.ExecuteScalar("spTimeTrackUsersInsert", sqlParametersInsert);
            }
            else
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                { 
                    new SqlParameter("@iTimeTrackUserID", m_iTimeTrackUserID),
                    new SqlParameter("@dtEdited", m_dtEdited), 
                    new SqlParameter("@iEditedBy", m_iEditedBy), 
                    new SqlParameter("@iConfigurationID", m_iConfigurationID), 
                    new SqlParameter("@strFirstName", m_strFirstName), 
                    new SqlParameter("@strSurname", m_strSurname), 
                    new SqlParameter("@strEmailAddress", m_strEmailAddress), 
                    new SqlParameter("@strPassword", m_strPassword),
                    new SqlParameter("@strClientID", m_strClientID),
                         new SqlParameter("@strClientSecret", m_strClientSecret),
                         new SqlParameter("@strRedirectURL", m_strRedirectURL),
                         new SqlParameter("@strRefreshToken", m_strRefreshToken),
                         new SqlParameter("@strAssigneeID", m_strAssigneeID)
                };

                //### Update
                clsDataAccessLocal.Execute("spTimeTrackUsersUpdate", sqlParametersUpdate);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void DeleteLocal(int iTimeTrackUserID)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[] 
                { 
                    new SqlParameter("@iTimeTrackUserID", iTimeTrackUserID)
                };

        //### Executes delete sp
        clsDataAccessLocal.Execute("spTimeTrackUsersDelete", sqlParameter);
    }

    public static DataTable GetUsersListLocal()
    {
        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        return clsDataAccessLocal.GetDataTable("spTimeTrackUsersList", EmptySqlParameter);
    }

    public static DataTable GetUsersListLocal(string strFilterExpression, string strSortExpression)
    {
        DataView dvUsersList = new DataView();

        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };

        dvUsersList = clsDataAccessLocal.GetDataView("spTimeTrackUsersList", EmptySqlParameter);

        dvUsersList.RowFilter = strFilterExpression;
        dvUsersList.Sort = strSortExpression;

        return dvUsersList.ToTable();
    }

    #endregion

    #region PROTECTED METHODS

    protected void GetData()
    {
        try
        {
            //### Populate
            SqlParameter[] sqlParameter = new SqlParameter[] 
                { 
                    new SqlParameter("@iTimeTrackUserID", m_iTimeTrackUserID)
                };

            DataRow dtrRecord = clsDataAccess.GetRecord("spTimeTrackUsersGetRecord", sqlParameter);

            m_dtAdded = Convert.ToDateTime(dtrRecord["dtAdded"]);
            m_iAddedBy = Convert.ToInt32(dtrRecord["iAddedBy"]);

            if (dtrRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(dtrRecord["dtEdited"]);

            if (dtrRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(dtrRecord["iEditedBy"]);

            m_iConfigurationID = Convert.ToInt32(dtrRecord["iConfigurationID"]);
            m_strFirstName = dtrRecord["strFirstName"].ToString();
            m_strSurname = dtrRecord["strSurname"].ToString();
            m_strPassword = dtrRecord["strPassword"].ToString();
            m_strEmailAddress = dtrRecord["strEmailAddress"].ToString();
            m_strClientID = dtrRecord["strClientID"].ToString();
            m_strClientSecret = dtrRecord["strClientSecret"].ToString();
             m_strRedirectURL = dtrRecord["strRedirectURL"].ToString();
            m_strRefreshToken = dtrRecord["strRefreshToken"].ToString();
            m_strAssigneeID = dtrRecord["strAssigneeID"].ToString();
            m_bIsDeleted = Convert.ToBoolean(dtrRecord["bIsDeleted"]);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion

    #region PROTECTED LOCAL METHODS

    protected void GetDataLocal()
    {
        try
        {
            //### Populate
            SqlParameter[] sqlParameter = new SqlParameter[] 
                { 
                    new SqlParameter("@iTimeTrackUserID", m_iTimeTrackUserID)
                };

            DataRow dtrRecord = clsDataAccessLocal.GetRecord("spTimeTrackUsersGetRecord", sqlParameter);

            m_dtAdded = Convert.ToDateTime(dtrRecord["dtAdded"]);
            m_iAddedBy = Convert.ToInt32(dtrRecord["iAddedBy"]);

            if (dtrRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(dtrRecord["dtEdited"]);

            if (dtrRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(dtrRecord["iEditedBy"]);

            m_iConfigurationID = Convert.ToInt32(dtrRecord["iConfigurationID"]);
            m_strFirstName = dtrRecord["strFirstName"].ToString();
            m_strSurname = dtrRecord["strSurname"].ToString();
            m_strPassword = dtrRecord["strPassword"].ToString();
            m_strEmailAddress = dtrRecord["strEmailAddress"].ToString();
            m_strClientID = dtrRecord["strClientID"].ToString();
            m_strClientSecret = dtrRecord["strClientSecret"].ToString();
            m_strRedirectURL = dtrRecord["strRedirectURL"].ToString();
            m_strRefreshToken = dtrRecord["strRefreshToken"].ToString();
            m_strAssigneeID = dtrRecord["strAssigneeID"].ToString();
            m_bIsDeleted = Convert.ToBoolean(dtrRecord["bIsDeleted"]);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion

}