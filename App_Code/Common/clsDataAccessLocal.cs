﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for clsDataAccessLocal
/// </summary>
public class clsDataAccessLocal
{
	public clsDataAccessLocal()
	{

	}

    /// <summary>
    /// Gets a datatable based on the stored procedure. Use when no filtering is required.
    /// </summary>
    /// <param name="strStoredProcedure">The name of the stored procedure and list of parameters.</param>
    /// <returns>The datatable containing the result.</returns>
    public static DataTable GetDataTable(string strStoredProcedure, SqlParameter[] sqlParametersList)
    {
        DataTable dtReturn = new DataTable();

        using (SqlConnection sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["customConnection"].ConnectionString))
        {
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = strStoredProcedure;
            sqlCommand.Parameters.AddRange(sqlParametersList);
            sqlCommand.Connection = sqlConnection;

            sqlConnection.Open();

            using (SqlDataReader sqlReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection))
            {
                dtReturn.Load(sqlReader);
            }

            sqlConnection.Close();
        }

        return dtReturn;
    }


    /// <summary>
    /// Gets a dataview based on the stored procedure. Use when custom filtering is required.
    /// </summary>
    /// <param name="strStoredProcedure">The name of the stored procedure and list of parameters.</param>
    /// <returns>The dataview containing the result.</returns>
    public static DataView GetDataView(string strStoredProcedure, SqlParameter[] sqlParametersList)
    {
        DataTable dtReturn = new DataTable();

        using (SqlConnection sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["customConnection"].ConnectionString))
        {
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = strStoredProcedure;
            sqlCommand.Parameters.AddRange(sqlParametersList);
            sqlCommand.Connection = sqlConnection;

            sqlConnection.Open();

            using (SqlDataReader sqlReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection))
            {
                dtReturn.Load(sqlReader);
            }

            sqlConnection.Close();
        }

        return dtReturn.DefaultView;
    }

    public static DataSet GetDataSet(string strStoredProcedure, SqlParameter[] sqlParametersList)
    {

        DataSet dsReturn = new DataSet();

        using (SqlConnection sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["customConnection"].ConnectionString))
        {
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = strStoredProcedure;
            sqlCommand.Parameters.AddRange(sqlParametersList);
            sqlCommand.Connection = sqlConnection;
            SqlDataAdapter da = new SqlDataAdapter(sqlCommand);

            sqlConnection.Open();

            da.Fill(dsReturn);

            sqlConnection.Close();
        }

        return dsReturn;
    }

    /// <summary>
    /// Gets a datarow based on the stored procedure. Use when gettig a single record from a table.
    /// </summary>
    /// <param name="strStoredProcedure">The name of the stored procedure and list of parameters.</param>
    /// <returns>The datarow containing the result.</returns>
    public static DataRow GetRecord(string strStoredProcedure, SqlParameter[] sqlParametersList)
    {
        DataTable dtReturn = new DataTable();

        using (SqlConnection sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["customConnection"].ConnectionString))
        {
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = strStoredProcedure;
            sqlCommand.Parameters.AddRange(sqlParametersList);
            sqlCommand.Connection = sqlConnection;

            sqlConnection.Open();

            using (SqlDataReader sqlReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection))
            {
                dtReturn.Load(sqlReader);
            }

            sqlConnection.Close();
        }

        return (dtReturn.Rows.Count > 0 ? dtReturn.Rows[0] : null);
    }

    /// <summary>
    /// Gets a datarow based on the stored procedure. Use when gettig a single record from a table.
    /// </summary>
    /// <param name="strStoredProcedure">The name of the stored procedure and list of parameters.</param>
    /// <returns>The datarow containing the result.</returns>
    public static DataRow GetRecord(string strSQL)
    {
        DataTable dtReturn = new DataTable();

        using (SqlConnection sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["customConnection"].ConnectionString))
        {
            SqlCommand sqlCommand = new SqlCommand();
            //sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = strSQL;
            //sqlCommand.Parameters.AddRange(sqlParametersList);
            sqlCommand.Connection = sqlConnection;

            sqlConnection.Open();

            using (SqlDataReader sqlReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection))
            {
                dtReturn.Load(sqlReader);
            }

            sqlConnection.Close();
        }

        return (dtReturn.Rows.Count > 0 ? dtReturn.Rows[0] : null);
    }

    /// <summary>
    /// Executes a stored procedure and returns the number of rows affected. Use for Updates and Deletes.
    /// </summary>
    /// <param name="strStoredProcedure">The name of the stored procedure and list of parameters.</param>
    /// <returns>The number of affected records.</returns>
    public static int Execute(string strStoredProcedure, SqlParameter[] sqlParametersList)
    {
        int iReturn = 0;

        using (SqlConnection sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["customConnection"].ConnectionString))
        {
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = strStoredProcedure;
            sqlCommand.Parameters.AddRange(sqlParametersList);
            sqlCommand.Connection = sqlConnection;

            sqlConnection.Open();
            iReturn = sqlCommand.ExecuteNonQuery();

            sqlConnection.Close();
        }

        return iReturn;
    }

    /// <summary>
    /// Executes a stored procedure and returns the first column of the first row. Use for Inserts or singe value Selects.
    /// </summary>
    /// <param name="strStoredProcedure">The name of the stored procedure and list of parameters.</param>
    /// <returns>The value found in the first column of the first row.</returns>
    public static object ExecuteScalar(string strStoredProcedure, SqlParameter[] sqlParametersList)
    {
        object objReturn = null;

        using (SqlConnection sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["customConnection"].ConnectionString))
        {
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = strStoredProcedure;
            sqlCommand.Parameters.AddRange(sqlParametersList);
            sqlCommand.Connection = sqlConnection;

            sqlConnection.Open();
            objReturn = sqlCommand.ExecuteScalar();

            sqlConnection.Close();
        }

        return objReturn;
    }
}