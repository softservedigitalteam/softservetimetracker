using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Text;
using System.Net.Mail;
using System.Data.SqlClient;
using System.IO;
using System.Security.Cryptography;
using Encoder = System.Text.Encoder;
using System.Text.RegularExpressions;
using SD = System.Drawing;
using System.Drawing;
using System.Net;


/// <summary>
/// Summary description for clsCommonFunctions
/// </summary>
public class clsCommonFunctions
{
    public clsCommonFunctions()
    {
    }

    #region Admin Settings

    //public static string GetAdminSetting(clsUser clsUser, string strSettingName)
    //{
    //    string strSettingValue = "";

    //    try
    //    {
    //        DataSet dtsData;
    //        dtsData = new DataSet();
    //        dtsData = DataConnection.GetDataObject().GetData("Data", "spGetAdminSetting '" + strSettingName + "'");

    //        strSettingValue = dtsData.Tables["Data"].Rows[0]["strSettingValue"].ToString();

    //        return strSettingValue;

    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //        return null;
    //    }

    //}

    public static long GetFileSize(string strFullPathToFile)
    {
        FileInfo fInfo = new FileInfo(@strFullPathToFile);
        long size = fInfo.Length;
        return size;
    }

    public static void GetAdminSettingForm()
    {
        try
        {
            //### This function should get a list of admin settings, ordered by their index
            //### It should hide the non-visible settings and ONLY show those that we want an
            //### administrator to have control over. Then it should place the information in
            //### appropriate form fields and populate those fields with existing data
        }
        catch (Exception ex)
        {

        }
    }

    #endregion

    #region Javascript Methods

    public static String JSAlert(String Message)
    {
        String strReturn;
        strReturn = "<script language='javascript'>" + "\r\n";
        strReturn += "<!--" + "\r\n";
        strReturn += "alert('" + Message.Replace("'", "").Replace("\r\n", "\\r\\n") + "');" + "\r\n";
        strReturn += "//-->" + "\r\n";
        strReturn += "</script>" + "\r\n";
        return strReturn;
    }

    public static String JSInstruction(String Instruction)
    {
        String strReturn;
        strReturn = "<script language='javascript'>" + "\r\n";
        strReturn += "<!--" + "\r\n";
        strReturn += Instruction + ";\r\n";
        strReturn += "//-->" + "\r\n";
        strReturn += "</script>" + "\r\n";
        return strReturn;
    }

    #endregion

    #region DateTime Methods

    public static double JulianDate(double dblYYYY, double dblMM, double dblDD, double dblHH)
    {
        double dblCenturies, b;
        dblHH = dblHH / 24;
        dblDD = dblDD + dblHH;
        if (dblMM == 1 || dblMM == 2)
        {
            dblMM += 12;
            dblYYYY -= 1;
        }
        dblCenturies = (int)(dblYYYY / 100);
        b = 2 - dblCenturies + (int)(dblCenturies / 4);
        return (int)(365.25 * dblYYYY) + (int)(30.6001 * (dblMM + 1)) + dblDD + 1720994.5 + b;
    }

    public static bool bValidDate(DateTime dtStart, DateTime dtEnd)
    {
        // -1 means the end date is after the start date; 0 means they equal; 1 means the end date is before start date
        int iComparision = dtStart.ToString("dd MMM yyyy").CompareTo(dtEnd.ToString("dd MMM yyyy"));

        if (iComparision == 1)
        {
            return false;
        }
        else
        {
            return true;
        }

        //if ((DateTime)dtStart > (DateTime)dtEnd)
        //{
        //    return false;
        //}
        //else
        //{
        //    return true;
        //}
    }

    public static bool bIsToday(DateTime dtStart, DateTime dtEnd)
    {
        // -1 means the end date is after the start date; 0 means they equal; 1 means the end date is before start date
        int iComparision = dtStart.ToString("dd MMM yyyy").CompareTo(dtEnd.ToString("dd MMM yyyy"));

        if (iComparision == 0)
        {
            return true;
        }
        else
            return false;
    }

    #endregion

    #region Password Methods

    public static string strCreateRandomPassword(int iPasswordLength)
    {
        string strAllowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGH� JKLMNOPQRSTUVWXYZ0123456789";

        Random rNum = new Random();
        string strNewPassWord = "";
        for (int i = 0; i < iPasswordLength; i++)
        {
            strNewPassWord += strAllowedChars[rNum.Next(strAllowedChars.Length)];
        }

        return strNewPassWord;
    }

    public static string GetMd5Sum(string str)
    {
        Encoder enc = Encoding.Unicode.GetEncoder();

        byte[] unicodeText = new byte[str.Length * 2];
        enc.GetBytes(str.ToCharArray(), 0, str.Length, unicodeText, 0, true);

        MD5 md5 = new MD5CryptoServiceProvider();
        byte[] result = md5.ComputeHash(unicodeText);

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < result.Length; i++)
        {
            sb.Append(result[i].ToString("X2"));
        }

        return sb.ToString();
    }

    #endregion

    #region image Methods

    #endregion

    #region String Methods

    public static string GetGeneratedNumber(String strTable, String strField, String strPrefix, Int16 intNumberOfZeros)
    {
        String sql;
        DataRow dtrData;
        String strNewNumber = "";
        String strNewPreNumber;
        String strNewPostNumber;
        Double dblNumber;

        sql = "SELECT TOP 1 * FROM " + strTable + " WHERE " + strField + " Like '" + strPrefix + "%' ORDER BY " + strField + " DESC";

        //dtsData = DataConnection.GetDataObject().GetData("Data", sql);
        dtrData = clsDataAccess.GetRecord(sql);
        if (dtrData != null)
        {
            if (dtrData.ItemArray.Length > 0)
            {
                strNewNumber = dtrData[strField].ToString();
                strNewPreNumber = strNewNumber.Substring(0, strPrefix.Length);
                strNewPostNumber = strNewNumber.Substring(strPrefix.Length, strNewNumber.Length - strPrefix.Length);
                dblNumber = Convert.ToDouble(strNewPostNumber);
                dblNumber = dblNumber + 1;
                strNewNumber = dblNumber.ToString();

                while (strNewNumber.Length != intNumberOfZeros)
                {
                    strNewNumber = "0" + strNewNumber;
                }

                strNewNumber = strPrefix + strNewNumber;
            }
        }
        else
        {
            strNewNumber = "1";
            while (strNewNumber.Length != intNumberOfZeros)
            {
                strNewNumber = "0" + strNewNumber;
            }
            strNewNumber = strPrefix + strNewNumber;
        }
        return strNewNumber;
    }

    public static string escapeChar(string input)
    {
        string escInput = input.Trim();
        escInput = escInput.Replace("'", "\''");

        return escInput;
    }

    #endregion

    #region Misc Methods

    public static bool DoesRecordExist(String strTableName, String strWhereClause)
    {
        int iCount;
        String strSQL;

        if (strWhereClause == "")
        {
            strSQL = "SELECT COUNT(*) AS iRecordCount FROM " + @strTableName;
        }
        else
        {
            strSQL = "SELECT COUNT(*) AS iRecordCount FROM " + @strTableName + " WHERE " + @strWhereClause;
        }

        iCount = (int)clsDataAccess.GetRecord(strSQL)[0];

        if (iCount == 0)
            return false;
        else
            return true;
    }

    public static int returnRecordID(string strColumnName,String strTableName, String strWhereClause)
    {
        int iColunmnID = 0;
        String strSQL;

        if (strWhereClause == "")
        {
            strSQL = "SELECT " + strColumnName + " FROM " + @strTableName;
        }
        else
        {
            strSQL = "SELECT " + strColumnName + " FROM " + @strTableName + " WHERE " + @strWhereClause;
        }

        try
        {

            if (clsDataAccess.GetRecord(strSQL)[0] != null)
                iColunmnID = (int)clsDataAccess.GetRecord(strSQL)[0];
        }
        catch { }

        return iColunmnID;
    }

    public static Int32 GetNumberOfRecords(String strTableName, String strWhereClause)
    {
        int iCount;
        String strSQL;

        if (strWhereClause == "")
        {
            strSQL = "SELECT COUNT(*) AS iRecordCount FROM " + @strTableName;
        }
        else
        {
            strSQL = "SELECT COUNT(*) AS iRecordCount FROM " + @strTableName + " WHERE " + @strWhereClause;
        }

        //iCount = (int)DataConnection.GetDataObject().ExecuteScalar(strSQL);
        iCount = (int)clsDataAccess.GetRecord(strSQL)[0];

        return iCount;
    }

    public static DataSet GetListTopAmountX(string strTableName, string strColumnName, string strForeignKeyColumnName, int iNumberOfRows, int iFKID, string strPKIDList)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
            {
                new SqlParameter("@strTableName", strTableName),
                new SqlParameter("@strColumnName", strColumnName),
                new SqlParameter("@strForeignKeyColumnName", strForeignKeyColumnName),
                new SqlParameter("@iNumberOfRows", iNumberOfRows),
               
                new SqlParameter("@iFKID", iFKID),
                new SqlParameter("@strPKIDList", strPKIDList)
            };
        return clsDataAccess.GetDataSet("spListTop", sqlParameter);
    }

    public static DataTable dtTopAmount(int iRowAmount, DataTable dtSource)
    {
        DataTable dtNew = new DataTable();
        //Copy Stucture not data

        dtNew = dtSource.Clone();

        if (dtSource.Rows.Count >= iRowAmount)
        {
            //dtNew.Rows.Clear();

            for (int i = 0; (i < iRowAmount && i < dtSource.Rows.Count); i++)
            {
                DataRow dtrNew = dtSource.Rows[i];
                dtNew.ImportRow(dtrNew);
            }
        }

        return dtNew;
    }

    #endregion

    #region Validation Methods

    public bool IsValidEmail(string email)
    {
        //regular expression pattern for valid email
        //addresses, allows for the following domains:
        //com,edu,info,gov,int,mil,net,org,biz,name,museum,coop,aero,pro,tv
        string pattern = @"^[-a-zA-Z0-9][-.a-zA-Z0-9]*@[-.a-zA-Z0-9]+(\.[-.a-zA-Z0-9]+)*\.
    (com|edu|info|gov|int|mil|net|org|biz|name|museum|coop|aero|pro|tv|[a-zA-Z]{2})$";
        //Regular expression object
        Regex check = new Regex(pattern, RegexOptions.IgnorePatternWhitespace);
        //boolean variable to return to calling method
        bool valid = false;

        //make sure an email address was provided
        if (string.IsNullOrEmpty(email))
        {
            valid = false;
        }
        else
        {
            //use IsMatch to validate the address
            valid = check.IsMatch(email);
        }
        //return the value to the calling method
        return valid;
    }

    #endregion

    #region Mail Methods

    public static void SendAnEmail(string emailAddress, string fromAddress, string subject, string body, string ccAddress, string bccAddress, string attachment)
    {
        //### Variable Declaration
        MailAddress MailTo = new MailAddress(emailAddress);
        MailAddress MailFrom = new MailAddress(fromAddress);
        MailMessage ObjEmail = new MailMessage();
        SmtpClient ObjEmailSender = new SmtpClient();

        //### Assign the Subject
        ObjEmail.Subject = subject;

        //### Set Email to HTML
        ObjEmail.IsBodyHtml = true;

        //### Add Body To Email
        ObjEmail.Body = body;

        //### Add To Address
        ObjEmail.To.Add(MailTo);

        //### Add From Address
        ObjEmail.From = MailFrom;

        //### Check if cc address is provided
        if (ccAddress != "")
        {
            MailAddress MailCC = new MailAddress(ccAddress);
            ObjEmail.CC.Add(MailCC);
        }
        if (bccAddress != "")
        {
            MailAddress MailBCC = new MailAddress(bccAddress);
            ObjEmail.Bcc.Add(MailBCC);
        }

        //### Check if there is an attachment
        if (attachment != "")
        {
            Attachment MyAttachment = new Attachment(attachment);
            ObjEmail.Attachments.Add(MyAttachment);
        }

        try
        {
            //### Send the Email            
            ObjEmailSender.Send(ObjEmail);
        }
        catch (Exception ex)
        {

        }
    }

    public static void SendMail(string strFrom, string strTo, string strCC, string strBCC, string strSubject, string strContent, Attachment[] atcAttatchedFiles, bool bUseGenericHTML)
    {
        //### Create an instance of the MailMessage class 
        using (MailMessage myMail = new MailMessage())
        {
            //### Set the subject            
            myMail.Subject = strSubject;

            //### Send To / From / BCC
            myMail.From = new MailAddress(strFrom);
            myMail.To.Add(strTo);
            myMail.CC.Add(strCC);

            //### Add Attachments to message object
            if (atcAttatchedFiles != null)
            {
                foreach (Attachment atcAttachedFile in atcAttatchedFiles)
                {
                    myMail.Attachments.Add(atcAttachedFile);
                }
            }

            //### Assign the content to the mail body
            myMail.IsBodyHtml = true;

            string strHtmlBody = "";

            if (bUseGenericHTML == true)
                strHtmlBody = SendMailGenericHTML(strContent, strSubject);
            else
                strHtmlBody += strContent;

            //string strPathToLogo = ConfigurationManager.AppSettings["WebRootFullPath"] + @"\images\Banner.png";

            //AlternateView HTMLEmail = AlternateView.CreateAlternateViewFromString(strHtmlBody, null, "text/html");

            //LinkedResource MyImage = new LinkedResource(strPathToLogo);

            ////### <img src="cid:InlineImageID" />
            //MyImage.ContentId = "logo";

            ////### Add this linked resource to HTML view
            //HTMLEmail.LinkedResources.Add(MyImage);

            //myMail.AlternateViews.Add(HTMLEmail);

            //### Password protected
            SmtpClient emailClient = new SmtpClient("41.185.13.123");
            emailClient.Port = 25;
            emailClient.Credentials = new System.Net.NetworkCredential("noreply@softservedigital.co.za", "N0r3ply123#");

            myMail.Body = strHtmlBody;

            //### Now, to send the message, use the Send method of the SmtpMail class 
            emailClient.Send(myMail);
            myMail.Dispose();
        }
    }

    private static String SendMailGenericHTML(String strContent, String strSubject)
    {
        //### This is a generic email function that accepts the strFrom, strTo, strSubject and strContent
        //### This will simplify the management of email sending

        //### Mail settings for the template
        string strPathToLogo = "";
        string strURL = "http://accelerate2.softservedigital.co.za";
        string strClientName = "Softserve Digital Development";
        string strColor = "#003264";

        StringBuilder strbMailBuilder = new StringBuilder();

        strbMailBuilder.AppendLine("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\"><title>" + strSubject + "</title></head><body style=\"background-color:#f6f5f5;color: " + strColor + "\">");
        strbMailBuilder.AppendLine("<center>");


        //### This code is added here as it is generic throughout email sending
        strbMailBuilder.AppendLine("<table cellpadding=\"2\" cellspacing=\"2\" style=\"font-family:Arial; font-size:12px; width:600px;\">");
        //strbMailBuilder.AppendLine("<tr><td><img src=\"cid:logo\" alt=\"" + strClientName + "\" title=\"" + strClientName + "\" /></td></tr>");
        strbMailBuilder.AppendLine("<tr><td style=\"background:" + strColor + "; color:#f6f5f5; font-weight:bold; padding:10px; text-align: center;\">" + strSubject + "</td></tr>");
        strbMailBuilder.AppendLine("<tr><td style=\"width: 100%; background-color: " + strColor + "; padding: 3px;\"></td></tr>");
        strbMailBuilder.AppendLine("<tr><td>");
        strbMailBuilder.AppendLine(strContent);
        strbMailBuilder.AppendLine("</td></tr>");
        strbMailBuilder.AppendLine("<tr><td style=\"width: 100%; background-color: " + strColor + " padding: 3px;\"></td></tr>");
        strbMailBuilder.AppendLine("<tr><td style=\"font-family:Arial; font-size:14px;\">Yours in Roses,<br /><b><a href=\"" + strURL + "\" style=\"text-decoration:none;color:#000;\">" + strClientName + "</a></b></td></tr>");

        strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("</center>");
        strbMailBuilder.AppendLine("</body></html>");

        return strbMailBuilder.ToString();
    }

    #endregion

    #region Convert Methods


    #endregion


    #region Search
    public static DataSet GetSearchList(int iCurrentRow, string strSearch)
    {
        DataSet dvProductsList = new DataSet();
        SqlParameter[] EmptySqlParameter = new SqlParameter[] {           
            new SqlParameter("@iCurrentRow",iCurrentRow),     
            new SqlParameter("@strSearch",strSearch),
        };

        dvProductsList = clsDataAccess.GetDataSet("spSearch", EmptySqlParameter);
        return dvProductsList;
    }
    #endregion

    #region Facebook
    public static string fbAppID()
    {
        return ConfigurationManager.AppSettings.Get("FacebookAppid");
    }

    public static string fbPageID()
    {
        return ConfigurationManager.AppSettings.Get("FacebookPage");
    }
    #endregion

    #region ERROR MESSAGE

   

    #endregion
}
