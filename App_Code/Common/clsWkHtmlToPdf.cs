﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Diagnostics;

/// <summary>
/// Class to manage wkhtmltopdf.
/// Converts HTML to PDF using the webkit rendering engine
/// </summary>
public class clsWkHtmlToPdf
{
    #region CONSTRUCTOR

    private clsWkHtmlToPdf()
    {

    }

    #endregion

    #region PUBLIC STATIC CONVERT METHODS

    public static byte[] UrlToPdf(String strUrl)
    {
        //### Variables
        Process prcWkhtmltopdf = new Process();

        //### Set properties
        prcWkhtmltopdf.StartInfo.CreateNoWindow = true;
        prcWkhtmltopdf.StartInfo.RedirectStandardOutput = true;
        prcWkhtmltopdf.StartInfo.RedirectStandardError = true;
        prcWkhtmltopdf.StartInfo.RedirectStandardInput = true;
        prcWkhtmltopdf.StartInfo.UseShellExecute = false;

        prcWkhtmltopdf.StartInfo.Arguments ="--print-media-type "
         + "--margin-top 10mm --margin-bottom 10mm --margin-right 10mm --margin-left 10mm "
         + "--page-size Letter --xsl-style-sheet"
         + " " + strUrl + " - ";

        //### Start
        prcWkhtmltopdf.Start();

        //### Load to byte array
        byte[] buffer = new byte[32768];
        byte[] bytPdfFile;

        using (MemoryStream ms = new MemoryStream())
        {
            while (true)
            {
                Int32 iRead = prcWkhtmltopdf.StandardOutput.BaseStream.Read(buffer, 0, buffer.Length);

                if (iRead <= 0)
                    break;

                ms.Write(buffer, 0, iRead);
            }
            ms.Flush();
            bytPdfFile = ms.ToArray();
            ms.Close();
        }

        //### Make sure applcation closes
        prcWkhtmltopdf.WaitForExit(60000);

        //### Handle results
        if (prcWkhtmltopdf.ExitCode != 0)
            throw new Exception("The PDF creation failed. Make sure 'wkhtmltopdf.exe' is in the correct location. '~/wkhtmltopdf/wkhtmltopdf.exe'");
        else
            return bytPdfFile;
    }

    public static byte[] HtmlToPdf(String strHtml)
    {
        //### Variables
        Process prcWkhtmltopdf = new Process();

        //### Set properties
        prcWkhtmltopdf.StartInfo.CreateNoWindow = true;
        prcWkhtmltopdf.StartInfo.RedirectStandardOutput = true;
        prcWkhtmltopdf.StartInfo.RedirectStandardError = true;
        prcWkhtmltopdf.StartInfo.RedirectStandardInput = true;
        prcWkhtmltopdf.StartInfo.UseShellExecute = false;

        prcWkhtmltopdf.StartInfo.Arguments = " --disable-smart-shrinking "
         + "--margin-top 10mm --margin-bottom 10mm --margin-right 10mm --margin-left 10mm "
         + "--page-size A4 "
         + " - - ";

        //### Start
        prcWkhtmltopdf.Start();

        //### Sends HTML
        StreamWriter swPdf = prcWkhtmltopdf.StandardInput;
        swPdf.AutoFlush = true;
        swPdf.Write(strHtml);
        swPdf.Dispose();

        //### Load to byte array
        byte[] buffer = new byte[32768];
        byte[] bytPdfFile;

        using (MemoryStream ms = new MemoryStream())
        {
            while (true)
            {
                Int32 iRead = prcWkhtmltopdf.StandardOutput.BaseStream.Read(buffer, 0, buffer.Length);

                if (iRead <= 0)
                    break;

                ms.Write(buffer, 0, iRead);
            }
            ms.Flush();
            bytPdfFile = ms.ToArray();
            ms.Close();
        }

        //### Make sure applcation closes
        prcWkhtmltopdf.WaitForExit(60000);

        //### Handle results
        if (prcWkhtmltopdf.ExitCode != 0)
        {
            throw new Exception("The PDF creation failed. Make sure 'wkhtmltopdf.exe' is in the correct location. '~/wkhtmltopdf/wkhtmltopdf.exe'");
        }
        else
        {
            return bytPdfFile;
        }
    }

    public static byte[] HtmlToPdf(String strHtml, String strBaseUrl)
    {
        return HtmlToPdf(CorrectPaths(strHtml, strBaseUrl));
    }

    #endregion

    #region PUBLIC IMAGE PATH CORRECTION METHODS

    public static String CorrectPaths(String strHtml, String strBaseUrl)
    {
        DirectoryInfo dirParentDirectory = new DirectoryInfo(strBaseUrl);

        return strHtml.Replace("src=\"../", "src=\"" + dirParentDirectory.Parent.FullName.Replace("\\", "/") + "/");
    }

    #endregion
}
