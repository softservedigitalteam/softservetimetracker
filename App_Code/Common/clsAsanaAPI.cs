﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

    public class clsAsanaAPI
    {
        private string m_strClientID;
        private string m_strClientSecret;
        private string m_strEndPointUserAuthorisation = "https://app.asana.com/-/oauth_authorize";
        private string m_strEndPointTokenExchange = "https://app.asana.com/-/oauth_token";
        private string m_strRedirectURI;
        private string m_strFullUserAuthorisationPath;
        private string m_strResponseCode;
        private string m_strResponseToken;
        private string m_strGrantType;
        
        public string strClientID
        {
            get
            {
                return m_strClientID;
            }
            set
            {
                m_strClientID = value;
            }
        }

        public string strClientSecret
        {
            get
            {
                return m_strClientSecret;
            }
            set
            {
                m_strClientSecret = value;
            }
        }

        public string strEndPointUserAuthorisation
        {
            get
            {
                return m_strEndPointUserAuthorisation;
            }
        }

        public string strEndPointTokenExchange
        {
            get
            {
                return m_strEndPointTokenExchange;
            }
        }

        public string strRedirectURI
        {
            get
            {
                return m_strRedirectURI;
            }
            set
            {
                m_strRedirectURI = value;
            }
        }

        public string strFullUserAuthorisationPath
        {
            get
            {
                return m_strFullUserAuthorisationPath;
            }
            set
            {
                m_strFullUserAuthorisationPath = value;
            }
        }

        public string strResponseCode
        {
            get
            {
                return m_strResponseCode;
            }
            set
            {
                m_strResponseCode = value;
            }
        }

        public string strResponseToken
        {
            get
            {
                return m_strResponseToken;
            }
            set
            {
                m_strResponseToken = value;
            }
        }

        public string strGrantType
        {
            get
            {
                return m_strGrantType;
            }
            set
            {
                m_strGrantType = value;
            }
        }

    #region FUNCTIONS
    //protected async Task getInfo()
    //    {
    //        //Empty String
    //        string code = "";
    //        using (var c = new HttpClient())
    //        {
    //            var values = new Dictionary<string, string>
    //            {
    //                { "grant_type", "authorization_code" },
    //                { "client_id", "166029857452613" },
    //                { "client_secret", "b95b13580c8fbe1735ec4bea6c5ab15c" },
    //                { "redirect_uri", "urn:ietf:wg:oauth:2.0:oob" },
    //                { "code", code }
    //            };
    //            var content = new FormUrlEncodedContent(values);
    //            var response = await c.PostAsync("https://app.asana.com/-/oauth_token", content);

    //            var responseString = await response.Content.ReadAsStringAsync();

    //            var jObj = JObject.Parse(responseString);
    //            var token = jObj.SelectToken("access_token");
    //            var token2 = jObj.SelectToken("refresh_token");
    //            //rt = token.ToString();

    //            int x = 2;
    //        }
    //    }

    //    protected async Task postTask()
    //    {
    //        //Empty String
    //        string code = "";
    //        using (var client = new HttpClient())
    //        {
    //            string token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdXRob3JpemF0aW9uIjoxNjYwMzYzNDMyMDU2NzQsInNjb3BlIjoiIiwiaWF0IjoxNDcxMDA1NDIyLCJleHAiOjE0NzEwMDkwMjJ9.jEGQT9w7lhUAiELHU9t8zC27WJaskTkhQyV3rD-IHRU";
    //            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
    //            //166612865950664

    //            var responseString = await client.GetStringAsync("https://app.asana.com/api/1.0/users/me");
    //            var myInfo = JObject.Parse(responseString);
    //            string myID = myInfo.SelectToken("data.id").ToString();

    //            dynamic workspaceInfo = JsonConvert.DeserializeObject(myInfo.ToString());
    //            JArray jaw = workspaceInfo.data.workspaces;

    //            List<string> Workspaces = new List<string>();
    //            foreach (var j in jaw)
    //            {
    //                Workspaces.Add(j["id"].ToString());
    //            }
    //            string workspaceID = Workspaces[0];


    //            var values = new Dictionary<string, string>
    //            {
    //                { "assignee", myID },
    //                { "name", "HELLOWORLD2" },
    //                { "notes", "My Note" },
    //                { "workspace", workspaceID }
    //            };

    //            var content = new FormUrlEncodedContent(values);
    //            var response = await client.PostAsync("https://app.asana.com/api/1.0/tasks", content);
    //            var responseString2 = await response.Content.ReadAsStringAsync();
    //            int r = 2;
    //        }
    //    }

    //    protected async Task getStuff()
    //    {
    //        using (var client = new HttpClient())
    //        {
    //            string token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdXRob3JpemF0aW9uIjoxNjYwMzYzNDMyMDU2NzQsInNjb3BlIjoiIiwiaWF0IjoxNDcxMDA1NDIyLCJleHAiOjE0NzEwMDkwMjJ9.jEGQT9w7lhUAiELHU9t8zC27WJaskTkhQyV3rD-IHRU";
    //            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
    //            //166612865950664

    //            var responseString = await client.GetStringAsync("https://app.asana.com/api/1.0/users/me");
    //            //txtData.Text = responseString.ToString();

    //            var jObj = JObject.Parse(responseString);
    //            dynamic jsonInfo = JsonConvert.DeserializeObject(jObj.ToString());
    //            JArray ja = jsonInfo.data.workspaces;

    //            List<string> Workspaces = new List<string>();
    //            foreach (var j in ja)
    //            {
    //                Workspaces.Add(j["id"].ToString());
    //            }

    //            string x = Workspaces[0];
    //            int y = 2;


    //            var responseString2 = await client.GetStringAsync("https://app.asana.com/api/1.0/workspaces/" + x + "/projects");
    //            var responseString3 = await client.GetStringAsync("https://app.asana.com/api/1.0/tasks?workspace=" + x + "&assignee=me&completed=true");
    //            var jSonTasks = JObject.Parse(responseString3);
    //            dynamic jSonTasksInfo = JsonConvert.DeserializeObject(jSonTasks.ToString());
    //            JArray ja2 = jSonTasksInfo.data;

    //            List<string> tasksID = new List<string>();
    //            foreach (var jti in ja2)
    //            {
    //                tasksID.Add(jti["id"].ToString());
    //            }

    //            List<Root> root = new List<Root>();
    //            int count = 0;
    //            foreach (var task in tasksID)
    //            {
    //                count++;
    //                var responseString4 = await client.GetStringAsync("https://app.asana.com/api/1.0/tasks/" + task);
    //                var myTask = JObject.Parse(responseString4);
    //                Root myTasksInfo = JsonConvert.DeserializeObject<Root>(myTask.ToString());
    //                root.Add(myTasksInfo);
    //                //if (count==50)
    //                // break;
    //            }

    //            string msg = "";
    //            int index = 1;
    //            foreach (Root r in root)
    //            {
    //                msg += index.ToString() + ": " + r.data.name.ToString() + "\r\n";
    //                index++;
    //            }
    //            //txtData.Text = msg.ToString();
    //            y = 3;
    //        }
    //    }
    #endregion

    public class Root
        {
            public AsanaTasks data { get; set; }
        }
        public class AsanaTasks
        {
            public AsanaTasks()
            {
                followers = new List<Followers>();
            }
            public string id { get; set; }
            public string created_at { get; set; }
            public string modified_at { get; set; }
            public string name { get; set; }
            public string notes { get; set; }
            public bool completed { get; set; }
            public string assignee_status { get; set; }
            public string completed_at { get; set; }
            public string due_on { get; set; }
            public string due_at { get; set; }
            public Workspace workspace;

            public string num_hearts { get; set; }
            public Assignee assignee { get; set; }

            public string parent { get; set; }
            public string[] hearts { get; set; }

            public List<Followers> followers { get; set; }

            public List<Projects> projects { get; set; }
            public string[] tags { get; set; }
            public List<Memberships> memberships { get; set; }
            public bool hearted { get; set; }
        }

    public class MyTasks
    {
        public string iAsanaTaskID { get; set; }
        public DateTime dtAdded { get; set; }
        public int iAddedBy { get; set; }
        public DateTime dtEdited { get; set; }
        public int iEditedBy { get; set; }
        public string strTitle { get; set; }
        public string strDescription { get; set; }
        public string strAsanaTaskLiveID { get; set; }
        public string strAssigneeID { get; set; }
        public bool bIsCompleted { get; set; }
        public DateTime dtCompleted { get; set; }
        public string strWorkspaceID { get; set; }
        public string strAsanaProjectID { get; set; }
        public int iProjectID { get; set; }
        public bool bIsDeleted { get; set; }

        public MyTasks()
        {
            iAsanaTaskID = "";
            dtAdded = DateTime.Now;
            iAddedBy = 0;
            dtEdited = DateTime.Now;
            iEditedBy = 0;
            strTitle = "";
            strDescription = "";
            strAsanaTaskLiveID = "";
            strAssigneeID = "";
            bIsCompleted = false;
            dtCompleted = DateTime.Now;
            strWorkspaceID = "";
            strAsanaProjectID = "";
            iProjectID = 0;
            bIsDeleted = false;
        }
    }

    public class GetAllAsanaTasks
    {
        public List<AllAsanaTasks> data { get; set; }
    }
    public class AllAsanaTasks
    {
        public string id { get; set; }
        public string name { get; set; }
        public string completed { get; set; }
        public string notes { get; set; }
        public string completed_at { get; set; }
        public Assignee assignee { get; set; }
        public Workspace workspace { get; set; }
        public List<Projects> projects { get; set; }
    }

    public class Tasks
        {
            public List<TaskInfo> data { get; set; }
        }

        public class TaskInfo
        {
            public string id { get; set; }
            public string name { get; set; }
            public string completed { get; set; }
            public string notes { get; set; }
            public Workspace workspace { get; set; }
            public List<Projects> projects { get; set; }
        }

        public class Workspace
        {
            public string id { get; set; }
            public string name { get; set; }

            public Workspace()
            {
                id = "";
                name = "";
            }
            
        }

    public class TaskWorkspace
    {
        public string id { get; set; }
    }

    public class Assignee
   {
        public string id { get; set; }
        public string name { get; set; }

        public Assignee()
        {
            id = "";
            name = "";
        }
    }
        public class Followers
        {
            public string id { get; set; }
            public string name { get; set; }
        }
        public class Projects
        {
            public string id { get; set; }
            public string name { get; set; }
            public string notes { get; set; }

            public Projects()
            {
                id = "";
                name = "";
                notes = "";
            }
        }
    public class TaskProjects
    {
        public string id { get; set; }
    }
    public class Sections
        {
            public string id { get; set; }
            public string name { get; set; }
        }
        public class Memberships
        {
            public Projects project { get; set; }
            public Sections section { get; set; }
        }
    }
