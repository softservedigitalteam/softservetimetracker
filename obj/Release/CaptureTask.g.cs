﻿#pragma checksum "..\..\CaptureTask.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "B7F803B09B9155843DB7D6BE0DD31A57"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SoftserveTimeTracker {
    
    
    /// <summary>
    /// CaptureTask
    /// </summary>
    public partial class CaptureTask : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 14 "..\..\CaptureTask.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblTimeSpent;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\CaptureTask.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnContinue;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\CaptureTask.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnMinimize;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\CaptureTask.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtTaskName;
        
        #line default
        #line hidden
        
        
        #line 82 "..\..\CaptureTask.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnStart;
        
        #line default
        #line hidden
        
        
        #line 110 "..\..\CaptureTask.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnFinish;
        
        #line default
        #line hidden
        
        
        #line 137 "..\..\CaptureTask.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxProjects;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SoftserveTimeTracker;component/capturetask.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\CaptureTask.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 5 "..\..\CaptureTask.xaml"
            ((SoftserveTimeTracker.CaptureTask)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            
            #line 5 "..\..\CaptureTask.xaml"
            ((SoftserveTimeTracker.CaptureTask)(target)).StateChanged += new System.EventHandler(this.Window_StateChanged);
            
            #line default
            #line hidden
            return;
            case 2:
            this.lblTimeSpent = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.btnContinue = ((System.Windows.Controls.Button)(target));
            
            #line 20 "..\..\CaptureTask.xaml"
            this.btnContinue.Click += new System.Windows.RoutedEventHandler(this.btnContinue_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.btnMinimize = ((System.Windows.Controls.Button)(target));
            
            #line 47 "..\..\CaptureTask.xaml"
            this.btnMinimize.Click += new System.Windows.RoutedEventHandler(this.btnMinimize_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.txtTaskName = ((System.Windows.Controls.TextBox)(target));
            
            #line 75 "..\..\CaptureTask.xaml"
            this.txtTaskName.GotFocus += new System.Windows.RoutedEventHandler(this.txtTaskName_GotFocus);
            
            #line default
            #line hidden
            
            #line 75 "..\..\CaptureTask.xaml"
            this.txtTaskName.LostFocus += new System.Windows.RoutedEventHandler(this.txtTaskName_LostFocus);
            
            #line default
            #line hidden
            
            #line 75 "..\..\CaptureTask.xaml"
            this.txtTaskName.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txtTaskName_TextChanged);
            
            #line default
            #line hidden
            return;
            case 6:
            this.btnStart = ((System.Windows.Controls.Button)(target));
            
            #line 82 "..\..\CaptureTask.xaml"
            this.btnStart.Click += new System.Windows.RoutedEventHandler(this.btnStart_Click);
            
            #line default
            #line hidden
            
            #line 82 "..\..\CaptureTask.xaml"
            this.btnStart.MouseEnter += new System.Windows.Input.MouseEventHandler(this.btnStart_MouseEnter);
            
            #line default
            #line hidden
            
            #line 82 "..\..\CaptureTask.xaml"
            this.btnStart.GotFocus += new System.Windows.RoutedEventHandler(this.btnStart_GotFocus);
            
            #line default
            #line hidden
            
            #line 82 "..\..\CaptureTask.xaml"
            this.btnStart.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.btnStart_MouseDown);
            
            #line default
            #line hidden
            
            #line 82 "..\..\CaptureTask.xaml"
            this.btnStart.MouseUp += new System.Windows.Input.MouseButtonEventHandler(this.btnStart_MouseUp);
            
            #line default
            #line hidden
            
            #line 82 "..\..\CaptureTask.xaml"
            this.btnStart.MouseLeave += new System.Windows.Input.MouseEventHandler(this.btnStart_MouseLeave);
            
            #line default
            #line hidden
            return;
            case 7:
            this.btnFinish = ((System.Windows.Controls.Button)(target));
            
            #line 110 "..\..\CaptureTask.xaml"
            this.btnFinish.Click += new System.Windows.RoutedEventHandler(this.btnFinish_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.cbxProjects = ((System.Windows.Controls.ComboBox)(target));
            
            #line 137 "..\..\CaptureTask.xaml"
            this.cbxProjects.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.cbxProjects_SelectionChanged);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

