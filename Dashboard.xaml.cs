﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SoftserveTimeTracker
{
    /// <summary>
    /// Interaction logic for Dashboard.xaml
    /// </summary>
    public partial class Dashboard : Window
    {
        public Dashboard()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            populateGridSummary();
        }

        private void populateGridSummary()
        {
            dgrRecentTasks.SetBinding(ItemsControl.ItemsSourceProperty, new System.Windows.Data.Binding { Source = "" });
            DataTable dtRecentTasks = clsTasks.GetTasksList("iAddedBy = " + Properties.Settings.Default["iTimeTrackUserID"].ToString(), "iTaskID DESC");
            DataTable dtSummary = new DataTable();
            dtSummary.Columns.Add("Task", typeof(string));
            dtSummary.Columns.Add("It Took", typeof(string));
            dtSummary.Columns.Add("When", typeof(string));
            int iCount = 1;
            foreach (DataRow dr in dtRecentTasks.Rows)
            {
                if (iCount <= 6)
                {
                    DataRow drRecent = dtSummary.NewRow();
                    drRecent["Task"] = dr["strTaskName"].ToString();
                    TimeSpan tsTimeItTook = ((DateTime)dr["dtTimeSessionElapsed"]) - ((DateTime)dr["dtAdded"]);
                    drRecent["It Took"] = tsTimeItTook.Hours + "h " + tsTimeItTook.Minutes + "m " + tsTimeItTook.Seconds + "s";
                    TimeSpan tsHoursAgo = DateTime.Now - (DateTime)dr["dtTimeSessionElapsed"];
                    int iHours = tsHoursAgo.Hours;
                    drRecent["When"] = iHours + " hr(s) ago";
                    dtSummary.Rows.Add(drRecent);
                    iCount++;
                }
                else
                {
                    break;
                }
            }
            dgrRecentTasks.SetBinding(ItemsControl.ItemsSourceProperty, new System.Windows.Data.Binding { Source = dtSummary });
        }
    }
}
