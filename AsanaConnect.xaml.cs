﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using Newtonsoft.Json.Linq;

namespace SoftserveTimeTracker
{
    /// <summary>
    /// Interaction logic for AsanaConnect.xaml
    /// </summary>
    public partial class AsanaConnect : Window
    {
        clsTimeTrackUsers TimeTrackUser;
        //System.Windows.Forms.Timer popup = new System.Windows.Forms.Timer();
        //bool isStop = false;

        int counter = 1;

        public AsanaConnect()
        {
            InitializeComponent();

            int iTimeTrackUser = Convert.ToInt32(Properties.Settings.Default["iTimeTrackUserID"]);
            TimeTrackUser = new clsTimeTrackUsers(iTimeTrackUser);

            //if (TimeTrackUser.strRefreshToken != "" && TimeTrackUser.strRefreshToken != null)
            //{

            //    if (Properties.Settings.Default["strRefreshToken"].ToString() != TimeTrackUser.strRefreshToken.ToString())
            //    {
            //        Properties.Settings.Default["strRefreshToken"] = TimeTrackUser.strRefreshToken.ToString();
            //        CaptureTask CaptureTask = new CaptureTask();
            //        CaptureTask.Show();
            //        this.Close();
            //    }
            //}
            if (TimeTrackUser != null)
            {
                if (TimeTrackUser.strClientID != "" && TimeTrackUser.strClientID != null)
                {
                    txtClientID.Text = TimeTrackUser.strClientID;
                }
                if (TimeTrackUser.strClientSecret != "" && TimeTrackUser.strClientSecret != null)
                {
                    txtClientSecret.Text = TimeTrackUser.strClientSecret;
                }
                if (TimeTrackUser.strRedirectURL != "" && TimeTrackUser.strRedirectURL != null)
                {
                    chkbxURL.IsChecked = true;
                    txtFullURL.IsEnabled = true;
                    txtFullURL.Text = TimeTrackUser.strRedirectURL;
                }
            }
            //popup.Tick += new EventHandler(TimerEvent);

            // Sets the timer interval to 5 seconds.
            //popup.Interval = 5000;
            //popup.Start();
        }

        private void btnAsana_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://asana.com/");
        }

        private void btnAuthorise_Click(object sender, RoutedEventArgs e)
        {
            TimeTrackUser.strClientID = txtClientID.Text;
            TimeTrackUser.strClientSecret = txtClientSecret.Text;
            if (chkbxURL.IsChecked == true && txtFullURL.Text != "")
            {
                TimeTrackUser.strRedirectURL = txtFullURL.Text;
            }
            else
            {
                TimeTrackUser.strRedirectURL = "https://app.asana.com/-/oauth_authorize?response_type=code&client_id=" + txtClientID.Text + "&redirect_uri=urn%3Aietf%3Awg%3Aoauth%3A2.0%3Aoob&state=<STATE_PARAM>";
            }

            TimeTrackUser.Update();
            TimeTrackUser.UpdateLocal();

            System.Diagnostics.Process.Start(TimeTrackUser.strRedirectURL.ToString());
        }

        private void btnConnect_Click(object sender, RoutedEventArgs e)
        {
            getToken();
        }

        private void btnMinimize_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void chkbxURL_Checked(object sender, RoutedEventArgs e)
        {
            chkbxURL.IsChecked = true;
            txtFullURL.IsEnabled = true;
        }

        private void chkbxURL_Unchecked(object sender, RoutedEventArgs e)
        {
            chkbxURL.IsChecked = false;
            txtFullURL.IsEnabled = false;
        }

        protected async void getToken()
        {
            btnConnect.IsEnabled = false;
            using (var c = new HttpClient())
            {
                var values = new Dictionary<string, string>
                {
                    { "grant_type", "authorization_code" },
                    { "client_id", TimeTrackUser.strClientID },
                    { "client_secret", TimeTrackUser.strClientSecret },
                    { "redirect_uri", "urn:ietf:wg:oauth:2.0:oob" },
                    { "code", txtCode.Text }
                };
                var content = new FormUrlEncodedContent(values);

                try
                {
                    var response = await c.PostAsync("https://app.asana.com/-/oauth_token", content);
                    var responseString = await response.Content.ReadAsStringAsync();

                    var jObj = JObject.Parse(responseString);
                    var token = jObj.SelectToken("access_token");
                    var token2 = jObj.SelectToken("refresh_token");

                    Properties.Settings.Default["strToken"] = token.ToString();
                    Properties.Settings.Default["strRefreshToken"] = token2.ToString();
                    TimeTrackUser.strRefreshToken = token2.ToString();
                    TimeTrackUser.Update();
                    TimeTrackUser.UpdateLocal();
                    //DateTime now = DateTime.Now;
                    //Properties.Settings.Default["dtTokenTime"] = now;
                    Properties.Settings.Default.Save();
                    System.Windows.MessageBox.Show("Connection Successful!");
                    CaptureTask CaptureTask = new CaptureTask();
                    CaptureTask.Show();
                    btnConnect.IsEnabled = true;
                    this.Close();
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show("Could not establish a connection. Ensure all fields are entered correctly and try again.");
                    btnConnect.IsEnabled = true;
                }
            }
        }

        private void txtClientID_GotFocus(object sender, RoutedEventArgs e)
        {
            txtClientID.Text = "";
        }

        private void txtClientSecret_GotFocus(object sender, RoutedEventArgs e)
        {
            txtClientSecret.Text = "";
        }

        private void txtFullURL_GotFocus(object sender, RoutedEventArgs e)
        {
            txtFullURL.Text = "";
        }

        private void txtCode_GotFocus(object sender, RoutedEventArgs e)
        {
            txtCode.Text = "";
        }

        //private void TimerEvent(Object myObject, EventArgs myEventArgs)
        //{
            //popup.Stop();
            //DialogResult result = System.Windows.Forms.MessageBox.Show("Continue running?", "Count is: " + counter, MessageBoxButtons.YesNo);

            // Displays a message box asking whether to continue running the timer.
            //if (result == System.Windows.Forms.DialogResult.Yes)
            //{
                //Restarts the timer and increments the counter.
                //counter += 1;
                //popup.Start();
            //}

        //}
    }
}
