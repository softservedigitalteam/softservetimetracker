﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;
using System.Windows.Controls;

namespace SoftserveTimeTracker
{
    /// <summary>
    /// Interaction logic for ViewTasks.xaml
    /// </summary>
    public partial class ViewTasks : Window
    {
        //string myAsanaID = "";
        clsTimeTrackUsers TimeTrackUser;
        //List<clsAsanaAPI.Workspace> Workspaces;
        //List<clsAsanaAPI.Projects> Projects;
        //List<clsAsanaAPI.Tasks> Tasks;
        //List<clsAsanaAPI.MyTasks> myTasks;
        DataTable projects;


        public ViewTasks()
        {
            InitializeComponent();

            int iTimeTrackUser = Convert.ToInt32(Properties.Settings.Default["iTimeTrackUserID"]);
            TimeTrackUser = new clsTimeTrackUsers(iTimeTrackUser);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //await getAsanaUserID();
            //await getUserWorkspaces();
            //await getProjects();
            getUserTasks();
        }

        //protected async Task getAsanaUserID()
        //{
        //    bool bHaveUserID = false;
        //    int iTimeTrackUserID = Convert.ToInt32(Properties.Settings.Default["iTimeTrackUserID"]);
        //    clsTimeTrackUsers TimeTrackUser = new clsTimeTrackUsers(iTimeTrackUserID);
        //    int xx = 2;
        //    int count = 1;

        //    while (bHaveUserID == false && count <= 3)
        //    {
        //        try
        //        {
        //            string token = Properties.Settings.Default["strToken"].ToString();
        //            string refreshToken = Properties.Settings.Default["strRefreshToken"].ToString();

        //            using (var client = new HttpClient())
        //            {
        //                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

        //                var responseString = await client.GetStringAsync("https://app.asana.com/api/1.0/users/me");
        //                var myInfo = JObject.Parse(responseString);
        //                myAsanaID = myInfo.SelectToken("data.id").ToString();
        //                bHaveUserID = true;
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            string token = Properties.Settings.Default["strToken"].ToString();
        //            string refreshToken = Properties.Settings.Default["strRefreshToken"].ToString();
        //            using (var c = new HttpClient())
        //            {
        //                var values = new Dictionary<string, string>
        //                {
        //                    { "grant_type", "refresh_token" },
        //                    { "client_id", TimeTrackUser.strClientID },
        //                    { "client_secret", TimeTrackUser.strClientSecret },
        //                    { "redirect_uri", "urn:ietf:wg:oauth:2.0:oob" },
        //                    { "refresh_token", refreshToken }
        //                };
        //                var content = new FormUrlEncodedContent(values);

        //                try
        //                {
        //                    var response = await c.PostAsync("https://app.asana.com/-/oauth_token", content);
        //                    var responseString = await response.Content.ReadAsStringAsync();

        //                    var jObj = JObject.Parse(responseString);
        //                    var newToken = jObj.SelectToken("access_token");
        //                    //var newToken2 = jObj.SelectToken("refresh_token");

        //                    Properties.Settings.Default["strToken"] = newToken.ToString();
        //                    //Properties.Settings.Default["strRefreshToken"] = newToken2.ToString();
        //                    //DateTime now = DateTime.Now;
        //                    //Properties.Settings.Default["dtTokenTime"] = now;
        //                    Properties.Settings.Default.Save();

        //                }
        //                catch (Exception ex2)
        //                {
        //                    System.Windows.Forms.MessageBox.Show("Could not establish a connection. Attempt:" + count.ToString());
        //                    count++;
        //                }
        //            }
        //        }
        //    }
        //}

        //protected async Task getUserWorkspaces()
        //{
        //    bool bHaveWorkspaces = false;
        //    int iTimeTrackUserID = Convert.ToInt32(Properties.Settings.Default["iTimeTrackUserID"]);
        //    clsTimeTrackUsers TimeTrackUser = new clsTimeTrackUsers(iTimeTrackUserID);
        //    Workspaces = new List<clsAsanaAPI.Workspace>();
        //    int count = 1;

        //    while (bHaveWorkspaces == false && count <= 3)
        //    {
        //        try
        //        {
        //            string token = Properties.Settings.Default["strToken"].ToString();
        //            string refreshToken = Properties.Settings.Default["strRefreshToken"].ToString();

        //            using (var client = new HttpClient())
        //            {
        //                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

        //                var responseString = await client.GetStringAsync("https://app.asana.com/api/1.0/users/me");
        //                var myInfo = JObject.Parse(responseString);

        //                dynamic workspaceInfo = JsonConvert.DeserializeObject(myInfo.ToString());
        //                JArray jaw = workspaceInfo.data.workspaces;

        //                foreach (var j in jaw)
        //                {
        //                    clsAsanaAPI.Workspace newWorkspace = new clsAsanaAPI.Workspace();
        //                    newWorkspace.id = j["id"].ToString();
        //                    newWorkspace.name = j["name"].ToString();
        //                    Workspaces.Add(newWorkspace);
        //                }
        //                bHaveWorkspaces = true;
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            string token = Properties.Settings.Default["strToken"].ToString();
        //            string refreshToken = Properties.Settings.Default["strRefreshToken"].ToString();

        //            using (var c = new HttpClient())
        //            {
        //                var values = new Dictionary<string, string>
        //                {
        //                    { "grant_type", "refresh_token" },
        //                    { "client_id", TimeTrackUser.strClientID },
        //                    { "client_secret", TimeTrackUser.strClientSecret },
        //                    { "redirect_uri", "urn:ietf:wg:oauth:2.0:oob" },
        //                    { "refresh_token", refreshToken }
        //                };
        //                var content = new FormUrlEncodedContent(values);

        //                try
        //                {
        //                    var response = await c.PostAsync("https://app.asana.com/-/oauth_token", content);
        //                    var responseString = await response.Content.ReadAsStringAsync();

        //                    var jObj = JObject.Parse(responseString);
        //                    var newToken = jObj.SelectToken("access_token");
        //                    //var newToken2 = jObj.SelectToken("refresh_token");

        //                    Properties.Settings.Default["strToken"] = newToken.ToString();
        //                    //Properties.Settings.Default["strRefreshToken"] = newToken2.ToString();
        //                    //DateTime now = DateTime.Now;
        //                    //Properties.Settings.Default["dtTokenTime"] = now;
        //                    Properties.Settings.Default.Save();

        //                }
        //                catch (Exception ex2)
        //                {
        //                    System.Windows.Forms.MessageBox.Show("Workspaces: Could not establish a connection. Attempt:" + count.ToString());
        //                    count++;
        //                }
        //            }
        //        }
        //    }
        //}

        //protected async Task getProjects()
        //{
        //    bool bHaveProjects = false;
        //    int iTimeTrackUserID = Convert.ToInt32(Properties.Settings.Default["iTimeTrackUserID"]);
        //    clsTimeTrackUsers TimeTrackUser = new clsTimeTrackUsers(iTimeTrackUserID);
        //    Projects = new List<clsAsanaAPI.Projects>();
        //    int count = 1;

        //    while (bHaveProjects == false && count <= 3)
        //    {
        //        try
        //        {
        //            string token = Properties.Settings.Default["strToken"].ToString();
        //            string refreshToken = Properties.Settings.Default["strRefreshToken"].ToString();

        //            using (var client = new HttpClient())
        //            {
        //                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

        //                foreach (var myWorkspace in Workspaces)
        //                {
        //                    var responseString = await client.GetStringAsync("https://app.asana.com/api/1.0/workspaces/" + myWorkspace.id + "/projects?opt_fields=name,notes");
        //                    var myInfo = JObject.Parse(responseString);

        //                    dynamic projectInfo = JsonConvert.DeserializeObject(myInfo.ToString());
        //                    JArray projects = projectInfo.data;

        //                    foreach (var project in projects)
        //                    {
        //                        clsAsanaAPI.Projects newProject = new clsAsanaAPI.Projects();
        //                        newProject.id = project["id"].ToString();
        //                        newProject.name = project["name"].ToString();
        //                        newProject.name = project["notes"].ToString();
        //                        Projects.Add(newProject);
        //                    }
        //                }

        //                bHaveProjects = true;
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            string token = Properties.Settings.Default["strToken"].ToString();
        //            string refreshToken = Properties.Settings.Default["strRefreshToken"].ToString();

        //            using (var c = new HttpClient())
        //            {
        //                var values = new Dictionary<string, string>
        //                {
        //                    { "grant_type", "refresh_token" },
        //                    { "client_id", TimeTrackUser.strClientID },
        //                    { "client_secret", TimeTrackUser.strClientSecret },
        //                    { "redirect_uri", "urn:ietf:wg:oauth:2.0:oob" },
        //                    { "refresh_token", refreshToken }
        //                };
        //                var content = new FormUrlEncodedContent(values);

        //                try
        //                {
        //                    var response = await c.PostAsync("https://app.asana.com/-/oauth_token", content);
        //                    var responseString = await response.Content.ReadAsStringAsync();

        //                    var jObj = JObject.Parse(responseString);
        //                    var newToken = jObj.SelectToken("access_token");
        //                    //var newToken2 = jObj.SelectToken("refresh_token");

        //                    Properties.Settings.Default["strToken"] = newToken.ToString();
        //                    //Properties.Settings.Default["strRefreshToken"] = newToken2.ToString();
        //                    //DateTime now = DateTime.Now;
        //                    //Properties.Settings.Default["dtTokenTime"] = now;
        //                    Properties.Settings.Default.Save();

        //                }
        //                catch (Exception ex2)
        //                {
        //                    System.Windows.Forms.MessageBox.Show("Projects: Could not establish a connection. Attempt:" + count.ToString());
        //                    count++;
        //                }
        //            }
        //        }
        //    }
        //}

        protected void getUserTasks()
        {
            DataTable dtAsanaUserTasks = clsAsanaTasks.GetAsanaTasksList("strAssigneeID = '" + TimeTrackUser.strAssigneeID.ToString() + "' AND bIsCompleted = 'false' AND iProjectID <> '0'", "");

            dtAsanaUserTasks.Columns.Add("strProjectTitle");
            foreach (DataRow drAsanaUserTask in dtAsanaUserTasks.Rows)
            {
                clsProjects clsProject = new clsProjects(Convert.ToInt32(drAsanaUserTask["iProjectID"]));
                drAsanaUserTask["strProjectTitle"] = clsProject.strTitle.ToString();
            }

            grdTasks.ItemsSource = dtAsanaUserTasks.DefaultView;
            grdTasks.Items.Refresh();
            int x = 2;
        }

        protected void populateProjectsDatabase()
        {
            projects = clsProjects.GetProjectsList();
        }

        private void btnMinimize_Click(object sender, RoutedEventArgs e)
        {
            CaptureTask ct = new CaptureTask();
            ct.Show();
            this.Close();
        }

        private void btnSelect_Click(object sender, RoutedEventArgs e)
        {
            if ((grdTasks.SelectedItem != null) && !(grdTasks.SelectedItems.Count > 1))
            {
                DataRowView drvTask = (DataRowView)grdTasks.SelectedItem;
                int iAsanaTaskID = Convert.ToInt32(drvTask["iAsanaTaskID"]);

                Properties.Settings.Default["iAsanaTaskID"] = iAsanaTaskID;
                Properties.Settings.Default.Save();

                CaptureTask ct = new CaptureTask();
                ct.Show();
                this.Close();
            }
        }

        private void grdTasks_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            //var item = grdTasks.SelectedItem;
            DataRowView drTaskInfo = (DataRowView)grdTasks.SelectedItem;
            int iAsanaTaskID = Convert.ToInt32(drTaskInfo["iAsanaTaskID"]);
            Properties.Settings.Default["iAsanaTaskID"] = iAsanaTaskID;
            Properties.Settings.Default.Save();
            CaptureTask ct = new CaptureTask();
            ct.Show();
            this.Close();
        }
    }
}
