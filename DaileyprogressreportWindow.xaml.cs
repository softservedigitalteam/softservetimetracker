﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net.Mail;
using System.Data;

namespace SoftserveTimeTracker
{
    /// <summary>
    /// Interaction logic for DaileyprogressreportWindow.xaml
    /// </summary>
    public partial class DaileyprogressreportWindow : Window
    {
        DataTable dtCompletedTaskList;
        public DaileyprogressreportWindow()
        {
            InitializeComponent();
            double height = SystemParameters.WorkArea.Height;
            double width = SystemParameters.WorkArea.Width;
            this.Top = (height - this.Height) / 2;
            this.Left = (width - this.Width) / 2;
            int iTimeTrackUserID = Convert.ToInt32(Properties.Settings.Default["iTimeTrackUserID"]);
            dtCompletedTaskList = clsTasks.GetTasksList("dtEdited >= '" + DateTime.Now.Date + "' and dtEdited < '" + DateTime.Now.Date.AddDays(1) + "' AND iEditedBy = '" + iTimeTrackUserID.ToString() +"' AND bIsTaskcompleted = 'true'", "");
            
            string strCompleteTaskList = "";
            foreach (DataRow drCompletedTask in dtCompletedTaskList.Rows)
            {
                strCompleteTaskList += drCompletedTask["strTaskName"].ToString() + "\r\n";
            }
            if (strCompleteTaskList != "")
            {
                txtWhatIManagedToDo.Text = strCompleteTaskList;
            }
            else
            {
                txtWhatIManagedToDo.Text = "--What-I-managed-Successfully--";
            }
            
        }

        private void btnSendEmail_Click(object sender, RoutedEventArgs e)
        {
            bool isvalid = true;
            if (txtWhatIManagedToDo.Text == "" | txtWhatIManagedToDo.Text == "--What-I-managed-Successfully--" | txtWhatIManagedToDo.Text == "Please List what you managed to do for the day ")
            {
                txtWhatIManagedToDo.Text = "Please List what you managed to do for the day ";
                isvalid = false;
            }
            if (txtProprities.Text == "" | txtProprities.Text == "-- Priorities for Tomorrow --  " | txtProprities.Text == "Please list all your top Priorities for tomorrow ")
            {
                txtProprities.Text = "Please list all your top Priorities for tomorrow ";
                isvalid = false;
            }
            if (txtChallengesIhad.Text == "" | txtChallengesIhad.Text == "-- Challenges I Had -- " | txtChallengesIhad.Text == "Please list all your Challenges you had for today ")
            {
                txtChallengesIhad.Text = "Please list all your Challenges you had for today ";
                isvalid = false;
            }
            if (isvalid == true)
            {
                SendEmail();
                this.Close();
            }

        }
        protected void SendEmail()
        {
            clsTimeTrackUsers clsTimeTrackUsers = new clsTimeTrackUsers(Convert.ToInt32(Properties.Settings.Default["iTimeTrackUserID"]));
            String email = clsTimeTrackUsers.strEmailAddress.ToString();
            Attachment[] empty = new Attachment[] { };
            string strContent = "";
            string managedSuccessfully = txtWhatIManagedToDo.Text;
            string challenges = txtChallengesIhad.Text;
            string priorities = txtProprities.Text;

            managedSuccessfully = managedSuccessfully.Replace("\r\n", "<br/>");
            challenges = challenges.Replace("\r\n", "<br/>");
            priorities = priorities.Replace("\r\n", "<br/>");

            StringBuilder strbMail = new StringBuilder();

            strbMail.AppendLine("<!-- Start of textbanner -->");
            strbMail.AppendLine("<table width='100%' bgcolor='#fff' cellpadding='0' cellspacing='0' border='0' id='backgroundTable' movable=''>");
            strbMail.AppendLine("<tbody>");
            strbMail.AppendLine("<tr>");
            strbMail.AppendLine("<td>");
            strbMail.AppendLine("<table bgcolor='#ffffff' width='650' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' options=''>"); //style='border-left:1px solid #333; border-right:1px solid #333;'
            strbMail.AppendLine("<tbody>");
            strbMail.AppendLine("<!-- Spacing -->");
            strbMail.AppendLine("<tr>");
            strbMail.AppendLine("<td height='50'></td>");
            strbMail.AppendLine("</tr>");
            strbMail.AppendLine("<!-- End of Spacing -->");
            strbMail.AppendLine("<tr>");
            strbMail.AppendLine("<td>");
            strbMail.AppendLine("<table width='100%' cellspacing='0' cellpadding='0'>");
            strbMail.AppendLine("<tbody>");

            strbMail.AppendLine("<!-- Content -->");
            strbMail.AppendLine("<tr>");
            strbMail.AppendLine("<td width='60'></td>");
            strbMail.AppendLine("<td valign='top' style='font-family:Arial;font-size: 15px; color: #3b3b3b; text-align:left;line-height: 22px;' text=''>");
            strbMail.AppendLine("<h3 style='font-family:Calibri;text-align:center;font-weight:normal;'>What I Managed Successfully</h4>");
            strbMail.AppendLine("<p style='font-family:Calibri;text-align:left;font-weight:normal;color:black;'>" + managedSuccessfully + "</p>");
            strbMail.AppendLine("<h3 style='font-family:Calibri;text-align:center;font-weight:normal;'>Challenges I Had</h4>");
            strbMail.AppendLine("<p style='font-family:Calibri;text-align:left;font-weight:normal;color:black;'>" + challenges + "</p>");
            strbMail.AppendLine("<h3 style='font-family:Calibri;text-align:center;font-weight:normal;'>Priorities For Tomorrow</h4>");
            strbMail.AppendLine("<p style='font-family:Calibri;text-align:left;font-weight:normal;color:black;'>" + priorities + "</p>");
            strbMail.AppendLine("</td>");
            strbMail.AppendLine("<td width='60'></td>");
            strbMail.AppendLine("</tr>");
            strbMail.AppendLine("<!-- End of Content -->");

            strbMail.AppendLine("</td>");
            strbMail.AppendLine("<td width='20'></td>");
            strbMail.AppendLine("</tr>");
            strbMail.AppendLine("</tbody>");
            strbMail.AppendLine("</table>");
            strbMail.AppendLine("</td>");
            strbMail.AppendLine("</tr>");
            strbMail.AppendLine("<!-- Spacing -->");
            strbMail.AppendLine("<tr>");
            strbMail.AppendLine("<td height='50'></td>");
            strbMail.AppendLine("</tr>");
            strbMail.AppendLine("<!-- End of Spacing -->");
            strbMail.AppendLine("</tbody>");
            strbMail.AppendLine("</table>");
            strbMail.AppendLine("</td>");
            strbMail.AppendLine("</tr>");
            strbMail.AppendLine("</tbody>");
            strbMail.AppendLine("</table>");
            strbMail.AppendLine("<!-- End of textbanner -->");

            strContent = strbMail.ToString();
            emailComponent.SendMail(email, "andrew@softservedigital.co.za", "adpapoutsis@hotmail.com", "", "Progress Report", strContent, empty, true);
            //emailComponent.SendMail(email, "jamie@softservedigital.co.za", "jamie@softservedigital.co.za", "", "Progress Report", strContent, empty, true);
        }


        private void txtWhatIManagedToDo_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        private void txtWhatIManagedToDo_GotFocus(object sender, RoutedEventArgs e)
        {
            if (txtWhatIManagedToDo.Text == "--What-I-managed-Successfully--")
            {
                txtWhatIManagedToDo.Text = "";
            }
        }
        private void txtChallengesIhad_GotFocus(object sender, RoutedEventArgs e)
        {
            if (txtChallengesIhad.Text == "--Challenges I Had--")
            {
                txtChallengesIhad.Text = "";
            }
        }
        private void txtProprities_GotFocus(object sender, RoutedEventArgs e)
        {
            if (txtProprities.Text == "--Priorities for Tomorrow--")
            {
                txtProprities.Text = "";
            }
        }

        private void txtChallengesIhad_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

 

        private void btnMinimize_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void txtWhatIManagedToDo_LostFocus(object sender, RoutedEventArgs e)
        {
            if (txtWhatIManagedToDo.Text == "")
            {
                txtWhatIManagedToDo.Text = "--What-I-managed-Successfully--";
            }
        }

        private void txtChallengesIhad_LostFocus(object sender, RoutedEventArgs e)
        {
            if (txtChallengesIhad.Text == "")
            {
                txtChallengesIhad.Text = "--Challenges I Had--";
            }
        }

        private void txtProprities_LostFocus(object sender, RoutedEventArgs e)
        {
            if (txtProprities.Text == "")
            {
                txtProprities.Text = "--Priorities for Tomorrow--";
            }
        }
    }
}
