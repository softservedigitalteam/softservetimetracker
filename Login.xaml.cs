﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Security.Cryptography;
using System.IO;


namespace SoftserveTimeTracker
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        clsTimeTrackUsers clsUser;
        public Login()
        {
            int counter = 0;
            string line;
            InitializeComponent();
            Properties.Settings.Default["iAsanaTaskID"] = null;
            try
            {
                using (StreamReader file = new StreamReader("C:TimeTrak.txt"))
                {
                    while ((line = file.ReadLine()) != null)
                    {
                        string test = Decrypt(line);

                        if (txtPassword.Password == "")
                        {
                            txtPassword.Password = test;
                            
                        }
                        else
                        {
                            txtUsername.Text = test;
                        }
                
                        counter++;
                    }

                    file.Close();
                    checRememberMe.IsChecked = true;
                }
            }
            catch
            {

            }
        }

        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                {
                    using (var stream = client.OpenRead("http://www.softservedigital.co.za"))
                    {
                        return true;
                    }
                }
            }
            catch
            {
                return false;
            }
        }
        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool bHasInternet = Convert.ToBoolean(Properties.Settings.Default["bHasInternet"]);

                if (bHasInternet)
                {
                    DataTable dtProjects = clsProjects.GetProjectsList();
                    DataTable dtLocalProjects = clsProjects.GetProjectsListLocal();
                    int x = 2;
                    if (dtLocalProjects.Rows.Count == 0)
                    {
                        foreach (DataRow drProjects in dtProjects.Rows)
                        {
                            clsProjects clsProject = new clsProjects();
                            clsProject.dtAdded = DateTime.Now;
                            clsProject.iAddedBy = 0;
                            clsProject.strAsanaProjectID = "";
                            clsProject.strTitle = drProjects["strTitle"].ToString();
                            clsProject.strDescription = drProjects["strDescription"].ToString();
                            clsProject.UpdateLocal();
                        }
                    }

                    else
                    {
                        foreach (DataRow drLocalProjects in dtLocalProjects.Rows)
                        {
                            clsProjects clsProject = new clsProjects(Convert.ToInt32(drLocalProjects["iProjectID"]), true);
                            clsProjects.DeleteLocal(clsProject.iProjectID);
                        }

                        foreach (DataRow drProjects in dtProjects.Rows)
                        {
                            clsProjects clsProject = new clsProjects();
                            clsProject.dtAdded = DateTime.Now;
                            clsProject.iAddedBy = 0;
                            clsProject.dtEdited = DateTime.Now;
                            clsProject.iEditedBy = 0;
                            clsProject.strAsanaProjectID = drProjects["strAsanaProjectID"].ToString();
                            clsProject.strTitle = drProjects["strTitle"].ToString();
                            clsProject.strDescription = drProjects["strDescription"].ToString();
                            clsProject.UpdateLocal();
                        }
                    }

                    string strPass = clsCommonFunctions.GetMd5Sum(txtPassword.Password);
                    clsTimeTrackUsers clsUser = new clsTimeTrackUsers(txtUsername.Text, clsCommonFunctions.GetMd5Sum(txtPassword.Password));
                    if (checRememberMe.IsChecked == true)
                    {
                        File.Create("C:TimeTrak.txt").Close();                        File.WriteAllText("C:TimeTrak.txt", String.Empty);
                        System.IO.File.Delete("C:TimeTrak.txt");
                        string password = Encrypt(txtPassword.Password);
                        string Username = Encrypt(txtUsername.Text);
                        System.IO.StreamWriter file = new System.IO.StreamWriter("C:TimeTrak.txt", true);
                        file.WriteLine(password);
                        file.WriteLine(Username);
                        file.Close();

                    }
                    if (checRememberMe.IsChecked == false)
                    {
                        File.Create("C:TimeTrak.txt").Close();
                        File.WriteAllText("C:TimeTrak.txt", String.Empty);
                        System.IO.File.Delete("C:TimeTrak.txt");
                    }

                    DataTable dtUsers = clsTimeTrackUsers.GetUsersListLocal();
                    string email = txtUsername.Text;
                    bool userExists = false;
                    foreach (DataRow drUsers in dtUsers.Rows)
                    {
                        if (drUsers["strEmailAddress"].ToString() == email)
                        {
                            userExists = true;
                            break;
                        }
                    }

                    if (userExists == false)
                    {
                        clsTimeTrackUsers clsNewUser = new clsTimeTrackUsers();
                        clsNewUser.dtAdded = DateTime.Now;
                        clsNewUser.iAddedBy = 0;
                        clsNewUser.iConfigurationID = 1;
                        clsNewUser.strFirstName = clsUser.strFirstName;
                        clsNewUser.strPassword = clsUser.strPassword;
                        clsNewUser.strEmailAddress = clsUser.strEmailAddress;
                        clsNewUser.strSurname = clsUser.strSurname;
                        clsNewUser.strClientID = clsUser.strClientID;
                        clsNewUser.strClientSecret = clsUser.strClientSecret;
                        clsNewUser.strRedirectURL = clsUser.strRedirectURL;
                        clsNewUser.strRefreshToken = clsUser.strRefreshToken;
                        clsNewUser.strAssigneeID = clsUser.strAssigneeID;

                        clsNewUser.UpdateLocal();
                    }

                    if (dtUsers.Rows.Count > 0)
                    {
                        clsTimeTrackUsers clsDeleteUser = new clsTimeTrackUsers(txtUsername.Text, clsCommonFunctions.GetMd5Sum(txtPassword.Password), true, true);
                        clsTimeTrackUsers.DeleteLocal(clsDeleteUser.iTimeTrackUserID);
                        clsTimeTrackUsers clsNewUser = new clsTimeTrackUsers();
                        clsNewUser.dtAdded = DateTime.Now;
                        clsNewUser.iAddedBy = 0;
                        clsNewUser.iConfigurationID = 1;
                        clsNewUser.strFirstName = clsUser.strFirstName;
                        clsNewUser.strPassword = clsUser.strPassword;
                        clsNewUser.strEmailAddress = clsUser.strEmailAddress;
                        clsNewUser.strSurname = clsUser.strSurname;
                        clsNewUser.strClientID = clsUser.strClientID;
                        clsNewUser.strClientSecret = clsUser.strClientSecret;
                        clsNewUser.strRedirectURL = clsUser.strRedirectURL;
                        clsNewUser.strRefreshToken = clsUser.strRefreshToken;
                        clsNewUser.strAssigneeID = clsUser.strAssigneeID;
                        clsNewUser.UpdateLocal();
                    }
                    else
                    {
                        clsTimeTrackUsers clsNewUser = new clsTimeTrackUsers();
                        clsNewUser.dtAdded = DateTime.Now;
                        clsNewUser.iAddedBy = 0;
                        clsNewUser.iConfigurationID = 1;
                        clsNewUser.strFirstName = clsUser.strFirstName;
                        clsNewUser.strPassword = clsUser.strPassword;
                        clsNewUser.strEmailAddress = clsUser.strEmailAddress;
                        clsNewUser.strSurname = clsUser.strSurname;
                        clsNewUser.strClientID = clsUser.strClientID;
                        clsNewUser.strClientSecret = clsUser.strClientSecret;
                        clsNewUser.strRedirectURL = clsUser.strRedirectURL;
                        clsNewUser.strRefreshToken = clsUser.strRefreshToken;
                        clsNewUser.strAssigneeID = clsUser.strAssigneeID;
                        clsNewUser.UpdateLocal();
                    }

                    Properties.Settings.Default["iTimeTrackUserID"] = clsUser.iTimeTrackUserID;
                    Properties.Settings.Default["Username"] = txtUsername.Text;
                    Properties.Settings.Default["strPassword"] = txtPassword.Password;

                    if (clsUser.iConfigurationID != 0)
                    {

                        clsConfiguration clsConfig = new clsConfiguration(clsUser.iConfigurationID);

                        if (clsConfig.b10Minutes)
                        {
                            Properties.Settings.Default["iConfigurationTime"] = 10;
                            Properties.Settings.Default["strConfigurationUnit"] = "Minutes";
                        }
                        else if (clsConfig.b30Minutes)
                        {
                            Properties.Settings.Default["iConfigurationTime"] = 30;
                            Properties.Settings.Default["strConfigurationUnit"] = "Minutes";
                        }
                        else if (clsConfig.b1Hour)
                        {
                            Properties.Settings.Default["iConfigurationTime"] = 1;
                            Properties.Settings.Default["strConfigurationUnit"] = "Hours";
                        }
                        else if (clsConfig.b2Hours)
                        {
                            Properties.Settings.Default["iConfigurationTime"] = 2;
                            Properties.Settings.Default["strConfigurationUnit"] = "Hours";
                        }
                        else if (clsConfig.b5Hours)
                        {
                            Properties.Settings.Default["iConfigurationTime"] = 5;
                            Properties.Settings.Default["strConfigurationUnit"] = "Hours";
                        }
                        else
                        {
                            Properties.Settings.Default["iConfigurationTime"] = 10;
                            Properties.Settings.Default["strConfigurationUnit"] = "Minutes";
                        }

                        Properties.Settings.Default.Save();

                        if (clsUser.strRefreshToken != "" && clsUser.strRefreshToken != null)
                        {

                            if (Properties.Settings.Default["strRefreshToken"].ToString() != clsUser.strRefreshToken.ToString())
                            {
                                Properties.Settings.Default["strRefreshToken"] = clsUser.strRefreshToken.ToString();
                                Properties.Settings.Default.Save();
                                CaptureTask CaptureTask = new CaptureTask();
                                CaptureTask.Show();
                                this.Close();
                            }
                            else
                            {
                                CaptureTask CaptureTask = new CaptureTask();
                                CaptureTask.Show();
                                this.Close();
                            }
                        }
                        else
                        {
                            AsanaConnect AsanaConnect = new AsanaConnect();
                            AsanaConnect.Show();
                            this.Close();
                        }
                        //CaptureTask CaptureTask = new CaptureTask();
                        //CaptureTask.Show();
                        //this.Close();

                    }
                    else
                    {
                        Properties.Settings.Default.Save();
                        Configuration Configuration = new Configuration();
                        Configuration.Show();
                        this.Close();
                    }
                }

                else
                {
                    string strPass = clsCommonFunctions.GetMd5Sum(txtPassword.Password);
                    clsTimeTrackUsers clsUser = new clsTimeTrackUsers(txtUsername.Text, true);
                    Properties.Settings.Default["iTimeTrackUserID"] = clsUser.iTimeTrackUserID;
                    Properties.Settings.Default["Username"] = txtUsername.Text;
                    Properties.Settings.Default["strPassword"] = txtPassword.Password;
                    //Properties.Settings.Default["strPassword"] = txtPassword.Password;

                    clsConfiguration clsConfig = new clsConfiguration(clsUser.iConfigurationID, true);
                    if (checRememberMe.IsChecked == true)
                    {
                        System.IO.File.Delete("C:TimeTrak.txt");
                        string password = Encrypt(txtPassword.Password);
                        string Username = Encrypt(txtUsername.Text);
                        System.IO.StreamWriter file = new System.IO.StreamWriter("C:test.txt", true);
                        file.WriteLine(password);
                        file.WriteLine(Username);
                        file.Close();

                    }
                    if (checRememberMe.IsChecked == false)
                    {
                        System.IO.File.Delete("C:test.txt");
                    }
                    if (clsConfig.b10Minutes)
                    {
                        Properties.Settings.Default["iConfigurationTime"] = 10;
                        Properties.Settings.Default["strConfigurationUnit"] = "Minutes";
                    }
                    else if (clsConfig.b30Minutes)
                    {
                        Properties.Settings.Default["iConfigurationTime"] = 30;
                        Properties.Settings.Default["strConfigurationUnit"] = "Minutes";
                    }
                    else if (clsConfig.b1Hour)
                    {
                        Properties.Settings.Default["iConfigurationTime"] = 1;
                        Properties.Settings.Default["strConfigurationUnit"] = "Hours";
                    }
                    else if (clsConfig.b2Hours)
                    {
                        Properties.Settings.Default["iConfigurationTime"] = 2;
                        Properties.Settings.Default["strConfigurationUnit"] = "Hours";
                    }
                    else if (clsConfig.b5Hours)
                    {
                        Properties.Settings.Default["iConfigurationTime"] = 5;
                        Properties.Settings.Default["strConfigurationUnit"] = "Hours";
                    }
                    else
                    {
                        Properties.Settings.Default["iConfigurationTime"] = 10;
                        Properties.Settings.Default["strConfigurationUnit"] = "Minutes";
                    }

                    Properties.Settings.Default.Save();

                    if (clsUser.strRefreshToken != "" && clsUser.strRefreshToken != null)
                    {

                        if (Properties.Settings.Default["strRefreshToken"].ToString() != clsUser.strRefreshToken.ToString())
                        {
                            Properties.Settings.Default["strRefreshToken"] = clsUser.strRefreshToken.ToString();
                            Properties.Settings.Default.Save();
                            CaptureTask CaptureTask = new CaptureTask();
                            CaptureTask.Show();
                            this.Close();
                        }
                        else
                        {
                            CaptureTask CaptureTask = new CaptureTask();
                            CaptureTask.Show();
                            this.Close();
                        }
                    }
                    else
                    {
                        AsanaConnect AsanaConnect = new AsanaConnect();
                        AsanaConnect.Show();
                        this.Close();
                    }

                    //CaptureTask CaptureTask = new CaptureTask();
                    //CaptureTask.Show();
                    //this.Close();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("The Username or Password you have entered is invalid. Please try again.");
                //MessageBox.Show(ex.ToString());
                txtUsername.Text = Properties.Settings.Default["Username"].ToString();
                txtPassword.Password = Properties.Settings.Default["strPassword"].ToString();
                txtUsername.Focus();
                //throw ex;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default["bHasInternet"] = CheckForInternetConnection();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            Properties.Settings.Default["iTimeTrackUserID"] = 0;
            Properties.Settings.Default["Username"] = "";
            Properties.Settings.Default["strPassword"] = "";
            Properties.Settings.Default["strTaskName"] = "";
            Properties.Settings.Default["bHasStarted"] = false;
            Properties.Settings.Default.Save();

            //try
            //{
            //    if (Convert.ToBoolean(Properties.Settings.Default["bHasInternet"]))
            //    {
            //        clsTimeTrackUsers clsUser = new clsTimeTrackUsers(Properties.Settings.Default["Username"].ToString(), clsCommonFunctions.GetMd5Sum(Properties.Settings.Default["strPassword"].ToString()));

            //        AsanaConnect AsanaConnect = new AsanaConnect();
            //        AsanaConnect.Show();
            //        this.Close();

            //        //CaptureTask CaptureTask = new CaptureTask();
            //        //CaptureTask.Show();
            //        //this.Close();
            //    }
            //    else
            //    {
            //        //clsTimeTrackUsers clsUser = new clsTimeTrackUsers(Properties.Settings.Default["Username"].ToString(), clsCommonFunctions.GetMd5Sum(Properties.Settings.Default["strPassword"].ToString()));
            //        //txtPassword.Visibility = Visibility.Hidden;
            //    }
            //}
            //catch
            //{

            //}
        }

        private void txtUsername_GotFocus(object sender, RoutedEventArgs e)
        {
            txtUsername.Text = "";
        }

        private void txtUsername_LostFocus(object sender, RoutedEventArgs e)
        {
            if (txtUsername.Text == "")
            {
                txtUsername.Text = "Username";
            }
        }

        private void txtUsername_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        private string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }
        private string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

    }
}
