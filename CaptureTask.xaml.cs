﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SoftserveTimeTracker
{
    /// <summary>
    /// Interaction logic for CaptureTask.xaml
    /// </summary>
    public partial class CaptureTask : Window
    {

        #region VARIABLES

        ContextMenuStrip contextMenuStrip = new ContextMenuStrip();
        ToolStripMenuItem mnuOpenDashboard = new ToolStripMenuItem();
        ToolStripMenuItem mnuExitApplication = new ToolStripMenuItem();
        ToolStripMenuItem mnuChangeSettings = new ToolStripMenuItem();
        DateTime starttime;
        DateTime stoptime;
        DateTime endtime;
        DateTime currenttime;
        //int previousHourTime = 0;
        System.Windows.Forms.Timer tmrDigitalClock = new System.Windows.Forms.Timer();
        System.Windows.Forms.Timer tmrTokenTimer = new System.Windows.Forms.Timer();

        private System.Windows.Forms.NotifyIcon MyNotifyIcon;

        clsTasks clsTask = new clsTasks();
        clsAsanaTasks clsAsanaTask;
        int iProjectID;
        bool bIsInitial;
        int iLastMinutes = 0;
        int iAsanaTaskID = 0;

        string myAsanaID = "";
        List<clsAsanaAPI.Workspace> Workspaces;
        List<clsAsanaAPI.Projects> Projects;
        List<clsAsanaAPI.Tasks> Tasks;

        #endregion

        #region EVENT METHODS

        public CaptureTask()
        {
            InitializeComponent();
            getID();
            populateGridSummary();
            createTimer();
            createNotifyIcon();
            createMenuDialog();
            setStartPosition();
            populateProjects();
            
            //await getUserWorkspaces();
            //await getProjects();
            //await getUserTasks();

            if (Properties.Settings.Default["iAsanaTaskID"] != null)
            {
                iAsanaTaskID = Convert.ToInt32(Properties.Settings.Default["iAsanaTaskID"]);
                clsAsanaTask = new clsAsanaTasks(iAsanaTaskID);
                txtTaskName.Text = clsAsanaTask.strTitle.ToString();
                cbxProjects.SelectedValue = clsAsanaTask.iProjectID;
            }
        }

        private void btnContinue_Click(object sender, RoutedEventArgs e)
        {
            continueCurrentTask();
        }

        protected async void getID()
        {
            await getAsanaUserID();
        }

        private void MyNotifyIcon_MouseDoubleClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            this.WindowState = WindowState.Normal;
        }

        private void Window_StateChanged(object sender, EventArgs e)
        {
            minimizeOrMaximizeWindow();
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            startNewTask();
        }

        private void btnFinish_Click(object sender, RoutedEventArgs e)
        {
            endCurrentTask();
        }

        //Next we add code to handle the event:
        private void tmrDigitalClock_Tick(object sender, System.EventArgs e)
        {
            // ###Original Code
            //currenttime = DateTime.Now;
            //TimeSpan tmSpent = currenttime - starttime;
            //string strTimeSpent = String.Format("{0}:{1:D2}:{2:D2}", tmSpent.Hours, tmSpent.Minutes, tmSpent.Seconds);
            //lblTimeSpent.Content = strTimeSpent;
            // ###Original Code

            //if (previousHourTime != starttime.Minute)
            //{
            //    if (WindowState == WindowState.Minimized)
            //    {
            //        WindowState = WindowState.Normal;
            //    }
            //}

            //if (currenttime >= endtime)
            //{
            //    stoptime = DateTime.Now;
            //    WindowState = WindowState.Normal;
            //}

            currenttime = DateTime.Now;
            TimeSpan tmSpent = currenttime - starttime;
            string strTimeSpent = String.Format("{0}:{1:D2}:{2:D2}", tmSpent.Hours, tmSpent.Minutes, tmSpent.Seconds);
            lblTimeSpent.Content = strTimeSpent;
            int iMinute = DateTime.Now.Minute;

            if (iMinute != iLastMinutes && iMinute % 2 == 0)
            {
                bool bHasInternet = CheckForInternetConnection();
                if (bHasInternet)
                {
                    Properties.Settings.Default["bHasInternet"] = true;
                    Properties.Settings.Default.Save();
                    DataTable dtTasksLocal = clsTasks.GetTasksListLocal();
                    if (dtTasksLocal.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtTasksLocal.Rows)
                        {
                            clsTasks clsTask = new clsTasks();
                            clsTask.strTaskName = dr["strTaskName"].ToString();
                            clsTask.strDuration = dr["strDuration"].ToString();
                            clsTask.strTaskNotes = dr["strTaskNotes"].ToString();
                            clsTask.iAddedBy = Convert.ToInt32(dr["iAddedBy"]);
                            clsTask.dtAdded = DateTime.Now;
                            clsTask.iProjectID = Convert.ToInt32(dr["iProjectID"]);
                            clsTask.dtTimeSessionElapsed = DateTime.Now;
                            clsTask.bIsTaskcompleted = Convert.ToBoolean(dr["bIsTaskcompleted"]);
                            clsTask.Update();
                            clsTasks clsDeleteTask = new clsTasks(Convert.ToInt32(dr["iTaskID"]), true);
                            clsTasks.DeleteLocal(clsDeleteTask.iTaskID);
                        }
                    }
                    iLastMinutes = iMinute;
                }
                else
                {
                    Properties.Settings.Default["bHasInternet"] = false;
                    Properties.Settings.Default.Save();
                }
            }

            if (currenttime >= endtime)
            {
                stoptime = DateTime.Now;
                WindowState = WindowState.Normal;
            }
        }

        private void mnuOpenDashboard_Click(object sender, System.EventArgs e)
        {
            Dashboard dashBoard = new Dashboard();
            dashBoard.Show();
        }

        private void mnuExitApplication_Click(object sender, System.EventArgs e)
        {
            displayClosingDialog();
        }

        private void mnuChangeSettings_Click(object sender, System.EventArgs e)
        {
            Configuration c = new Configuration();
            c.ShowDialog();
        }

        private void txtTaskName_GotFocus(object sender, RoutedEventArgs e)
        {
            txtTaskName.Text = "";
            clsAsanaTask = null;
            Properties.Settings.Default["iAsanaTaskID"] = null;
        }

        private void txtTaskName_LostFocus(object sender, RoutedEventArgs e)
        {
            if (txtTaskName.Text == "")
            {
                txtTaskName.Text = "What are you busy doing?";
            }
        }

        private void btnStart_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            
            if (txtTaskName.Text != "" && txtTaskName.Text != "What are you busy doing?" && txtTaskName.Text != "Enter Task Here or Select a Task")
            {
                btnStart.IsEnabled = true;
            }
            else
            {
                btnStart.IsEnabled = false;
            }
        }

        private void btnStart_GotFocus(object sender, RoutedEventArgs e)
        {

        }

        private void txtTaskName_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void btnStart_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void btnStart_MouseUp(object sender, MouseButtonEventArgs e)
        {
        }

        private void btnStart_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnStart.IsEnabled = true;
        }

        private void btnMinimize_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            displayClosingDialog();
        }

        private void btnCollapse_Click(object sender, RoutedEventArgs e)
        {
            this.Height = 280;
            this.Top = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Bottom - this.Height - 10;
            btnExpand.Visibility = Visibility.Visible;
            btnCollapse.Visibility = Visibility.Hidden;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Height = 574;
            this.Top = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Bottom - this.Height - 10;
            btnExpand.Visibility = Visibility.Hidden;
            btnCollapse.Visibility = Visibility.Visible; 
        }

        #endregion

        #region PAGE METHODS

        private void populateGridSummary()
        {
            bool bHasInternet = CheckForInternetConnection();

            if (bHasInternet)
            {
                dgrRecentTasks.SetBinding(ItemsControl.ItemsSourceProperty, new System.Windows.Data.Binding { Source = "" });
                DataTable dtRecentTasks = clsTasks.GetTasksList("iAddedBy = " + Properties.Settings.Default["iTimeTrackUserID"].ToString(), "iTaskID DESC");
                DataTable dtSummary = new DataTable();
                dtSummary.Columns.Add("Task", typeof(string));
                dtSummary.Columns.Add("It Took", typeof(string));
                dtSummary.Columns.Add("When", typeof(string));
                int iCount = 1;
                foreach (DataRow dr in dtRecentTasks.Rows)
                {
                    if (iCount <= 6)
                    {
                        DataRow drRecent = dtSummary.NewRow();
                        drRecent["Task"] = dr["strTaskName"].ToString();
                        //TimeSpan tsTimeItTook = ((DateTime)dr["dtTimeSessionElapsed"]) - ((DateTime)dr["dtAdded"]);
                        drRecent["It Took"] = dr["strDuration"].ToString();/*tsTimeItTook.Hours + "h " + tsTimeItTook.Minutes + "m " + tsTimeItTook.Seconds + "s";*/
                        TimeSpan tsHoursAgo = DateTime.Now - (DateTime)dr["dtTimeSessionElapsed"];
                        int iHours = tsHoursAgo.Hours;
                        drRecent["When"] = iHours + " hr(s) ago";
                        dtSummary.Rows.Add(drRecent);
                        iCount++;
                    }
                    else
                    {
                        break;
                    }
                }
                dgrRecentTasks.SetBinding(ItemsControl.ItemsSourceProperty, new System.Windows.Data.Binding { Source = dtSummary });
            }
            else
            {
                dgrRecentTasks.SetBinding(ItemsControl.ItemsSourceProperty, new System.Windows.Data.Binding { Source = "" });
                DataTable dtRecentTasks = clsTasks.GetTasksListLocal("iAddedBy = " + Properties.Settings.Default["iTimeTrackUserID"].ToString(), "iTaskID DESC");
                DataTable dtSummary = new DataTable();
                dtSummary.Columns.Add("Task", typeof(string));
                dtSummary.Columns.Add("It Took", typeof(string));
                dtSummary.Columns.Add("When", typeof(string));
                int iCount = 1;
                foreach (DataRow dr in dtRecentTasks.Rows)
                {
                    if (iCount <= 6)
                    {
                        DataRow drRecent = dtSummary.NewRow();
                        drRecent["Task"] = dr["strTaskName"].ToString();
                        //TimeSpan tsTimeItTook = ((DateTime)dr["dtTimeSessionElapsed"]) - ((DateTime)dr["dtAdded"]);
                        drRecent["It Took"] = dr["strDuration"].ToString();/*tsTimeItTook.Hours + "h " + tsTimeItTook.Minutes + "m " + tsTimeItTook.Seconds + "s";*/
                        TimeSpan tsHoursAgo = DateTime.Now - (DateTime)dr["dtTimeSessionElapsed"];
                        int iHours = tsHoursAgo.Hours;
                        drRecent["When"] = iHours + " hr(s) ago";
                        dtSummary.Rows.Add(drRecent);
                        iCount++;
                    }
                    else
                    {
                        break;
                    }
                }
                dgrRecentTasks.SetBinding(ItemsControl.ItemsSourceProperty, new System.Windows.Data.Binding { Source = dtSummary });
            }

        }

        protected void populateProjects()
        {

            //DataTable dtProjectsList = new DataTable();
            //ComboBoxItem item = new ComboBoxItem();
            //dtProjectsList = clsProjects.GetProjectsList();
            //cbxProjects.ItemsSource = dtProjectsList.AsDataView();

            bool bHasInternet = Convert.ToBoolean(Properties.Settings.Default["bHasInternet"]);

            if (bHasInternet)
            {
                DataTable dtProjects = clsProjects.GetProjectsList();
                //New Table For Empty Row
                DataTable dtNewTable = new DataTable();
                dtNewTable = dtProjects.Copy();
                dtNewTable.Clear();

                //Creating Empty Row
                DataRow dtEmptyRow = dtNewTable.NewRow();
                dtEmptyRow["iProjectID"] = 0;
                dtEmptyRow["iAddedBy"] = 0;
                dtEmptyRow["dtAdded"] = DateTime.Now;
                dtEmptyRow["iEditedBy"] = 0;
                dtEmptyRow["dtEdited"] = DateTime.Now;
                dtEmptyRow["strTitle"] = "  -- Select Project --";
                dtEmptyRow["strAsanaProjectID"] = "  -- Select Project --";
                dtEmptyRow["strDescription"] = "  -- Select Project --";
                dtEmptyRow["bIsDeleted"] = false;
                //Adding Empty Row To New Table
                dtNewTable.Rows.Add(dtEmptyRow);

                dtNewTable.Merge(dtProjects);
                cbxProjects.ItemsSource = dtNewTable.AsDataView();
                cbxProjects.SelectedIndex = 0;

                //cbxProjects.Items.Add("-- Not Selected --");
                //foreach (DataRow project in dtProjects.Rows)
                //{
                //    cbxProjects.Items.Add(project["strTitle"].ToString());
                //    cbxProjects.SelectedIndex = 0;
                //}
            }
            else
            {
                DataTable dtProjects = clsProjects.GetProjectsListLocal();
                //New Table For Empty Row
                DataTable dtNewTable = new DataTable();
                dtNewTable = dtProjects.Copy();
                dtNewTable.Clear();

                //Creating Empty Row
                DataRow dtEmptyRow = dtNewTable.NewRow();
                dtEmptyRow["iProjectID"] = 0;
                dtEmptyRow["iAddedBy"] = 0;
                dtEmptyRow["dtAdded"] = DateTime.Now;
                dtEmptyRow["iEditedBy"] = 0;
                dtEmptyRow["dtEdited"] = DateTime.Now;
                dtEmptyRow["strTitle"] = "-- Select Project --";
                dtEmptyRow["strAsanaProjectID"] = "-- Select Project --";
                dtEmptyRow["strDescription"] = "-- Select Project --";
                dtEmptyRow["bIsDeleted"] = false;
                //Adding Empty Row To New Table
                dtNewTable.Rows.Add(dtEmptyRow);

                dtNewTable.Merge(dtProjects);
                cbxProjects.ItemsSource = dtNewTable.AsDataView();
                cbxProjects.SelectedIndex = 0;

                //cbxProjects.Items.Add("-- Not Selected --");
                //foreach (DataRow dr in dtProjects.Rows)
                //{
                //    cbxProjects.Items.Add(dr["strTitle"].ToString());
                //    cbxProjects.SelectedIndex = 0;
                //}
            }

        }

        private void setStartPosition()
        {
            this.Height = 280;
            this.Left = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Right - this.Width - 10;
            this.Top = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Bottom - this.Height - 30;
        }

        private void createTimer()
        {
            tmrDigitalClock.Enabled = true;
            tmrDigitalClock.Interval = 1;
            tmrDigitalClock.Stop();
            tmrDigitalClock.Tick += new System.EventHandler(tmrDigitalClock_Tick);
        }

        private void createNotifyIcon()
        {
            //Uri iconUri = new Uri("pack://application:,,,/images/Clock.ico", UriKind.RelativeOrAbsolute);
            //this.Icon = BitmapFrame.Create(iconUri);
            MyNotifyIcon = new System.Windows.Forms.NotifyIcon();
            //MyNotifyIcon.Icon = new System.Drawing.Icon("C:/Program Files (x86)/Softserve Digital Development/Softserve Time Attack/Clock.ico");
            MyNotifyIcon.Icon = new System.Drawing.Icon(System.Windows.Application.GetResourceStream(new Uri("images/Clock.ico", UriKind.Relative)).Stream);
            MyNotifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(MyNotifyIcon_MouseDoubleClick);
            MyNotifyIcon.ContextMenuStrip = contextMenuStrip;
        }

        private void createMenuDialog()
        {
            mnuOpenDashboard.Text = "Open Dashboard";
            mnuOpenDashboard.Click += new EventHandler(mnuOpenDashboard_Click);
            contextMenuStrip.Items.Add(mnuOpenDashboard);

            mnuChangeSettings.Text = "Change Your Settings";
            mnuChangeSettings.Click += new EventHandler(mnuChangeSettings_Click);
            contextMenuStrip.Items.Add(mnuChangeSettings);

            mnuExitApplication.Text = "Exit Time Tracker";
            mnuExitApplication.Click += new EventHandler(mnuExitApplication_Click);
            contextMenuStrip.Items.Add(mnuExitApplication);
        }

        private void startNewTask()
        {
            bool bHasInternet = CheckForInternetConnection();
            bool bHasUpdatedAsana = false;
            chkbxTaskCompleted.IsEnabled = true;
            if (bHasInternet)
            {

                //cbxProjects.IsEnabled = false;
                lblTimeSpent.Content = "0:00:00";
                starttime = DateTime.Now;
                int iTime = Convert.ToInt32(Properties.Settings.Default["iConfigurationTime"]);
                string strUnit = Properties.Settings.Default["strConfigurationUnit"].ToString();
                if (strUnit == "Minutes")
                {
                    endtime = starttime.AddMinutes(iTime);
                }
                else if (strUnit == "Hours")
                {
                    endtime = starttime.AddHours(iTime);
                }
                ////endtime = starttime.AddHours(1);
                tmrDigitalClock.Start();
                clsTask = new clsTasks();
                clsTask.strTaskName = txtTaskName.Text;
                clsTask.bIsTaskcompleted = false;
                clsTask.dtAdded = DateTime.Now;
                clsTask.iAddedBy = Convert.ToInt32(Properties.Settings.Default["iTimeTrackUserID"]);
                clsTask.strTaskNotes = "";
                clsTask.dtTimeSessionElapsed = DateTime.Now;
                clsTask.iProjectID = Convert.ToInt32(cbxProjects.SelectedValue);
                TimeSpan tsTimeItTook = clsTask.dtTimeSessionElapsed - clsTask.dtAdded;
                clsTask.strDuration = tsTimeItTook.Hours + "h " + tsTimeItTook.Minutes + "m " + tsTimeItTook.Seconds + "s";
                if (clsAsanaTask != null)
                {
                    clsTask.iAsanaTaskID = clsAsanaTask.iAsanaTaskID;
                }
                clsTask.Update();

                //while (bHasUpdatedAsana == false)
                //{
                //    try
                //    {
                //        string token = Properties.Settings.Default["strToken"].ToString();
                //        string refreshToken = Properties.Settings.Default["strRefreshToken"].ToString();


                //    }
                //    catch (Exception ex)
                //    {

                //    }
                //}

                Properties.Settings.Default["strTaskName"] = clsTask.strTaskName;
                Properties.Settings.Default["bHasStarted"] = true;
                Properties.Settings.Default["iTaskID"] = clsTask.iTaskID;



                Properties.Settings.Default.Save();
                txtTaskName.IsEnabled = false;
                cbxProjects.IsEnabled = false;
                btnTasks.IsEnabled = false;
                btnStart.Visibility = Visibility.Hidden;
                btnStart.IsEnabled = false;
                btnFinish.Visibility = Visibility.Visible;
                btnFinish.IsEnabled = true;
                btnContinue.Visibility = Visibility.Visible;
                WindowState = WindowState.Minimized;
            }
            else
            {
                //cbxProjects.IsEnabled = false;
                lblTimeSpent.Content = "0:00:00";
                starttime = DateTime.Now;
                int iTime = Convert.ToInt32(Properties.Settings.Default["iConfigurationTime"]);
                string strUnit = Properties.Settings.Default["strConfigurationUnit"].ToString();
                if (strUnit == "Minutes")
                {
                    endtime = starttime.AddMinutes(iTime);
                }
                else if (strUnit == "Hours")
                {
                    endtime = starttime.AddHours(iTime);
                }
                ////endtime = starttime.AddHours(1);
                tmrDigitalClock.Start();
                clsTask = new clsTasks();
                clsTask.strTaskName = txtTaskName.Text;
                clsTask.bIsTaskcompleted = false;
                clsTask.dtAdded = DateTime.Now;
                clsTask.iAddedBy = Convert.ToInt32(Properties.Settings.Default["iTimeTrackUserID"]);
                clsTask.strTaskNotes = "";
                clsTask.dtTimeSessionElapsed = DateTime.Now;
                clsTask.iProjectID = Convert.ToInt32(cbxProjects.SelectedValue);
                TimeSpan tsTimeItTook = clsTask.dtTimeSessionElapsed - clsTask.dtAdded;
                clsTask.strDuration = tsTimeItTook.Hours + "h " + tsTimeItTook.Minutes + "m " + tsTimeItTook.Seconds + "s";
                clsTask.UpdateLocal();

                Properties.Settings.Default["strTaskName"] = clsTask.strTaskName;
                Properties.Settings.Default["bHasStarted"] = true;
                Properties.Settings.Default["iTaskID"] = clsTask.iTaskID;
                Properties.Settings.Default.Save();
                txtTaskName.IsEnabled = false;
                cbxProjects.IsEnabled = false;
                btnTasks.IsEnabled = false;
                btnStart.Visibility = Visibility.Hidden;
                btnStart.IsEnabled = false;
                btnFinish.Visibility = Visibility.Visible;
                btnFinish.IsEnabled = true;
                btnContinue.Visibility = Visibility.Visible;
                WindowState = WindowState.Minimized;
            }
        }

        private void continueCurrentTask()
        {
            bool bHasInternet = CheckForInternetConnection();

            if (bHasInternet)
            {
                clsTasks clsTask = new clsTasks(Convert.ToInt32(Properties.Settings.Default["iTaskID"]));
                clsTask.bIsTaskcompleted = false;
                clsTask.dtEdited = DateTime.Now;
                clsTask.iEditedBy = Convert.ToInt32(Properties.Settings.Default["iTimeTrackUserID"]);
                clsTask.dtTimeSessionElapsed = DateTime.Now;
                TimeSpan tsDuration = endtime - clsTask.dtTimeSessionElapsed;
                if (tsDuration.Hours <= 0 && tsDuration.Minutes <= 0 && tsDuration.Seconds <= 0)
                {
                    int iTime = Convert.ToInt32(Properties.Settings.Default["iConfigurationTime"]);
                    string strUnit = Properties.Settings.Default["strConfigurationUnit"].ToString();
                    if (strUnit == "Minutes")
                    {
                        endtime = stoptime.AddMinutes(iTime);
                    }
                    else if (strUnit == "Hours")
                    {
                        endtime = stoptime.AddHours(iTime);
                    }
                }
                clsTask.Update();
                WindowState = WindowState.Minimized;
                //endtime = starttime.AddHours(1);
                tmrDigitalClock.Start();
            }
            else
            {
                clsTasks clsTask = new clsTasks(Convert.ToInt32(Properties.Settings.Default["iTaskID"]), true);
                clsTask.bIsTaskcompleted = false;
                clsTask.dtEdited = DateTime.Now;
                clsTask.iEditedBy = Convert.ToInt32(Properties.Settings.Default["iTimeTrackUserID"]);
                clsTask.dtTimeSessionElapsed = DateTime.Now;
                TimeSpan tsDuration = endtime - clsTask.dtTimeSessionElapsed;
                if (tsDuration.Hours <= 0 && tsDuration.Minutes <= 0 && tsDuration.Seconds <= 0)
                {
                    int iTime = Convert.ToInt32(Properties.Settings.Default["iConfigurationTime"]);
                    string strUnit = Properties.Settings.Default["strConfigurationUnit"].ToString();
                    if (strUnit == "Minutes")
                    {
                        endtime = stoptime.AddMinutes(iTime);
                    }
                    else if (strUnit == "Hours")
                    {
                        endtime = stoptime.AddHours(iTime);
                    }
                }
                clsTask.UpdateLocal();
                WindowState = WindowState.Minimized;
                //endtime = starttime.AddHours(1);
                tmrDigitalClock.Start();
            }
        }

        private async void endCurrentTask()
        {
            bool bHasInternet = CheckForInternetConnection();
            
            if (bHasInternet)
            {
                //cbxProjects.IsEnabled = true;
                //cbxProjects.SelectedIndex = 0;
                stoptime = DateTime.Now;
                tmrDigitalClock.Stop();
                lblTimeSpent.Content = "0:00:00";
                clsTasks clsTask = new clsTasks(Convert.ToInt32(Properties.Settings.Default["iTaskID"]));
                if (chkbxTaskCompleted.IsChecked == true)
                {
                    clsTask.bIsTaskcompleted = true;
                }
                else
                {
                    clsTask.bIsTaskcompleted = false;
                }
                clsTask.dtEdited = DateTime.Now;
                clsTask.iEditedBy = Convert.ToInt32(Properties.Settings.Default["iTimeTrackUserID"]);
                clsTask.dtTimeSessionElapsed = DateTime.Now;
                TimeSpan tsTimeItTook = clsTask.dtTimeSessionElapsed - clsTask.dtAdded;
                clsTask.strDuration = tsTimeItTook.Hours + "h " + tsTimeItTook.Minutes + "m " + tsTimeItTook.Seconds + "s";
                clsTask.Update();
                Properties.Settings.Default["strTaskName"] = "What are you busy doing?";
                Properties.Settings.Default["iTaskID"] = 0;
                Properties.Settings.Default.Save();
                btnStart.Visibility = Visibility.Visible;
                btnStart.IsEnabled = true;
                btnFinish.Visibility = Visibility.Hidden;
                btnFinish.IsEnabled = false;
                btnContinue.Visibility = Visibility.Hidden;
                await updateTaskDetails();
                txtTaskName.IsEnabled = true;
                cbxProjects.IsEnabled = true;
                btnTasks.IsEnabled = true;
                txtTaskName.Text = "";
                txtTaskName.Text = "What are you busy doing?";
                Properties.Settings.Default["bHasInternet"] = true;
                Properties.Settings.Default.Save();
            }
            else
            {
                //cbxProjects.IsEnabled = true;
                //cbxProjects.SelectedIndex = 0;
                stoptime = DateTime.Now;
                tmrDigitalClock.Stop();
                lblTimeSpent.Content = "0:00:00";
                clsTasks clsTask = new clsTasks(Convert.ToInt32(Properties.Settings.Default["iTaskID"]), true);
                if (chkbxTaskCompleted.IsChecked == true)
                {
                    clsTask.bIsTaskcompleted = true;
                }
                else
                {
                    clsTask.bIsTaskcompleted = false;
                }
                clsTask.dtEdited = DateTime.Now;
                clsTask.iEditedBy = Convert.ToInt32(Properties.Settings.Default["iTimeTrackUserID"]);
                clsTask.dtTimeSessionElapsed = DateTime.Now;
                TimeSpan tsTimeItTook = clsTask.dtTimeSessionElapsed - clsTask.dtAdded;
                clsTask.strDuration = tsTimeItTook.Hours + "h " + tsTimeItTook.Minutes + "m " + tsTimeItTook.Seconds + "s";
                clsTask.UpdateLocal();
                Properties.Settings.Default["strTaskName"] = "What are you busy doing?";
                Properties.Settings.Default["iTaskID"] = 0;
                Properties.Settings.Default.Save();
                btnStart.Visibility = Visibility.Visible;
                btnStart.IsEnabled = true;
                btnFinish.Visibility = Visibility.Hidden;
                btnFinish.IsEnabled = false;
                btnContinue.Visibility = Visibility.Hidden;
                txtTaskName.IsEnabled = true;
                cbxProjects.IsEnabled = true;
                btnTasks.IsEnabled = true;
                txtTaskName.Text = "";
                txtTaskName.Text = "What are you busy doing?";
            }
            chkbxTaskCompleted.IsChecked = false;
            chkbxTaskCompleted.IsEnabled = false;
        }

        protected async Task updateTaskDetails()
        {
            if (clsAsanaTask != null)
            {
                if (chkbxTaskCompleted.IsChecked == true)
                {
                    clsAsanaTask.iEditedBy = Convert.ToInt32(Properties.Settings.Default["iTimeTrackUserID"]);
                    clsAsanaTask.dtEdited = DateTime.Now;
                    clsAsanaTask.bIsCompleted = true;
                    clsAsanaTask.dtCompleted = DateTime.Now;
                    clsAsanaTask.Update();

                    bool bTaskUpdated = false;
                    int iTimeTrackUserID = Convert.ToInt32(Properties.Settings.Default["iTimeTrackUserID"]);
                    clsTimeTrackUsers TimeTrackUser = new clsTimeTrackUsers(iTimeTrackUserID);
                    int count = 1;

                    while (bTaskUpdated == false && count <= 3)
                    {
                        try
                        {
                            string token = Properties.Settings.Default["strToken"].ToString();
                            string refreshToken = Properties.Settings.Default["strRefreshToken"].ToString();

                            using (var client = new HttpClient())
                            {
                                var values = new Dictionary<string, string>
                                {
                                    { "completed", "true" }
                                };
                                var content = new FormUrlEncodedContent(values);

                                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
                                string putURI = "https://app.asana.com/api/1.0/tasks/" + clsAsanaTask.strAsanaTaskLiveID.ToString();
                                var response = await client.PutAsync(putURI, content);
                                //var response = await client.PutAsJsonAsync(putURI, data);
                                var responseString = await response.Content.ReadAsStringAsync();
                                
                                bTaskUpdated = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            string token = Properties.Settings.Default["strToken"].ToString();
                            string refreshToken = Properties.Settings.Default["strRefreshToken"].ToString();

                            using (var c = new HttpClient())
                            {
                                var values = new Dictionary<string, string>
                            {
                                { "grant_type", "refresh_token" },
                                { "client_id", TimeTrackUser.strClientID },
                                { "client_secret", TimeTrackUser.strClientSecret },
                                { "redirect_uri", "urn:ietf:wg:oauth:2.0:oob" },
                                { "refresh_token", refreshToken }
                            };
                                var content = new FormUrlEncodedContent(values);

                                try
                                {
                                    var response = await c.PostAsync("https://app.asana.com/-/oauth_token", content);
                                    var responseString = await response.Content.ReadAsStringAsync();

                                    var jObj = JObject.Parse(responseString);
                                    var newToken = jObj.SelectToken("access_token");
                                    //var newToken2 = jObj.SelectToken("refresh_token");

                                    Properties.Settings.Default["strToken"] = newToken.ToString();
                                    //Properties.Settings.Default["strRefreshToken"] = newToken2.ToString();
                                    //DateTime now = DateTime.Now;
                                    //Properties.Settings.Default["dtTokenTime"] = now;
                                    Properties.Settings.Default.Save();

                                }
                                catch (Exception ex2)
                                {
                                    System.Windows.Forms.MessageBox.Show("Tasks: Could not establish a connection. Attempt:" + count.ToString());
                                    count++;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void minimizeOrMaximizeWindow()
        {
            if (this.WindowState == WindowState.Minimized)
            {
                //this.ShowInTaskbar = false;
                //MyNotifyIcon.BalloonTipTitle = "Time Tracker";
                //MyNotifyIcon.BalloonTipText = "Time Tracker has been Minimized.";
                //MyNotifyIcon.ShowBalloonTip(1000);
                MyNotifyIcon.Visible = true;
            }
            //else if (this.WindowState == WindowState.Normal)
            //{
            //    populateGridSummary();
            //    txtTaskName.Text = Properties.Settings.Default["strTaskName"].ToString();
            //    MyNotifyIcon.Visible = false;
            //    this.ShowInTaskbar = true;
            //}
        }

        private void displayClosingDialog()
        {
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Are you sure you want to close Time Tracker?", "Exit Confirmation", System.Windows.MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                WindowState = WindowState.Minimized;
            }
            //else if (messageBoxResult == MessageBoxResult.No)
            //{

            //}
        }

        #endregion
        protected void btnConfig_Click(object sender, RoutedEventArgs e)
        {
            Configuration c = new Configuration();
            c.ShowDialog();
        }
        private void cbxProjects_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            bool bHasInternet = Convert.ToBoolean(Properties.Settings.Default["bHasInternet"]);

            if (cbxProjects.SelectedIndex == 0)
            {
                txtTaskName.IsEnabled = false;
            }
            else
            {
                txtTaskName.IsEnabled = true;
            }

            if (bHasInternet)
            {
                //if (bIsInitial == false)
                //{
                //    string strProjectName = cbxProjects.SelectedItem.ToString();

                //    DataTable dtProjects = clsProjects.GetProjectsList("strTitle = '" + strProjectName + "'", "");
                //    foreach (DataRow dr in dtProjects.Rows)
                //    {
                //        iProjectID = Convert.ToInt32(dr["iProjectID"]);
                //        break;
                //    }
                //}
                //bIsInitial = false;
                iProjectID = Convert.ToInt32(cbxProjects.SelectedValue);

            }
            else
            {
                //if (bIsInitial == false)
                //{
                //    string strProjectName = cbxProjects.SelectedItem.ToString();

                //    DataTable dtProjects = clsProjects.GetProjectsListLocal("strTitle = '" + strProjectName + "'", "");
                //    foreach (DataRow dr in dtProjects.Rows)
                //    {
                //        iProjectID = Convert.ToInt32(dr["iProjectID"]);
                //        break;
                //    }
                //}
                //bIsInitial = false;
                iProjectID = Convert.ToInt32(cbxProjects.SelectedValue);

            }
        }

        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                {
                    using (var stream = client.OpenRead("http://www.softservedigital.co.za"))
                    {
                        return true;
                    }
                }
            }
            catch
            {
                return false;
            }
        }

        private void bProgressReport_Click(object sender, RoutedEventArgs e)
        {
            DaileyprogressreportWindow d = new DaileyprogressreportWindow();
            d.Show();
        }

        private void btnAsanaConnect_Click(object sender, RoutedEventArgs e)
        {
            AsanaConnect AsanaConnect = new AsanaConnect();
            AsanaConnect.ShowDialog();
            
        }

        protected async Task postAsanaTask()
        {
            //Empty String
            string code = "";
            using (var client = new HttpClient())
            {
                string token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdXRob3JpemF0aW9uIjoxNjYwMzYzNDMyMDU2NzQsInNjb3BlIjoiIiwiaWF0IjoxNDcxMDA1NDIyLCJleHAiOjE0NzEwMDkwMjJ9.jEGQT9w7lhUAiELHU9t8zC27WJaskTkhQyV3rD-IHRU";
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
                //166612865950664

                var responseString = await client.GetStringAsync("https://app.asana.com/api/1.0/users/me");
                var myInfo = JObject.Parse(responseString);
                string myID = myInfo.SelectToken("data.id").ToString();

                dynamic workspaceInfo = JsonConvert.DeserializeObject(myInfo.ToString());
                JArray jaw = workspaceInfo.data.workspaces;

                List<string> Workspaces = new List<string>();
                foreach (var j in jaw)
                {
                    Workspaces.Add(j["id"].ToString());
                }
                string workspaceID = Workspaces[0];


                var values = new Dictionary<string, string>
                {
                    { "assignee", myID },
                    { "name", "HELLOWORLD2" },
                    { "notes", "My Note" },
                    { "workspace", workspaceID }
                };

                var content = new FormUrlEncodedContent(values);
                var response = await client.PostAsync("https://app.asana.com/api/1.0/tasks", content);
                var responseString2 = await response.Content.ReadAsStringAsync();
                int r = 2;
            }
        }

        protected async Task getAsanaUserID()
        {
            bool bHaveUserID = false;
            int iTimeTrackUserID = Convert.ToInt32(Properties.Settings.Default["iTimeTrackUserID"]);
            clsTimeTrackUsers TimeTrackUser = new clsTimeTrackUsers(iTimeTrackUserID);
            int xx = 2;
            int count = 1;

            while (bHaveUserID == false && count <= 3) 
            {
                try
                {
                    string token = Properties.Settings.Default["strToken"].ToString();
                    string refreshToken = Properties.Settings.Default["strRefreshToken"].ToString();

                    using (var client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

                        var responseString = await client.GetStringAsync("https://app.asana.com/api/1.0/users/me");
                        var myInfo = JObject.Parse(responseString);
                        myAsanaID = myInfo.SelectToken("data.id").ToString();
                        if (TimeTrackUser.strAssigneeID != myAsanaID)
                        {
                            TimeTrackUser.strAssigneeID = myAsanaID;
                            TimeTrackUser.Update();
                        }
                        bHaveUserID = true;
                    }
                }
                catch (Exception ex)
                {
                    string token = Properties.Settings.Default["strToken"].ToString();
                    string refreshToken = Properties.Settings.Default["strRefreshToken"].ToString();
                    using (var c = new HttpClient())
                    {
                        var values = new Dictionary<string, string>
                        {
                            { "grant_type", "refresh_token" },
                            { "client_id", TimeTrackUser.strClientID },
                            { "client_secret", TimeTrackUser.strClientSecret },
                            { "redirect_uri", "urn:ietf:wg:oauth:2.0:oob" },
                            { "refresh_token", refreshToken }
                        };
                        var content = new FormUrlEncodedContent(values);

                        try
                        {
                            var response = await c.PostAsync("https://app.asana.com/-/oauth_token", content);
                            var responseString = await response.Content.ReadAsStringAsync();

                            var jObj = JObject.Parse(responseString);
                            var newToken = jObj.SelectToken("access_token");
                            //var newToken2 = jObj.SelectToken("refresh_token");

                            Properties.Settings.Default["strToken"] = newToken.ToString();
                            //Properties.Settings.Default["strRefreshToken"] = newToken2.ToString();
                            //DateTime now = DateTime.Now;
                            //Properties.Settings.Default["dtTokenTime"] = now;
                            Properties.Settings.Default.Save();

                        }
                        catch (Exception ex2)
                        {
                            System.Windows.Forms.MessageBox.Show("Could not establish a connection. Attempt:" + count.ToString());
                            count++;
                        }
                    }
                }
            }
        }

        protected async Task getUserWorkspaces()
        {
            bool bHaveWorkspaces = false;
            int iTimeTrackUserID = Convert.ToInt32(Properties.Settings.Default["iTimeTrackUserID"]);
            clsTimeTrackUsers TimeTrackUser = new clsTimeTrackUsers(iTimeTrackUserID);
            Workspaces = new List<clsAsanaAPI.Workspace>();
            int count = 1;

            while (bHaveWorkspaces == false && count <= 3)
            {
                try
                {
                    string token = Properties.Settings.Default["strToken"].ToString();
                    string refreshToken = Properties.Settings.Default["strRefreshToken"].ToString();

                    using (var client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

                        var responseString = await client.GetStringAsync("https://app.asana.com/api/1.0/users/me");
                        var myInfo = JObject.Parse(responseString);

                        dynamic workspaceInfo = JsonConvert.DeserializeObject(myInfo.ToString());
                        JArray jaw = workspaceInfo.data.workspaces;

                        foreach (var j in jaw)
                        {
                            clsAsanaAPI.Workspace newWorkspace = new clsAsanaAPI.Workspace();
                            newWorkspace.id = j["id"].ToString();
                            newWorkspace.name = j["name"].ToString();
                            Workspaces.Add(newWorkspace);
                        }
                        bHaveWorkspaces = true;
                    }
                }
                catch (Exception ex)
                {
                    string token = Properties.Settings.Default["strToken"].ToString();
                    string refreshToken = Properties.Settings.Default["strRefreshToken"].ToString();

                    using (var c = new HttpClient())
                    {
                        var values = new Dictionary<string, string>
                        {
                            { "grant_type", "refresh_token" },
                            { "client_id", TimeTrackUser.strClientID },
                            { "client_secret", TimeTrackUser.strClientSecret },
                            { "redirect_uri", "urn:ietf:wg:oauth:2.0:oob" },
                            { "refresh_token", refreshToken }
                        };
                        var content = new FormUrlEncodedContent(values);

                        try
                        {
                            var response = await c.PostAsync("https://app.asana.com/-/oauth_token", content);
                            var responseString = await response.Content.ReadAsStringAsync();

                            var jObj = JObject.Parse(responseString);
                            var newToken = jObj.SelectToken("access_token");
                            //var newToken2 = jObj.SelectToken("refresh_token");

                            Properties.Settings.Default["strToken"] = newToken.ToString();
                            //Properties.Settings.Default["strRefreshToken"] = newToken2.ToString();
                            //DateTime now = DateTime.Now;
                            //Properties.Settings.Default["dtTokenTime"] = now;
                            Properties.Settings.Default.Save();

                        }
                        catch (Exception ex2)
                        {
                            System.Windows.Forms.MessageBox.Show("Workspaces: Could not establish a connection. Attempt:" + count.ToString());
                            count++;
                        }
                    }
                }
            }
        }

        protected async Task getProjects()
        {
            bool bHaveProjects = false;
            int iTimeTrackUserID = Convert.ToInt32(Properties.Settings.Default["iTimeTrackUserID"]);
            clsTimeTrackUsers TimeTrackUser = new clsTimeTrackUsers(iTimeTrackUserID);
            Projects = new List<clsAsanaAPI.Projects>();
            int count = 1;

            while (bHaveProjects == false && count <= 3)
            {
                try
                {
                    string token = Properties.Settings.Default["strToken"].ToString();
                    string refreshToken = Properties.Settings.Default["strRefreshToken"].ToString();

                    using (var client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

                        foreach (var myWorkspace in Workspaces)
                        {
                            var responseString = await client.GetStringAsync("https://app.asana.com/api/1.0/workspaces/" + myWorkspace.id + "/projects");
                            var myInfo = JObject.Parse(responseString);

                            dynamic projectInfo = JsonConvert.DeserializeObject(myInfo.ToString());
                            JArray projects = projectInfo.data;

                            foreach (var project in projects)
                            {
                                clsAsanaAPI.Projects newProject = new clsAsanaAPI.Projects();
                                newProject.id = project["id"].ToString();
                                newProject.name = project["name"].ToString();
                                Projects.Add(newProject);
                            }
                        }

                        bHaveProjects = true;
                    }
                }
                catch (Exception ex)
                {
                    string token = Properties.Settings.Default["strToken"].ToString();
                    string refreshToken = Properties.Settings.Default["strRefreshToken"].ToString();

                    using (var c = new HttpClient())
                    {
                        var values = new Dictionary<string, string>
                        {
                            { "grant_type", "refresh_token" },
                            { "client_id", TimeTrackUser.strClientID },
                            { "client_secret", TimeTrackUser.strClientSecret },
                            { "redirect_uri", "urn:ietf:wg:oauth:2.0:oob" },
                            { "refresh_token", refreshToken }
                        };
                        var content = new FormUrlEncodedContent(values);

                        try
                        {
                            var response = await c.PostAsync("https://app.asana.com/-/oauth_token", content);
                            var responseString = await response.Content.ReadAsStringAsync();

                            var jObj = JObject.Parse(responseString);
                            var newToken = jObj.SelectToken("access_token");
                            //var newToken2 = jObj.SelectToken("refresh_token");

                            Properties.Settings.Default["strToken"] = newToken.ToString();
                            //Properties.Settings.Default["strRefreshToken"] = newToken2.ToString();
                            //DateTime now = DateTime.Now;
                            //Properties.Settings.Default["dtTokenTime"] = now;
                            Properties.Settings.Default.Save();

                        }
                        catch (Exception ex2)
                        {
                            System.Windows.Forms.MessageBox.Show("Projects: Could not establish a connection. Attempt:" + count.ToString());
                            count++;
                        }
                    }
                }
            }
        }

        protected async Task getUserTasks()
        {
            bool bHaveTasks = false;
            int iTimeTrackUserID = Convert.ToInt32(Properties.Settings.Default["iTimeTrackUserID"]);
            clsTimeTrackUsers TimeTrackUser = new clsTimeTrackUsers(iTimeTrackUserID);
            Tasks = new List<clsAsanaAPI.Tasks>();
            int count = 1;

            while (bHaveTasks == false && count <= 3)
            {
                try
                {
                    string token = Properties.Settings.Default["strToken"].ToString();
                    string refreshToken = Properties.Settings.Default["strRefreshToken"].ToString();

                    using (var client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

                        foreach (var workspace in Workspaces)
                        {
                            var responseString = await client.GetStringAsync("https://app.asana.com/api/1.0/tasks?workspace=" + workspace.id.ToString() + "&assignee=me&opt_fields=name,projects,workspace,completed,notes");
                            var myInfo = JObject.Parse(responseString);
                            clsAsanaAPI.Tasks myTasks = new clsAsanaAPI.Tasks();
                            myTasks = JsonConvert.DeserializeObject<clsAsanaAPI.Tasks>(myInfo.ToString());
                            Tasks.Add(myTasks);
                        }
                        bHaveTasks = true;
                    }
                }
                catch (Exception ex)
                    {
                    string token = Properties.Settings.Default["strToken"].ToString();
                    string refreshToken = Properties.Settings.Default["strRefreshToken"].ToString();

                    using (var c = new HttpClient())
                    {
                        var values = new Dictionary<string, string>
                            {
                                { "grant_type", "refresh_token" },
                                { "client_id", TimeTrackUser.strClientID },
                                { "client_secret", TimeTrackUser.strClientSecret },
                                { "redirect_uri", "urn:ietf:wg:oauth:2.0:oob" },
                                { "refresh_token", refreshToken }
                            };
                        var content = new FormUrlEncodedContent(values);

                        try
                        {
                            var response = await c.PostAsync("https://app.asana.com/-/oauth_token", content);
                            var responseString = await response.Content.ReadAsStringAsync();

                            var jObj = JObject.Parse(responseString);
                            var newToken = jObj.SelectToken("access_token");
                            //var newToken2 = jObj.SelectToken("refresh_token");

                            Properties.Settings.Default["strToken"] = newToken.ToString();
                            //Properties.Settings.Default["strRefreshToken"] = newToken2.ToString();
                            //DateTime now = DateTime.Now;
                            //Properties.Settings.Default["dtTokenTime"] = now;
                            Properties.Settings.Default.Save();

                        }
                        catch (Exception ex2)
                        {
                            System.Windows.Forms.MessageBox.Show("Tasks: Could not establish a connection. Attempt:" + count.ToString());
                            count++;
                        }
                    }
                }
            }
        }

        private void btnTasks_Click(object sender, RoutedEventArgs e)
        {
            ViewTasks ViewTasks = new ViewTasks();
            ViewTasks.Show();
            this.Close();
        }

        private void chkbxTaskCompleted_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void chkbxTaskCompleted_Unchecked(object sender, RoutedEventArgs e)
        {

        }
    }
}
